package com.ocs.accountservice.util;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.ocs.accountservice.constant.AccountConstant;
import com.ocs.common.constants.AppConstants;
import com.ocs.common.dto.AccountPendingTxnStmtDetailsDTO;
import com.ocs.common.dto.AccountStmtDetailsDTO;
import com.ocs.common.dto.AccountStmtResDataDTO;
import com.ocs.common.dto.AccountStmtResPendingTxnDataDTO;
import com.ocs.common.dto.GenericResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CreateXLS {
	public static XSSFWorkbook workbook;
	public static XSSFSheet sheet;

	public CreateXLS() {
		workbook = new XSSFWorkbook();
		sheet = workbook.createSheet("Account Statement");

	}

	public ByteArrayOutputStream generateExcel(String[] accountSummary,
			GenericResponse<AccountStmtResPendingTxnDataDTO> pendingTxnDetails,
			GenericResponse<AccountStmtResDataDTO> txnDetails, Map<String, String> translationsMap) throws IOException {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			// String[] header = new String[] {
			// translationsMap.get(AccountConstant.ACCOUNT_SUMMARY),
			// translationsMap.get(AccountConstant.PENDING_TXN),
			// translationsMap.get(AccountConstant.TRANSACTIONS) };
			String[] header = new String[] { translationsMap.get(AccountConstant.ACCOUNT_SUMMARY),
					translationsMap.get(AccountConstant.PENDING_TXN),
					translationsMap.get(AccountConstant.TRANSACTIONS) };

			String[] sub_header1 = { translationsMap.get(AccountConstant.ACCOUNT_HOLDER_NAME),
					translationsMap.get(AccountConstant.ACCOUNT_NUMBER),
					translationsMap.get(AccountConstant.ACCOUNT_TYPE),
					translationsMap.get(AccountConstant.AVAILABLE_BALANCE),
					translationsMap.get(AccountConstant.ACTUAL_BALANCE) };

			String[] sub_header2 = { translationsMap.get(AccountConstant.HOLD_DATE),
					translationsMap.get(AccountConstant.DESCRIPTIONS), translationsMap.get(AccountConstant.CURRENCY),
					translationsMap.get(AccountConstant.AMOUNT), translationsMap.get(AccountConstant.POST_DATE) };

			String amount = translationsMap.get(AccountConstant.AMOUNT);
			String balance = translationsMap.get(AccountConstant.BALANCE);
			if (AppConstants.RESULT_CODE.equals(txnDetails.getStatus().getCode())
					&& txnDetails.getData().getCcy() != null) {
				amount = amount + "(" + txnDetails.getData().getCcy() + ")";
				balance = balance + "(" + txnDetails.getData().getCcy() + ")";
			}
			String[] sub_header3 = { translationsMap.get(AccountConstant.TRANSACTION_DATE),
					translationsMap.get(AccountConstant.DESCRIPTIONS), amount, balance };

			///////////////////////// Account Summary ///////////////////
			int rowMain_i = 0;
			Row row = sheet.createRow(rowMain_i);
			Cell cell = row.createCell(1);
			cell.setCellValue(header[0]);
			cell.setCellStyle(headingstyle(0, 4, rowMain_i, ++rowMain_i));

			row = sheet.createRow(++rowMain_i);
			for (int i = 0; i < sub_header1.length; i++) {
				Cell cell1 = row.createCell(i);
				cell1.setCellValue(sub_header1[i]);
				cell1.setCellStyle(titleStyle());
			}
			row = sheet.createRow(++rowMain_i);
			for (int i = 0; i < accountSummary.length; i++) {
				Cell cell3 = row.createCell(i);
				cell3.setCellValue(accountSummary[i]);
				cell3.setCellStyle(textCellStyle());
			}

			/////////////////////////// Pending Transaction ///////////////

			if (AppConstants.RESULT_CODE.equals(pendingTxnDetails.getStatus().getCode())
					&& !pendingTxnDetails.getData().getTxnDetails().isEmpty()) {
				rowMain_i = rowMain_i + 1;
				row = sheet.createRow(++rowMain_i);
				Cell cell2 = row.createCell(1);
				cell2.setCellValue(header[1]);
				cell2.setCellStyle(headingstyle(0, 4, rowMain_i, ++rowMain_i));

				row = sheet.createRow(++rowMain_i);
				for (int i = 0; i < sub_header2.length; i++) {
					Cell cell3 = row.createCell(i);
					cell3.setCellValue(sub_header2[i]);
					cell3.setCellStyle(titleStyle());
				}
				for (AccountPendingTxnStmtDetailsDTO dto : pendingTxnDetails.getData().getTxnDetails()) {
					row = sheet.createRow(++rowMain_i);
					Cell cellp1 = row.createCell(0);
					cellp1.setCellValue(dto.getHoldDate());
					cellp1.setCellStyle(textCellStyle());
					Cell cellp2 = row.createCell(1);
					StringBuilder desc = new StringBuilder();
					desc.append(Objects.nonNull(dto.getHoldReasonDesc()) ? dto.getHoldReasonDesc() + "\n"
							: StringUtils.EMPTY);
					desc.append(Objects.nonNull(dto.getHoldReferenceNumber()) ? dto.getHoldReferenceNumber() + "\n"
							: StringUtils.EMPTY);
					desc.append(Objects.nonNull(dto.getHoldDescription1()) ? dto.getHoldDescription1() + "\n"
							: StringUtils.EMPTY);
					desc.append(Objects.nonNull(dto.getHoldDescription2()) ? dto.getHoldDescription2() + "\n"
							: StringUtils.EMPTY);
					desc.append(Objects.nonNull(dto.getHoldDescription3()) ? dto.getHoldDescription3() + "\n"
							: StringUtils.EMPTY);
					desc.append(Objects.nonNull(dto.getHoldDescription4()) ? dto.getHoldDescription4() + "\n"
							: StringUtils.EMPTY);
					desc.append(Objects.nonNull(dto.getHoldDate()) ? dto.getHoldDate() : StringUtils.EMPTY);
					cellp2.setCellValue(
							/*
							 * dto.getHoldReasonDesc() +"\n" + dto.getHoldReferenceNumber() +"\n" +
							 * dto.getHoldDescription1() +"\n" + dto.getHoldDescription2() +"\n" +
							 * dto.getHoldDescription3() +"\n" + dto.getHoldDescription4() +"\n" +
							 * dto.getHoldDate()
							 */
							desc.toString());
					cellp2.setCellStyle(textCellStyle());
					Cell cellp3 = row.createCell(2);
					cellp3.setCellValue(pendingTxnDetails.getData().getCcy());
					cellp3.setCellStyle(textCellStyle());
					Cell cellp4 = row.createCell(3);
					cellp4.setCellValue(dto.getHoldAmount());
					cellp4.setCellStyle(amountCellStyle());
					Cell cellp5 = row.createCell(4);
					cellp5.setCellValue(dto.getHoldExpiryDate());
					cellp5.setCellStyle(textCellStyle());
				}
			}
///////////////////////// Transaction ////////////////////////

			if (AppConstants.RESULT_CODE.equals(txnDetails.getStatus().getCode())
					&& !txnDetails.getData().getTxnDetails().isEmpty()) {
				rowMain_i = rowMain_i + 1;
				row = sheet.createRow(++rowMain_i);
				Cell cell3 = row.createCell(1);
				cell3.setCellValue(header[2]);
				cell3.setCellStyle(headingstyle(0, 4, rowMain_i, ++rowMain_i));

				row = sheet.createRow(++rowMain_i);
				for (int i = 0; i < sub_header3.length; i++) {
					Cell cell4 = row.createCell(i);
					cell4.setCellValue(sub_header3[i]);
					cell4.setCellStyle(titleStyle());
				}

				for (AccountStmtDetailsDTO dto : txnDetails.getData().getTxnDetails()) {
					row = sheet.createRow(++rowMain_i);
					Cell cellp1 = row.createCell(0);
					cellp1.setCellValue(dto.getTxnDate());
					cellp1.setCellStyle(textCellStyle());
					Cell cellp2 = row.createCell(1);

					StringBuilder desc = new StringBuilder();
//desc.append(Objects.nonNull(dto.getTxnCodeDesc())?dto.getTxnCodeDesc() + "\n" : StringUtils.EMPTY);
//desc.append(Objects.nonNull(dto.getReferenceChequeNo())?dto.getReferenceChequeNo() + "\n" : StringUtils.EMPTY);
//desc.append(Objects.nonNull(dto.getNarrativeOne())?dto.getNarrativeOne() + "\n" : StringUtils.EMPTY);
//desc.append(Objects.nonNull(dto.getNarrativeTwo())?dto.getNarrativeTwo() + "\n" : StringUtils.EMPTY);
//desc.append(Objects.nonNull(dto.getNarrativeThree())?dto.getNarrativeThree() + "\n" : StringUtils.EMPTY);
//desc.append(Objects.nonNull(dto.getNarrativeFour())?dto.getNarrativeFour() + "\n" : StringUtils.EMPTY);
//desc.append(Objects.nonNull(dto.getTxnValueDate())?dto.getTxnValueDate() : StringUtils.EMPTY);

					if (Objects.nonNull(dto.getTxnCodeDesc()) && StringUtils.isNotEmpty(dto.getTxnCodeDesc().trim())) {
						desc.append(dto.getTxnCodeDesc() + "\n");
					}
					if (Objects.nonNull(dto.getReferenceChequeNo())
							&& StringUtils.isNotEmpty(dto.getReferenceChequeNo().trim())) {
						desc.append(dto.getReferenceChequeNo() + "\n");
					}
					if (Objects.nonNull(dto.getNarrativeOne())
							&& StringUtils.isNotEmpty(dto.getNarrativeOne().trim())) {
						desc.append(dto.getNarrativeOne() + "\n");
					}
					if (Objects.nonNull(dto.getNarrativeTwo())
							&& StringUtils.isNotEmpty(dto.getNarrativeTwo().trim())) {
						desc.append(dto.getNarrativeTwo() + "\n");
					}
					if (Objects.nonNull(dto.getNarrativeThree())
							&& StringUtils.isNotEmpty(dto.getNarrativeThree().trim())) {
						desc.append(dto.getNarrativeThree() + "\n");
					}
					if (Objects.nonNull(dto.getNarrativeFour())
							&& StringUtils.isNotEmpty(dto.getNarrativeFour().trim())) {
						desc.append(dto.getNarrativeFour() + "\n");
					}
					if (Objects.nonNull(dto.getTxnValueDate())
							&& StringUtils.isNotEmpty(dto.getTxnValueDate().trim())) {
						desc.append(dto.getTxnValueDate());
					}

					cellp2.setCellValue(
							/*
							 * dto.getTxnCodeDesc() +"\n" + dto.getReferenceChequeNo() +"\n" +
							 * dto.getNarrativeOne() +"\n" + dto.getNarrativeTwo() +"\n" +
							 * dto.getNarrativeThree() +"\n" + dto.getNarrativeFour() +"\n" +
							 * dto.getTxnValueDate()
							 */
							desc.toString());
					cellp2.setCellStyle(textCellStyle());
					Cell cellp3 = row.createCell(2);
					cellp3.setCellValue(dto.getTxnAmount());
					cellp3.setCellStyle(amountCellStyle());
					Cell cellp4 = row.createCell(3);
					cellp4.setCellValue(dto.getRunningBalance());
					cellp4.setCellStyle(amountCellStyle());
				}
			}
			workbook.write(stream);

			FileOutputStream out = new FileOutputStream("C:\\Statement.xlsx");
			workbook.write(out);
			out.flush();
			out.close();

		} catch (Exception e) {
			  log.error("Error Message",e.getMessage());

		} finally {

//if(stream!=null)
//stream.close();
			if (workbook != null)
				workbook.close();
		}
		return stream;
	}

	public XSSFCellStyle headingstyle(int col_i, int col_j, int row_i, int row_j) {
		for (int i = col_i; i <= col_j; i++) {
			sheet.setColumnWidth(i, 8000);
		}
		sheet.addMergedRegion(new CellRangeAddress(row_i, row_j, col_i + 1, col_j - 1));

		XSSFFont font = workbook.createFont();
		font.setFontHeightInPoints((short) 20);
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.RED.getIndex());
		font.setBold(true);

		XSSFCellStyle style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setFont(font);

		return style;
	}

	public XSSFCellStyle titleStyle() {
		XSSFFont font = workbook.createFont();
		font.setFontName("Times New Roman");
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		font.setColor(IndexedColors.BLUE.getIndex());

		XSSFCellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setFont(font);

		return style;
	}

	public XSSFCellStyle amountCellStyle() {

		XSSFFont font = workbook.createFont();
		font.setFontName("Courier New");
		font.setFontHeight((short) (10 * 20));
		font.setColor(IndexedColors.BLACK.getIndex());

		XSSFCellStyle style = workbook.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setWrapText(true);
		style.setVerticalAlignment(VerticalAlignment.TOP);
		style.setAlignment(HorizontalAlignment.RIGHT);

		style.setFont(font);

		return style;
	}

	public XSSFCellStyle textCellStyle() {
		XSSFFont font = workbook.createFont();
		font.setFontName("Courier New");
		font.setFontHeight((short) (10 * 20));
		font.setColor(IndexedColors.BLACK.getIndex());

		XSSFCellStyle style = workbook.createCellStyle();
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setWrapText(true);
		style.setVerticalAlignment(VerticalAlignment.TOP);
		style.setFont(font);

		return style;
	}

	public XSSFCellStyle errorStyle() {

		XSSFFont font = workbook.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.RED.getIndex());
		font.setBold(true);

		XSSFCellStyle style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(font);

		return style;
	}
}