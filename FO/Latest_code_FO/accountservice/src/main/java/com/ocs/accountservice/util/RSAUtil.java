package com.ocs.accountservice.util;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.ocs.accountservice.constant.AppConstant;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RSAUtil {

	private static String privateKey;

	private static String publicKey;

	public static void generateRSAKeys() throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(AppConstant.RSA_KEY_ALG);
		keyGen.initialize(AppConstant.RSA_KEY_SIZE);

		final KeyPair keyPair = keyGen.generateKeyPair();

		publicKey = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
		privateKey = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());
		log.info("Generated RSA Public ");

		System.out.println(encrypt("sysadmin123", publicKey));

	}

	private static String encrypt(String data, String publicKey) throws InvalidKeyException, NoSuchAlgorithmException,
			InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		final Cipher cipher = Cipher.getInstance(AppConstant.RSA_CIPHR_ALG);
		cipher.init(Cipher.ENCRYPT_MODE, readPublicKey(publicKey));
		return Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes()));
	}

	public static String encrypt(String data) throws InvalidKeyException, NoSuchAlgorithmException,
			InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		return encrypt(data, publicKey);
	}

	private static PrivateKey readPrivateKey(String pemPrivateKeyString)
			throws NoSuchAlgorithmException, InvalidKeySpecException {

		log.debug("Inside debug method");
		String base64KeyData = pemPrivateKeyString.replace("-----BEGIN PRIVATE KEY-----", "")
				.replace("-----END PRIVATE KEY-----", "").replaceAll("\\s", "");

		byte[] privateKeyBytes = Base64.getDecoder().decode(base64KeyData);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);

		return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
	}

	private static PublicKey readPublicKey(String pemPublicKeyString)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		String base64KeyData = pemPublicKeyString.replace("-----BEGIN PUBLIC KEY-----", "")
				.replace("-----END PUBLIC KEY-----", "").replaceAll("\\s", "");

		byte[] publicKeyBytes = Base64.getDecoder().decode(base64KeyData);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);

		return (PublicKey) keyFactory.generatePublic(keySpec);
	}

	public static String getPrivateKey() {
		return privateKey;
	}

	public static String getPublicKey() {
		return publicKey;
	}

	public static String decryptRSA(String encryptedText, String privateKeyVal)
			throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {
		return RSAUtil.decrypt(encryptedText, privateKeyVal);
	}

	private static String decrypt(String encryptedText, String privateKeyVal)
			throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {

		byte[] data = Base64.getDecoder().decode(encryptedText.getBytes());
		PrivateKey privateKey = readPrivateKey(privateKeyVal);

		final Cipher cipher = Cipher.getInstance(AppConstant.RSA_CIPHR_ALG);
		cipher.init(Cipher.DECRYPT_MODE, privateKey);

		return new String(cipher.doFinal(data));
	}

}
