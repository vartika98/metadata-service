package com.ocs.accountservice.dto.accountDetailsResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDetailsResponse implements Serializable {
	
    private ResultUtilVO result;
    private AccountDetailsDTO data;
    
}