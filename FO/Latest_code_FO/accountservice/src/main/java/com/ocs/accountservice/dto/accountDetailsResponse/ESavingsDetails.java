package com.ocs.accountservice.dto.accountDetailsResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ESavingsDetails implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -4218036372709144271L;
	private String currency;
    private double depositAmount;
    private String debitAccount;
    private String targetSavings;
    private String targetDate;
    private String siRefNo;
    private double buyRate;
    private double sellRate;
    private String accountStatus;
    private String siStatus;
    private String remarks;
    private int userNo;
    private String accountNo;
    private String createdDate;
    private String currentDate;

}