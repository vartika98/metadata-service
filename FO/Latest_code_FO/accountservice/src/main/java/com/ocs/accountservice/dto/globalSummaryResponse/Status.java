package com.ocs.accountservice.dto.globalSummaryResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Status implements Serializable {
	private static final long serialVersionUID = 1L;

	private String code;
	private String description;
	private String unitId;
}