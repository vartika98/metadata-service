package com.ocs.accountservice.adapter.mw.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountModel implements Serializable {
	private static final long serialVersionUID = 165487575235678L;

	private String number;
	private String type;
	private String name;
	private String currency;
	private BigDecimal balance;
	private boolean isClosed;
	private boolean isBlocked;
	private boolean isDormant;
	private boolean isDeceased;
	private String desc;
	private String actualBalance;
	private BigDecimal reservedBalance;
	private String inactive;
	private String lastUsedDate;
}
