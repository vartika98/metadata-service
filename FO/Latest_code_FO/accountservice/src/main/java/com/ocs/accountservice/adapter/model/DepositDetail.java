package com.ocs.accountservice.adapter.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepositDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	private String branch;
	private String depositType;
	private String reference;
	private String number;
	private String accountType;
	private String currency;
	private String startDate;
	private String periodCode;
	private String periodCodeDescription;
	private BigDecimal depositAmt;
	private BigDecimal currentInterest;
	private BigDecimal accruedInterest;
	private String interestDate;
	private BigDecimal interestAmount;
	private String interestAccount;
	private String renewalDate;
	private String maturityDate;
	private BigDecimal maturityAmount;
	private String isAutoRenewal;
}
