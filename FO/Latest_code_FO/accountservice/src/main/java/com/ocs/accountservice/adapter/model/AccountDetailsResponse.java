package com.ocs.accountservice.adapter.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDetailsResponse implements Serializable {
	private static final long serialVersionUID = 132423456757L;

	private GlobalSummaryStatus status;
	private String globalId;
	private String nationalId;

	@JsonProperty("eSavingsAccounts")
	private List<ESavingsDetail> eSavingsAccounts;

	@JsonProperty("depositAccounts")
	private List<DepositDetail> depositAccounts;
}
