package com.ocs.accountservice.dto.ibanResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDetails implements Serializable {

	private static final long serialVersionUID = -3966437290825905903L;

	private String iban;
	private String accountNumber;
	private String accountType;
	private String customerType;
	private String currency;
	private String shortName;
	private String bankId;
	private String country;
	private String location;
	private String branchId;
	private String swiftCode;

}
