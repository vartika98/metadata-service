package com.ocs.accountservice.adapter.repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.ocs.accountservice.core.repository.JPAPageConfigRepository;
import com.ocs.common.entity.Translation;

@Repository
public class PageConfigRepositoryImpl implements PageConfigRepository {
	
	 private final JPAPageConfigRepository jpaPageConfigRepository;

	    
	  public PageConfigRepositoryImpl(JPAPageConfigRepository jpaPageConfigRepository) {
	        this.jpaPageConfigRepository = jpaPageConfigRepository;
	    }

	@Override
	public Map<String, String> findByUnit_IdAndChannel_ChannelAndLanguage_IdAndPageAndGroup(String unit, String channel,
			String en, String accStmtReceipt, String metadataGroup) {
		 List<Translation> translation = jpaPageConfigRepository.findByUnit_IdAndChannel_ChannelAndLanguage_IdAndPageAndGroup(
	                unit, channel, en, accStmtReceipt, metadataGroup);
		 
      return translation.stream()
             .collect(Collectors.toMap(Translation::getKey, Translation::getValue));

	}
	}




	

