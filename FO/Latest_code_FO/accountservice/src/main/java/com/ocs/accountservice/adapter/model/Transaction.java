package com.ocs.accountservice.adapter.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction implements Serializable {
	private static final long serialVersionUID = 8748459832871L;

	private String txnDate;
	private String txnCode;
	private String txnCodeDesc;
	private String txnType;
	private String txnAmount;
	private String runningBalance;
	private String referenceChequeNo;
	private String narrativeOne;
	private String narrativeTwo;
	private String narrativeThree;
	private String narrativeFour;
	private boolean withinOneMonth;
	private boolean showRepeat;
	private String auid;
	private boolean swiftTxn;

}
