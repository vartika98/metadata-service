package com.ocs.accountservice.adapter.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ESavingsDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	private String currency;
	private BigDecimal depositAmount;
	private String debitAccount;
	private String targetSavings;
	private String targetDate;
	private String siRefNo;
	private BigDecimal buyRate;
	private BigDecimal sellRate;
	private String accountStatus;
	private String siStatus;
	private String remarks;
	private Long userNo;
	private String accountNo;
	private String createdDate;
	private String currentDate;
}
