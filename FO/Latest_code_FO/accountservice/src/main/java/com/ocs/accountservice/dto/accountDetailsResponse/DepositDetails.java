package com.ocs.accountservice.dto.accountDetailsResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepositDetails implements Serializable {

	private static final long serialVersionUID = -2918186455489253071L;
	private String branch;
	private String depositType;
	private String reference;
	private String number;
	private String accountType;
	private String currency;
	private String startDate;
	private String periodCode;
	private String periodCodeDescription;
	private double depositAmt;
	private int currentInterest;
	private double accruedInterest;
	private String interestDate;
	private double interestAmount;
	private String interestAccount;
	private String renewalDate;
	private String maturityDate;
	private double maturityAmount;
	private String isAutoRenewal;
}
