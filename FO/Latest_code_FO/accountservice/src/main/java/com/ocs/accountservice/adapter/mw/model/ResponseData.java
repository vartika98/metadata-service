package com.ocs.accountservice.adapter.mw.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseData {
	private MwResponseStatus status;
	private String globalId;
	private List<AccountSummary> summary;
}
