package com.ocs.accountservice.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class JWTAuthenticationTokenProvider { // env variable

	// @Value("#{systemEnvironment['JWT_PVT_KEY']}")
	private String jwtPvtKey = "-----BEGIN PRIVATE KEY-----MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCleMFr8Mw+RQddfpdF4SAw2JjmOaq6ZHxocA3auWJs4MGX1+VAFhvr6tFPDf2arlsHr5dx2DSe0UXg5LWk3KqSYMA2xVU9p2CAmHGd7fCQfX4at64frAih+ynNNzStpjaJGvukHVpBwXUGTcK8CUxreiOW9cMKUdaz/bkdsFJ6+3GFXjMGuaSftElkcc76XsH4bBbHawsxkE+5VPGxaKFM/+8LfAV2DN51PGfm1RycYFQ7P5UIoZK2RIbEMxBH/CxnNQb1iT4wvViOi0RbRpU18Kk6dgLm2jyffrY9Wx8RJ31xsC4UmhlODjtWCE5IlCZLSsDz1HbFJO7Er3tgDVjVAgMBAAECggEAAZ4jx0sZjBWiyxNfl5GxRO5RLFzRMIpRSpvgEJPc2d37/afhFLoJuC2SlYMb5I+/CqQjqp815Atg81kVg/uav/kNZj883UV/68AnH1Izz3+Zsr0PUD2fYm2UsFGmy7FfeazlsQbHxWTqLuvfJOck0GYukh1r+6x00hLjHdBbGaIhjFv50PvaaJERIIjozXYThlIELnb2ChKKMhTroF/M70ehw8B+SvpUvisl8KkbNfLwuz4Uj1FrR0YxbquBCT9HgGtprjQFW/b7uw3k9uVWCYe8lsuozh9BH0GUTsIZtNBvD06xmvgI7ELiHkcfzeI+PVjWL2LR9FXHvx0txP2sfwKBgQDSGD4RR7xgYHIhFR3WL1567gQZMWYBtVEUOTPRMMQNE/gphOsHscnVI+9ykyys814WQ1+rrHUg7xfYUJ0zaA4a6IVXHeCJAS7VLC4DjzK7cyd+szccZRA9ibuSw6YFnslNl6yZ3ngzLEPjd7O58dbvF4QcwwKJmU7nZUImRBgD0wKBgQDJoICY30j61CULz/2Of32FAxZChAyxhtkKX73jljQMkOHirC0hZ5VjdeF1WQLaLz3E9wov9ymnSOf8y7Bwc7+CnvkTPUk2d8KqQjeZZz+3f1oy/qfxDJYc5D+pUpxMEXc1hsjE5DaV53HkKCZrwlqzHYD++hoWLIqXpvnoySnPtwKBgCAt4mPXLv08CH8Usus5eikuB6asMEmWsgJ8+HjUFVNDecEyQKV/D8D7rhNIg5FB/Wk/jInI8m/G70oMnudeyrpIXxQG2XtimP/BspKM/mJDswru/EP3HtOuIvLJ0MYPmhmuX5t5vcNoxkUy8m5KoncgjPWy0NKujq1Wjxk5WMxNAoGAV8hmVKhaCMKWQndSiFjfWCrI5TrCoAP6uLaIYO7DSHlHrz2RBIspkjz/ZOzU/hM1TGzV5Z4xZ2Cph/lSnxL13TPDc2nqVyl6+k+MD2DRb92lcrBGg/8b2AZT5S9PVxr/kLFW9cn6LfevMUt9tsrOXE9LGzfeZasMyAvtoDj82C8CgYEAqpi6QYaaBkM1+YGXnUoF4hPIhDFMYvBPlgpOoINt5LCLgGHC9p5xihdsdleA/w1wvtFrO8EbaQs6Oh2lROinVJ3lR9hoacEAAOgvRDenMML3Vwhq0xzX34hMPQpJ5dpaMFiY7peHRjP5rm84JA9NtCA4mdksoo8/B0l3VarwQtY=-----END PRIVATE KEY-----";

	// @Value("#{systemEnvironment['JWT_PUBLIC_KEY']}")
	private String jwtKey = "MIIDETCCAfkCFA6eYfJbvV1oIUPreHekWsw5WymlMA0GCSqGSIb3DQEBCwUAMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwHhcNMjMwOTA1MDcxMzMxWhcNMzMwOTAyMDcxMzMxWjBFMQswCQYDVQQGEwJBVTETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApXjBa/DMPkUHXX6XReEgMNiY5jmqumR8aHAN2rlibODBl9flQBYb6+rRTw39mq5bB6+Xcdg0ntFF4OS1pNyqkmDANsVVPadggJhxne3wkH1+GreuH6wIofspzTc0raY2iRr7pB1aQcF1Bk3CvAlMa3ojlvXDClHWs/25HbBSevtxhV4zBrmkn7RJZHHO+l7B+GwWx2sLMZBPuVTxsWihTP/vC3wFdgzedTxn5tUcnGBUOz+VCKGStkSGxDMQR/wsZzUG9Yk+ML1YjotEW0aVNfCpOnYC5to8n362PVsfESd9cbAuFJoZTg47VghOSJQmS0rA89R2xSTuxK97YA1Y1QIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAldZg2My1i4qhtVZnetn2K2BDlzwLamaDdK3DDB3d54qwQtjnsMN6UubCYbs+wLfdK6SkoGr/a9e0LQ+gAerc7MJKAws884vJPd5qLkHwPMY0Y7fPy6AcQoCv6s1NB3ki5kzuh9wCSlB3/hD4ZHrEZFUOVte9tryuvjGK+8GFk19ZTfP6+jo+zycZtRPBjpKtutV5YOnPwW3rV1RJVei67Ow5wD2ruX5nsOm0Y4E+G5ChoEBsuuc7uR6ZpSlHnbHXqw+msPVW6M+6lnZdZRRr+8vXZVvX79PviXTmQAP/glOIo+2v0sVZyDqMooQkwQ3mbOeeCCW8IVadF4YcOXgyT";

	// @Value("#{systemEnvironment['TOKEN_VALIDITY']}")
	private long tokenValidity = 300000;

	// @Value("#{systemEnvironment['TOKEN_ISSUER']}")
	private String tokenIssuer = "DB";

	private static final String BEGIN_CERTIFICATE = "-----BEGIN CERTIFICATE-----";
	private static final String END_CERTIFICATE = "-----END CERTIFICATE-----";

	public String generateToken(String id, boolean isRefreshToken)
			throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
		Claims claims = Jwts.claims().subject(id).build();
		long nowMillis = System.currentTimeMillis();
		long expMillis;
		if (isRefreshToken) {
			expMillis = nowMillis + (tokenValidity * 2);
		} else {
			expMillis = nowMillis + tokenValidity;
		}
		Date exp = new Date(expMillis);

		return Jwts.builder().claims(claims).issuedAt(new Date(nowMillis)).expiration(exp).issuer(tokenIssuer)
				.id(UUID.randomUUID().toString()).signWith(readPrivateKey(jwtPvtKey)).compact();
	}

	private static RSAPrivateKey readPrivateKey(String pemPrivateKeyString)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		String base64KeyData = pemPrivateKeyString.replace("-----BEGIN PRIVATE KEY-----", "")
				.replace("-----END PRIVATE KEY-----", "").replaceAll("\\s", "");
		byte[] privateKeyBytes = Base64.getDecoder().decode(base64KeyData);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);

		return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
	}

	public RSAPublicKey getRSAPublicKey() throws CertificateException, IOException {

		BufferedReader reader = new BufferedReader(new StringReader(jwtKey));
		StringBuilder pemContents = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			pemContents.append(line).append("\n");
		}
		reader.close();

		String pemData = pemContents.toString().replaceAll(BEGIN_CERTIFICATE, "").replaceAll(END_CERTIFICATE, "")
				.replaceAll("\\s+", "");
		// Decode the Base64-encoded PEM data
		byte[] decodedBytes = Base64.getDecoder().decode(pemData);

		// Generate a certificate from the decoded bytes
		CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
		X509Certificate certificate = (X509Certificate) certFactory
				.generateCertificate(new ByteArrayInputStream(decodedBytes));

		PublicKey publicKey = certificate.getPublicKey();
		RSAPublicKey rsaPublicKey = null;
		if (publicKey instanceof RSAPublicKey) {
			rsaPublicKey = (RSAPublicKey) publicKey;
		} else {
			System.err.println("The public key is not an RSA public key.");
		}
		return rsaPublicKey;
	}

	public Boolean validateToken(String token) {
		boolean isValid = false;
		try {
			RSAPublicKey publicKey = null;
			publicKey = getRSAPublicKey();
			Claims claims = Jwts.parser().verifyWith(publicKey).build().parseSignedClaims(token).getPayload();
			final Date expiration = claims.getExpiration();
			isValid = expiration.after(new Date());

		} catch (Exception e) {
			return isValid;
		}
		return isValid;
	}

	public Claims getAllClaimsFromToken(String token) {

		RSAPublicKey publicKey = null;
		try {

			publicKey = getRSAPublicKey();
			return Jwts.parser().verifyWith(publicKey).build().parseSignedClaims(token).getPayload();

		} catch (Exception e) {
			return null;
		}
	}

}
