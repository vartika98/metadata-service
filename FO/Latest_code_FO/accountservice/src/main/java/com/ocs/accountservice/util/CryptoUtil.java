package com.ocs.accountservice.util;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.ocs.accountservice.constant.AppConstant;

public class CryptoUtil {

	public static String getPBKDF2(final String password, final String salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {

		final PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), fromHex(salt), AppConstant.PBKDF2_ITERATION,
				AppConstant.PBKDF2_KEY_SIZE);
		final SecretKeyFactory skf = SecretKeyFactory.getInstance(AppConstant.PBKDF2_SECRET_ALGORITHM);
		final byte[] hash = skf.generateSecret(spec).getEncoded();
		return toHex(hash);

	}

	private static String toHex(final byte[] array) {
		final BigInteger bi = new BigInteger(1, array);
		final String hex = bi.toString(16);
		final int paddingLength = array.length * 2 - hex.length();
		return paddingLength > 0 ? String.format("%0" + paddingLength + "d", 0) + hex : hex;
	}

	private static byte[] fromHex(final String hex) {
		final byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return bytes;
	}
}
