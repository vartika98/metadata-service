package com.ocs.accountservice.dto.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FxRateRequest implements Serializable {

	private static final long serialVersionUID = 7573873579092990866L;

	private String baseCurrency;
	private String targetCurrency;
	private String amount;
	private String date;
}
