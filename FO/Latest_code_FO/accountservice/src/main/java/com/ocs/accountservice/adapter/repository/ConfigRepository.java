package com.ocs.accountservice.adapter.repository;

import com.ocs.common.dto.ConfigDto;

//Repository interface defining data access operations related to users
public interface ConfigRepository {
 
	ConfigDto findByUnit_IdAndChannel_ChannelAndKeyAndStatus(String unit, String channel, String key, String status);

}