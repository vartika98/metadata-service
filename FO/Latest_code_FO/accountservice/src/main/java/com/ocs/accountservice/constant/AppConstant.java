package com.ocs.accountservice.constant;

import java.util.HashMap;
import java.util.Map;

public class AppConstant {

	public static final String PUBLIC_URL = "/public";

	public static final String HEADER_UNIT = "unit";

	public static final String HEADER_CHANNEL = "channel";

	public static final String HEADER_ACCEPT_LANGUAGE = "Accept-Language";

	public static final String SERVICE_ID = "serviceId";

	public static final String RSA_CIPHR_ALG = "RSA/ECB/PKCS1Padding";

	public static final int PBKDF2_ITERATION = 100000;

	public static final int PBKDF2_KEY_SIZE = 128;

	public static final String PBKDF2_SECRET_ALGORITHM = "PBKDF2WithHmacSHA512";

	public static final String RSA_KEY_ALG = "RSA";

	public static final int RSA_KEY_SIZE = 2048;

	public static final String STATUS = "ACT";

	public static final String CHANNEL_MB = "MB";

	public static final String IB_OCS_ENC_REQ = "IB_OCS_ENC_REQ";

	public static final String RP_SERVICE_ID = "RP";

	public static final String LOGIN_SERVICE_ID = "LOGIN";

	public static final String SUCCESS = "SUCCESS";

	public static final String ACT = "000000";

	public static final String RESULT_CODE = "000000";

	public static final String GEN_ERROR_CODE = "000001";

	public static final String GEN_ERROR_DESC = "Unable to process your request,Please contact Customer Care for futher assistance or try again later";

	public static final String GLOBAL_ID = "globalId";
	public static final String GID = "gid";
	public static final String GUID = "guid";
	public static final String AUID = "auid";

	public static final String FUNCTIONAL_ID = "functionalId";
	public static final String INQUIRY_TYPE = "inquiryType";
	public static final String UNIT_ID = "unitId";
	public static final String IS_DELEGATED_ACCOUNT = "isDelegateAccount";

	public static final Map<String, String> ACCOUNT_TYPE_MAP = new HashMap<>();

	static {
		ACCOUNT_TYPE_MAP.put("CA", "accounts");
		ACCOUNT_TYPE_MAP.put("DN", "savingAccs");
		ACCOUNT_TYPE_MAP.put("DP", "deposits");
		ACCOUNT_TYPE_MAP.put("ES", "esavingAccs");
	}

}
