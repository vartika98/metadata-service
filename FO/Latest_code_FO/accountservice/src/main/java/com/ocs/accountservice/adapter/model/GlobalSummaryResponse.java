package com.ocs.accountservice.adapter.model;

import java.io.Serializable;

import com.ocs.common.dto.ResultUtilVO;

import lombok.Data;

@Data
public class GlobalSummaryResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private ResultUtilVO result;
	private GlobalAccountSummary data;
}