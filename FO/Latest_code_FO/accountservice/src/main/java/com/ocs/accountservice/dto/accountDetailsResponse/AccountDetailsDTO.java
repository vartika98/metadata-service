package com.ocs.accountservice.dto.accountDetailsResponse;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDetailsDTO implements Serializable {
	
    private Long globalId;
    private String nationalId;
    private List<ESavingsDetails> eSavingsAccounts;
    private List<DepositDetails> depositAccounts;
    
}
