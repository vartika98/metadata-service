package com.ocs.accountservice.core.usecase;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.ocs.accountservice.adapter.repository.ConfigRepository;
import com.ocs.accountservice.adapter.service.MwService;
import com.ocs.accountservice.util.JWTAuthenticationTokenProvider;
import com.ocs.common.dto.GenericResponse;
import com.ocs.common.dto.ResultUtilVO;
import com.ocs.common.repository.JPARRmessageRepository;
import com.ocs.common.service.RRmessageAuditingService;
import com.ocs.common.service.impl.MqJmsMessage;

import jakarta.servlet.http.HttpServletRequest;

public class AccountUseCaseImpl {

	private ConfigRepository configRepository;

	private RRmessageAuditingService rrMessageAuditingService;

	private JPARRmessageRepository rrMessageRepository;

	private ResultUtilVO resultUtilVO;

	@Autowired
	private JWTAuthenticationTokenProvider jwtAuthenticationTokenProvider;

	@Autowired
	MqJmsMessage mqJmsMessage;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private MwService mwService;

	public AccountUseCaseImpl(ConfigRepository configRepository, RRmessageAuditingService rrMessageAuditingService,
			JPARRmessageRepository rrMessageRepository) {
		this.configRepository = configRepository;
		this.rrMessageAuditingService = rrMessageAuditingService;
		this.rrMessageRepository = rrMessageRepository;
	}

	public GenericResponse<Map<String, Object>> getAccountStmt(String auid, String startDate, String endDate) {
		GenericResponse<Map<String, Object>> response = new GenericResponse<>();
		
		return response;
	}
}
