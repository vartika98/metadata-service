package com.ocs.accountservice.dto.fxRateResponse;

import java.io.Serializable;
import java.util.List;

import com.ocs.accountservice.dto.globalSummaryResponse.Status;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FxResponseData implements Serializable {

	private static final long serialVersionUID = -88166770967045264L;

	private Status status;
	private String unitId;
	private String sysDate;
	private List<FxRate> fxRates;
}
