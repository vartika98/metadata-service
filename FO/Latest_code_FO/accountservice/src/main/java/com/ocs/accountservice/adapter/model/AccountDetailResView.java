package com.ocs.accountservice.adapter.model;

import java.io.Serializable;

import com.ocs.common.dto.ResultUtilVO;

import lombok.Data;

@Data
public class AccountDetailResView implements Serializable {
	private static final long serialVersionUID = 1L;

	private ResultUtilVO result;
	private AccountDetailsResponse data;
	@Override
	public String toString() {
		return "AccountDetailResView [result=" + result + ", data=" + data + "]";
	}
}
