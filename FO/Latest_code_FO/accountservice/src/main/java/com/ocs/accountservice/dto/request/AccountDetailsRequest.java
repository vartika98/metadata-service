package com.ocs.accountservice.dto.request;

public class AccountDetailsRequest {
	private Long globalId;

	public AccountDetailsRequest() {
		// Default constructor
	}

	public AccountDetailsRequest(Long globalId) {
		this.globalId = globalId;
	}

	public Long getGlobalId() {
		return globalId;
	}

	public void setGlobalId(Long globalId) {
		this.globalId = globalId;
	}

	@Override
	public String toString() {
		return "AccountDetailsRequest{" + "globalId=" + globalId + '}';
	}
}
