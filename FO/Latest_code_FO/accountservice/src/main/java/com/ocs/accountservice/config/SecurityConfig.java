package com.ocs.accountservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http.formLogin(formlogin -> formlogin.disable()).csrf(csrf -> csrf.disable()).headers((headers) -> {
			headers.frameOptions(frameOption -> {
				frameOption.disable();
			});
		}).authorizeHttpRequests(httpRequest -> {
			httpRequest.anyRequest().permitAll();
		});
		return http.build();
	}
}
