package com.ocs.accountservice.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ocs.common.entity.Translation;
@Repository
public interface JPAPageConfigRepository extends JpaRepository<Translation , Integer> {
	List<Translation> findByUnit_IdAndChannel_ChannelAndLanguage_IdAndPageAndGroup(String unit, String channel, String en,
			String accStmtReceipt, String metadataGroup);

}
