package com.ocs.accountservice.adapter.model;

import java.io.Serializable;

import com.ocs.common.dto.ResultUtilVO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountStmtRes implements Serializable {
	private static final long serialVersionUID = 8765456788765L;

	private ResultUtilVO result;
	private AccountStmtResTxnDataRes data;
}
