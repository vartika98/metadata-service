package com.ocs.accountservice.dto.ibanResponse;

import java.io.Serializable;

import com.ocs.common.dto.ResultUtilVO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IBANResponseDto implements Serializable {

	private static final long serialVersionUID = -6703367727493647961L;

//	private ResultUtilVO result;
	private AccountDetails data;

}
