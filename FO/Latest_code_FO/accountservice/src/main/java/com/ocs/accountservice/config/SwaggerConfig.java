package com.ocs.accountservice.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;

@OpenAPIDefinition
@Configuration
public class SwaggerConfig {

	private static final String SERVICE_TITLE = "Account Service";
	private static final String SERVICE_VERSION = "1.0.0";
	private static final String URL = "http://localhost:8080";
	private static final String LICENSE_URL = "https://www.nb.com";

	@Bean
	OpenAPI customOpenAPI() {
		final String securitySchemeName = "bearerAuth";
		return new OpenAPI().servers(List.of(new Server().url(URL)))
				.components(new Components().addSecuritySchemes(securitySchemeName,
						new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("Bearer").bearerFormat("JWT")))
				.security(List.of(new SecurityRequirement().addList(securitySchemeName)))
				.info(new Info().title(SERVICE_TITLE).version(SERVICE_VERSION)
						.license(new License().name("NB-License").url(LICENSE_URL)));
	}
}
