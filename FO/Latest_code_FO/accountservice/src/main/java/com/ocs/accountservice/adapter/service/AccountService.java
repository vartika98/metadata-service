package com.ocs.accountservice.adapter.service;

import java.util.List;
import java.util.Map;

import com.ocs.accountservice.dto.fxRateResponse.FxResponseData;
import com.ocs.accountservice.dto.ibanResponse.IBANResponseDto;
import com.ocs.common.dto.AccountStmtReqDTO;
import com.ocs.common.dto.GenericResponse;

public interface AccountService {

	public GenericResponse<Map<String, Object>> getAccountDetails(String unit, String channel, String gid, String lang,
			String serviceId);

	public GenericResponse<Map<String, Object>> getAccountStmt(String channel, String unit, String lang,
			String serviceId, String guid, String startDate, String endDate);

	public List<FxResponseData> getFxRates(String gId, String unit, String channel, String headerUnit, String serviceId,
			String functionId);

	public GenericResponse<IBANResponseDto> getIBANInfo(String uid);
	
	public GenericResponse<String> generateXLSReport(String unit, String channel, String lang, AccountStmtReqDTO req);

}
