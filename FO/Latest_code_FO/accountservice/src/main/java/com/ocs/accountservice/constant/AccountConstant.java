package com.ocs.accountservice.constant;

public interface AccountConstant {
	 
	public static final String API_ROOT_URL = "/";
 
	// open new deposit account
	public static final String SERVICEID = "serviceId";
 
	public static final String ACCOUNT_MICROID = "ACCOUNT";
 
	// Delegate Acc
	public static final String DELEGATOR_USERNO = "userNo";
 
	public static final String ACTIVE_STATUS = "ACT";
	
	public static final String PENDING_STATUS = "PEN";
	
	public static final String SBO_STATUS = "SBO";
	
	public static final String REJ_STATUS = "REJ";
 
	public static final String EMPTYSTRING = "";
 
	public static final String HEADER_ACC_NO = "accNo";
 
	public static final String ACTIVE = "Active";
 
	public static final String PENDING = "Pending";
	
	public static final String SENT_TO_BO = "Sent for Back Office Approval";
	
	public static final String REJECTED = "Rejected";
 
	public static final String DELETE = "DEL";
 
	public static final String DELUSERNO = "delUserNo";
 
	public static final String DELACNO = "delAcNo";
 
	public static final String USERNAME = "username";
 
	public static final String GID = "gId";
 
	public static final String ACNO = "acNo";
 
	public static final String DELEGSHORTNAME = "delegShortName";
 
	public static final String DELEGUSERNO = "delegUserNo";
 
	public static final String ACTYPEDESC = "acTypeDesc";
 
	public static final String DELEGACNO = "delegAcNo";
	
	public static final String ALLWDTRANTYPES = "allwdTranTypes";
 
	// Deposit
	public static final String DEPOSITTYPE = "depositType";
 
	public static final String MNEMONIC = "mnemonic";
 
	public static final String CURR = "curr";
 
	public static final String AMT = "amt";
 
	public static final String DEPSTARTDATE = "startDate";
 
	public static final String PERIODCODE = "periodCode";
 
	public static final String AUTORENEW = "autoRenew";
 
	public static final String FROMACC = "acNo";
 
	public static final String MATURITYACC = "prinAccNo";
 
	public static final String INTERESTACC = "intAccNo";
 
	public static final String CAPITALIZEINTEREST = "N";
 
	public static final String DEPOSIT_PERIOD_CODE = "DEPOSIT_PERIOD_CODE";
 
	public static final String PRODCODE = "prodCode";
 
	public static final String DEPOSIT = "DEPOSIT";
 
	public static final String INTERESTRATE = "intRate";
 
	public static final String FREQUENCY = "V22";
 
	public static final String OCS_SYSDATE = "yyyy-MM-dd";
 
	public static final String PROD_CODE = "DEP";
 
	// Account Details & cancel auto renewel
	public static final String Y = "Y";
 
	public static final String BRANCH = "branch";
 
	public static final String REFERENCENO = "reference";
 
	public static final String DEPOSIT_ACCT_CURR = "DEPOSIT_ACCT_CURR";
 
	public static final String ESAVING_EX_RATE = "1";
 
	public static final String ESAVING_ACCOUNT_TYPE = "ESAVING_ACCOUNT_POST_TYPE_";
 
	public static final String SERVICEID_ACCSUM = "ACCSUM";
 
	public static final String ISWAIVECHARGE = "ISWAIVECHARGE";
 
	public static final String ISWAIVECHARGE_FLAG = "Y";
 
	public static final String CHARGE_ACCOUNTNO = "CHARGE_ACCOUNTNO";
 
	public static final String BASE_RATE_CODE_SUFFIX = "BASERATECODE_";
 
	public static final String DETAILS = "DETAILS";
 
	public static final String DEPOSIT_INTRATE = "DEPACC_INTRATE";
 
	public static final String DEPSITACC_UNITS = "units";
 
	public static final String ENT_VLD_AMT = "ENT_VLD_AMT";
 
	public static final String RESPONSE_DATE = "dd-MM-yyyy";
 
	public static final String ESAVING_FORCE_OPEN_FLAG = "ESAVING_IS_FORCE_OPEN";
 
	public static final String DEPOSIT_TYPES = "DEPOSIT_TYPES";
 
	public static final String DEP_FREQUENCY = "FREQUENCY";
 
	public static final String DELACC_SMS_FLAG = "DELACC_SMS_FLAG";
 
	// Account Statement releated changes - Start
	public static final String METADATA_GROUP = "Default";
 
	public static final String ACCOUNT_SUMMARY_RECEIPT = "AccStmtReceipt";
 
	public static final String ACCOUNT_SUMMARY_RECEIPT_HEADER = "AccountStmtHeader";
 
	public static final String TXN_DATE = "TxnDateLbl";
 
	public static final String DESCRIPTION = "DescriptionLbl";
 
	public static final String AMOUNT_LBL = "AmountLbl";
 
	public static final String BALANCE_LBL = "BalanceLbl";
 
	public static final String CREDIT = "Credit";
 
	public static final String DEBIT = "Debit";
 
	public static final String CREDIT_TXN_CODES ="+";// "556";
 
	public static final String DEBIT_TXN_CODES ="-"; //"129,246,746,544,579,140";
 
	public static final String ACSTMT_TXNTYPE_I18 = "ACCSTMT_TXNTYPE";//ACCSTMTSUM
	
	public static final String ACCSTMT= "ACCSTMT";
	
	public static final String PENDING_ACCSTMT = "PENDING_ACCSTMT";
	
	public static final String ACC_STMT_RECEIPT = "ACC_STMT_RECEIPT";
 
	public static final String PENDING_TXN = "PENDING_TXN";
 
	public static final String ACCOUNT_SUMMARY = "ACCOUNT_SUMMARY";
 
	public static final String TRANSACTIONS = "TRANSACTIONS";
 
	public static final String ACCOUNT_HOLDER_NAME = "ACCOUNT_HOLDER_NAME";
 
	public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
 
	public static final String ACCOUNT_TYPE = "ACCOUNT_TYPE";
 
	public static final String ACTUAL_BALANCE = "ACTUAL_BALANCE";
 
	public static final String AVAILABLE_BALANCE = "AVAILABLE_BALANCE";
 
	public static final String CURRENCY = "CURRENCY";
 
	public static final String DESCRIPTIONS = "DESCRIPTION";
 
	public static final String HOLD_DATE = "HOLD_DATE";
 
	public static final String AMOUNT = "AMOUNT";
 
	public static final String POST_DATE = "POST_DATE";
 
	public static final String TRANSACTION_DATE = "TRANSACTION_DATE";
 
	public static final String BALANCE = "BALANCE";
 
	public static final Object PENDING_TXN_NO_DATA = "PENDING_TXN_NO_DATA";
 
	public static final Object TXN_NO_DATA = "TXN_NO_DATA";
 
	public final String REQUEST_DATE = "dd/MM/yyyy";
 
	public final String REPONSE_DATE = "yyyy-MM-dd";
	
	public static final String ACCTCARDHOLDERNAME = "custName";
	
	public static final String ACCTYPEDESC = "accTypeDesc";
	
	public static final String AVLBAL = "avlBal";
	
	public static final String ACC_ACTUAL_BAL = "accBal";
	
	public static final String ACCNUMFORMAT = "accNumFormat";
	
	public static final String STARTDATE ="startDate";
	
	public static final String ENDDATE = "endDate";
	
	public static final String UNIT = "unit";
	
	public static final String ESAVINGS = "ESAVINGS";
	// Account Statement releated changes - End
	
	public static final String OCS_T_FEEBACK_CUST = "OCS_T_FEEBACK_CUST";
	
	public static final String ACCDEL_TXN_CODE = "ACDLGT";
	
	public static final String OUTBOX_ACTIVE			 =	"Y";
	public static final String INBOX_ACTIVE				 =	"N";
	
	public static final String DELEGATE_OUTBOX_ACTIVE			 =	"N";
	public static final String DELEGEATE_INBOX_ACTIVE			 =	"Y";
	
	public static final String INBOX_MESSAGE	=	"Your request for Account Delegation transactions has been sent to Account Number ";
	public static final String MESSAGE_OF		=	" of ";
	public static final String MESSAGE_APPROVAL	=	" for his/her approval ";
	public static final String REQUEST_FROM		=	"You have A/C Delegation request from A/C ";
	
	public static final String ACCDLG_DEL_TXN_CODE = "DELACDLGT";
	public static final String ACCDEL_SUBJECT ="Delegation Cancellation";
	public static final String ACCDEL_SUMMARY = "Delegation Cancellation";
	public static final String ACCDEL_FROM_INBOX_MESSAGE = "Dear Customer, Your request on QNB Internet Banking for the cancellation of the delegation to account number";
	public static final String ACCDEL_SUCCESS_MESSAGE = "has been completed successfully";
	public static final String ACCDEL_TO_INBOX_MESSAGE = "Dear Customer, Your request on QNB Internet Banking for the cancellation of the delegated account number";
	
	
	public static final String ACCDEL_NOTALLOWED_FUNCS = "DELNOTALLOWDFUNCS";
	
	public static final String SWIFT_REQ = "swiftReq";
	public static final String SWIFT_RES = "swiftRes";
	public static final String MAIL_TXN_RECEIPT_CONFIG ="MAIL_TXN_RECEIPT";
	public static final String CC = "CC";
	public static final String SENDMAIL_NOTID = "_SENDMAIL_NOTID";
	public static final String SWIFT = "SWIFT";
	
	public static final String FD_NO_OTP = "OTP_REQ";
	public static final String SMS_NOTID = "_SMS_NOTID";
}