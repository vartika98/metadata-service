package com.ocs.accountservice.dto.globalSummaryResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account implements Serializable {

	private static final long serialVersionUID = -1986961501022022237L;

	private String number;
	private String type;
	private String name;
	private String currency;
	private String balance;
	private boolean isClosed;
	private boolean isBlocked;
	private boolean isDormant;
	private boolean isDeceased;
	private String desc;
	private String actualBalance;
	private String reservedBalance;
	private String inactive;
	private String lastUsedDate;
}
