package com.ocs.accountservice.dto.globalSummaryResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Summary implements Serializable {

	private Customer customers;

}
