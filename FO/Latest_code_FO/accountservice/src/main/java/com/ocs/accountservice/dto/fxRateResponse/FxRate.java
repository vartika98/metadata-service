package com.ocs.accountservice.dto.fxRateResponse;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FxRate implements Serializable {

	private static final long serialVersionUID = -4552804086274498042L;
	private String currency;
	private String currencyNumber;
	private String currDesc;
	private String spotRateReciprocal;
	private String spotRate;
	private String numberOfRetailRates;
	private List<Rate> rates;

}
