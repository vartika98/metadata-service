package com.ocs.accountservice.adapter.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocs.accountservice.adapter.service.AccountService;
import com.ocs.accountservice.constant.AppConstant;
import com.ocs.common.constants.AppConstants;
import com.ocs.common.dto.GenericResponse;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping(path = "/statement"+AppConstants.VERSION)
public class StatmentController {

	@Autowired
	private AccountService accountService;

	@PostMapping("/summary")
	@ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json", examples = @ExampleObject(value = """
			{
				"kp": "",
				"rk": "bfd0f081-f611-49d5-9144-13a26820804f",
				"ocsEncFlag": "",
				"key": "g1j+QIDAQAB"
			}""")))
	public GenericResponse<Map<String, Object>> getAccountStatment(
			@RequestHeader(name = AppConstant.HEADER_UNIT, required = true) String unit,
			@RequestHeader(name = AppConstant.HEADER_CHANNEL, required = true) String channel,
			@RequestHeader(name = AppConstant.HEADER_ACCEPT_LANGUAGE , required = true) String lang,
			@RequestHeader(name = AppConstant.SERVICE_ID, required = true) String serviceId,
			@RequestBody() Map<String, String> payload) {
		String auid = payload.get("auid");
		String startDate = payload.get("startDate");
		String endDate = payload.get("endDate");
		accountService.getAccountStmt(channel, unit, lang, serviceId, auid, startDate, endDate);
		return null;
	}
}
