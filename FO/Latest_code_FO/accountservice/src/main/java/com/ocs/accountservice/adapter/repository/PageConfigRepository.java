package com.ocs.accountservice.adapter.repository;

import java.util.Map;

public interface PageConfigRepository {

	Map<String, String> findByUnit_IdAndChannel_ChannelAndLanguage_IdAndPageAndGroup(String unit, String channel,
			String en, String accStmtReceipt, String metadataGroup);

	
}
