package com.ocs.accountservice.core.entity;

public enum AccountType {
	CURRENT_ACCT_TYPES,
	SAVINGS_ACCT_TYPES,
	E_SAVINGS_ACCT_TYPES,
	DEPOSIT_ACCT_TYPES,
	DEPOSIT_TYPES
}
