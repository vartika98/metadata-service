package com.ocs.accountservice.adapter.service;

import com.ocs.accountservice.adapter.model.AccountDetailResView;
import com.ocs.accountservice.adapter.model.AccountStmtRes;
import com.ocs.accountservice.adapter.mw.model.GlobalAccountSummaryRes;
import com.ocs.accountservice.dto.fxRateResponse.FxRateResponse;
import com.ocs.accountservice.dto.ibanResponse.IBANResponseDto;
import com.ocs.accountservice.dto.request.FxRateRequest;
import com.ocs.common.dto.GenericResponse;

public interface MwService {

	public GlobalAccountSummaryRes getGlobalAccountSummary(String channelId, String unitId, String lang,
			String serviceId);

	public AccountDetailResView getAccountDetails(String channelId, String unitId, String lang, String serviceId,
			String globalId);

	public AccountStmtRes getAccountStmt(String channelId, String unitId, String lang, String serviceId, String auid,
			String startDate, String endDate);

	public GenericResponse<FxRateResponse> getFxRate(FxRateRequest request, String unit, String channel, String lang,
			String serviceID);

	public GenericResponse<IBANResponseDto> getIBANInfo(String accountNumber);

}
