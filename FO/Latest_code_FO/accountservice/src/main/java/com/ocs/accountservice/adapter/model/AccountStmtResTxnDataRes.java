package com.ocs.accountservice.adapter.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountStmtResTxnDataRes implements Serializable {
	private static final long serialVersionUID = 16465734756764L;

	private BigDecimal balance;
	private String currency;
	private String accountNumber;
	private List<AccountStmtResTxnData> transactions;

}
