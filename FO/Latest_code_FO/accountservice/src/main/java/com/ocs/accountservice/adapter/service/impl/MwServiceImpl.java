package com.ocs.accountservice.adapter.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ocs.accountservice.adapter.model.AccountDetailResView;
import com.ocs.accountservice.adapter.model.AccountDetailsResponse;
import com.ocs.accountservice.adapter.model.AccountStmtRes;
import com.ocs.accountservice.adapter.model.AccountStmtResTxnData;
import com.ocs.accountservice.adapter.model.AccountStmtResTxnDataRes;
import com.ocs.accountservice.adapter.model.DepositDetail;
import com.ocs.accountservice.adapter.model.ESavingsDetail;
import com.ocs.accountservice.adapter.model.GlobalSummaryStatus;
import com.ocs.accountservice.adapter.mw.model.AccountModel;
import com.ocs.accountservice.adapter.mw.model.AccountSummary;
import com.ocs.accountservice.adapter.mw.model.GlobalAccountSummaryRes;
import com.ocs.accountservice.adapter.mw.model.MwResponseStatus;
import com.ocs.accountservice.adapter.mw.model.ResponseData;
import com.ocs.accountservice.adapter.service.MwService;
import com.ocs.accountservice.constant.AppConstant;
import com.ocs.accountservice.dto.fxRateResponse.FxRate;
import com.ocs.accountservice.dto.fxRateResponse.FxRateResponse;
import com.ocs.accountservice.dto.fxRateResponse.FxResponseData;
import com.ocs.accountservice.dto.fxRateResponse.Rate;
import com.ocs.accountservice.dto.globalSummaryResponse.Result;
import com.ocs.accountservice.dto.globalSummaryResponse.Status;
import com.ocs.accountservice.dto.ibanResponse.AccountDetails;
import com.ocs.accountservice.dto.ibanResponse.IBANResponseDto;
import com.ocs.accountservice.dto.request.FxRateRequest;
import com.ocs.accountservice.util.CommonUtil;
import com.ocs.common.constants.AppConstants;
import com.ocs.common.dto.AppExceptionHandlerUtilDto;
import com.ocs.common.dto.GenericResponse;
import com.ocs.common.dto.ResultUtilVO;
import com.ocs.common.repository.JPARRmessageRepository;
import com.ocs.common.service.RRmessageAuditingService;
import com.ocs.common.service.impl.MqJmsMessage;
import com.ocs.common.util.DateUtil;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MwServiceImpl implements MwService {

	@Autowired
	private RRmessageAuditingService rrMessageAuditingService;

	@Autowired
	private JPARRmessageRepository rrMessageRepository;

	@Autowired
	MqJmsMessage mqJmsMessage;

	@Autowired
	private HttpServletRequest request;

	ResultUtilVO resultUtilVo = new ResultUtilVO();

	@Override
	public GlobalAccountSummaryRes getGlobalAccountSummary(String channelId, String unitId,
			String lang, String serviceId) {
		log.info("MwServiceImpl:getGlobalAccountSummary:start");
		GenericResponse<GlobalAccountSummaryRes> res = new GenericResponse<>();
		AppExceptionHandlerUtilDto appExeDto = new AppExceptionHandlerUtilDto();
		ResponseEntity<GlobalAccountSummaryRes> response = null;
		HttpEntity<Map<String, String>> entity = null;
		Map<String, String> header = new HashMap<>();

		try {
			resultUtilVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS, AppConstant.RESULT_CODE,
					AppConstant.SUCCESS);
			appExeDto.setStartTime(DateUtil.getCurrentDate());
			appExeDto.setChannel(channelId);
			appExeDto.setUnit(unitId);
			appExeDto.setServiceId(serviceId);
			Map<String, String> req = new HashMap<>();
			req.put(AppConstant.FUNCTIONAL_ID, "ACCOUNTSUMMARY");
			req.put(AppConstant.INQUIRY_TYPE, "GI");
			req.put(AppConstant.GLOBAL_ID, "100000000267548");
			req.put(AppConstant.UNIT_ID, "PRD");
			req.put(AppConstant.IS_DELEGATED_ACCOUNT, "TRUE");
			entity = new HttpEntity<>(req, CommonUtil.generHeaders(unitId, channelId));

			List<AccountModel> accountList = new ArrayList<>();
			List<AccountSummary> customerList = new ArrayList<>();
			accountList.add(new AccountModel("0027329621001", "CA", "AC0027329621001", "QAR", BigDecimal.valueOf(185022.90), false, false, false, false, "CURRENT ACCOUNT", "215128.28", BigDecimal.valueOf(0.00), "N", "2024-05-10"));
			accountList.add(new AccountModel("0066657858001", "CA", "AC0066657858001", "QAR", BigDecimal.valueOf(988999704590.14), false, false, false, false, "CURRENT ACCOUNT", "988999724025.84", BigDecimal.valueOf(0.00), "N", "2024-05-10"));
			accountList.add(new AccountModel("0066657858043", "DN", "AC0066657858043", "QAR", BigDecimal.valueOf(22.22), false, false, false, false, "SAVING PLUS", "22555224481.58", BigDecimal.valueOf(0.00), "N", "2024-05-10"));
			accountList.add(new AccountModel("0066657858052", "DE", "657858", "QAR", BigDecimal.valueOf(4413.69), false, false, false, false, "CALL DEPOSIT-24HRS", "4576.15", BigDecimal.valueOf(0.000), "N", "2024-05-09"));
			accountList.add(new AccountModel("0066657858054", "DE", "657858", "QAR", BigDecimal.valueOf(2342.10), false, false, true, false, "CALL DEPOSIT-24HRS", "2342.10", BigDecimal.valueOf(0.00), "N", "2024-05-10"));
			accountList.add(new AccountModel("0066657858055", "DE", "657858", "USD", BigDecimal.valueOf(1319.24), false, false, false, false, "CALL DEPOSIT-24HRS", "1319.24", BigDecimal.valueOf(0.00), "N", "2024-05-01"));
			accountList.add(new AccountModel("0066657858056", "DE", "657858", "AUD", BigDecimal.valueOf(2573.27), false, false, false, false, "CALL DEPOSIT-24HRS", "2573.27", BigDecimal.valueOf(0.00), "N", "2024-05-10"));
			accountList.add(new AccountModel("0066657858057", "DE", "657858", "USD", BigDecimal.valueOf(4892.61), false, false, false, false, "CALL DEPOSIT-24HRS", "4892.61", BigDecimal.valueOf(0.00), "N", "2024-05-02"));
			accountList.add(new AccountModel("0066657858058", "DE", "657858", "QAR", BigDecimal.valueOf(25000.00), false, false, false, false, "CALL DEPOSIT-24HRS", "25000.00", BigDecimal.valueOf(0.00), "N", "2024-05-01"));
			accountList.add(new AccountModel("0066657858100", "DP", "657858", "QAR", BigDecimal.valueOf(164468.00), false, false, false, false, "FIXED DEPOSIT", "164468.00", BigDecimal.valueOf(0.00), "N", "2024-05-02"));
			accountList.add(new AccountModel("0066657858114", "DT", "AC0066657858114", "QAR", BigDecimal.valueOf(3039.11), false, false, false, false, "FIXED DEPOSIT-UP TO ONE YEAR LIEN", "3039.11", BigDecimal.valueOf(0.00), "N", "2015-11-08"));

			customerList.add(new AccountSummary("25478415643", "657858", "657858", "EA", "CUSTOMER A", "0066", false, accountList));

			MwResponseStatus status = new MwResponseStatus("000000", "PRD : Success", "PRD");

			ResponseData accountSummary = new ResponseData(status, "100000000267548", customerList);

			GlobalAccountSummaryRes globalSummaryRes = new GlobalAccountSummaryRes();
			globalSummaryRes.setResult(resultUtilVo);
			globalSummaryRes.getData().add(accountSummary);

			response = ResponseEntity.ok(globalSummaryRes);

			appExeDto.setEndTime(DateUtil.getCurrentDate());

			if (Objects.nonNull(response) && Objects.nonNull(response.getBody())
					&& AppConstant.RESULT_CODE.equals(response.getBody().getResult().getCode())) {
				res.setData(response.getBody());
			}

			header.put(AppConstant.HEADER_UNIT, unitId);
			header.put(AppConstant.CHANNEL_MB, channelId);
			header.put(AppConstant.SERVICE_ID, serviceId);
			header.put(AppConstant.GUID, CommonUtil.generateUniqueID(appExeDto.getChannel()));
		} catch (Exception e) {
			appExeDto.setEndTime(DateUtil.getCurrentDate());
			log.error("Exception in mw account global summary service :{}", e.getMessage());
			resultUtilVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);

		} finally {
//			rrMessageAuditingService.updateRRMessage(appExeDto, rrMessageRepository, entity.getBody().toString(),
//					response.getBody().toString(), header, null, resultUtilVo, entity.getHeaders().toString());
//			mqJmsMessage.insertMsgQueue(entity.getHeaders(), entity.getBody(), request, response.getBody().getData(),
//					appExeDto, resultUtilVo);
//			log.info("MwServiceImpl:getGlobalAccountSummary:end");
		}

		res.setStatus(resultUtilVo);
		return res.getData();
	}

	@Override
	public AccountDetailResView getAccountDetails(String channelId, String unitId, String lang,
			String serviceId, String globalId) {
		log.info("MwServiceImpl:getAccountDetails:start");
		GenericResponse<AccountDetailResView> res = new GenericResponse<>();
		AppExceptionHandlerUtilDto appExeDto = new AppExceptionHandlerUtilDto();
		ResponseEntity<AccountDetailResView> response = null;
		HttpEntity<Map<String, String>> entity = null;
		Map<String, String> header = new HashMap<>();

		try {
			resultUtilVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
			appExeDto.setStartTime(DateUtil.getCurrentDate());
			appExeDto.setChannel(channelId);
			appExeDto.setUnit(unitId);
			appExeDto.setServiceId(serviceId);
			Map<String, String> req = new HashMap<>();
			req.put(AppConstant.GLOBAL_ID , "100000000267548");
			entity = new HttpEntity<>(req, CommonUtil.generHeaders(unitId, channelId));

			List<DepositDetail> depositList = new ArrayList<>();
			depositList.add(new DepositDetail("0066", "RTD", "657858066-005", "0066657858100", "DP", "QAR", "2024-01-01", "6M", "6 Months", BigDecimal.valueOf(25000.00), BigDecimal.valueOf(4), BigDecimal.valueOf(366.67), "2024-06-30", BigDecimal.valueOf(502.78), "0066657858043", "2024-06-30", "2024-06-30", BigDecimal.valueOf(25000.00), "N"));
			depositList.add(new DepositDetail("0066", "RTD", "657858066-008", "0066657858100", "DP", "QAR", "2024-01-31", "6M", "6 Months", BigDecimal.valueOf(21863.00), BigDecimal.valueOf(4), BigDecimal.valueOf(247.78), "2024-07-30", BigDecimal.valueOf(439.69), "0066657858001", "2024-07-30", "2024-07-30", BigDecimal.valueOf(21863.00), "N"));
			depositList.add(new DepositDetail("0066", "RTD", "657858066-009", "0066657858100", "DP", "QAR", "2024-01-31", "1Y", "1 Year", BigDecimal.valueOf(20807.00), BigDecimal.valueOf(4), BigDecimal.valueOf(235.81), "2025-01-30", BigDecimal.valueOf(843.84), "0066657858043", "2025-01-30", "2025-01-30", BigDecimal.valueOf(20807.00), "N"));
			depositList.add(new DepositDetail("0066", "RTD", "657858066-012", "0066657858100", "DP", "QAR", "2024-05-01", "1M", "1 Month", BigDecimal.valueOf(20500.00), BigDecimal.valueOf(3.1), BigDecimal.valueOf(19.42), "2024-05-31", BigDecimal.valueOf(52.96), "0066657858053", "2024-05-31", "0001-01-01", BigDecimal.valueOf(20500.00), "Y"));
			depositList.add(new DepositDetail("0066", "RTD", "657858066-013", "0066657858100", "DP", "QAR", "2024-05-01", "1M", "1 Month", BigDecimal.valueOf(25000.00), BigDecimal.valueOf(3.1), BigDecimal.valueOf(23.68), "2024-05-31", BigDecimal.valueOf(64.58), "0066657858043", "2024-05-31", "2024-05-31", BigDecimal.valueOf(25000.00), "N"));
			depositList.add(new DepositDetail("0066", "RTD", "657858066-014", "0066657858100", "DP", "QAR", "2024-05-01", "1M", "1 Month", BigDecimal.valueOf(25620.00), BigDecimal.valueOf(3.1), BigDecimal.valueOf(24.27), "2024-05-31", BigDecimal.valueOf(66.19), "0066657858001", "2024-05-31", "2024-05-31", BigDecimal.valueOf(25620.00), "N"));
			depositList.add(new DepositDetail("0066", "RTD", "657858066-015", "0066657858100", "DP", "QAR", "2024-05-02", "2M", "2 Months", BigDecimal.valueOf(25678.00), BigDecimal.valueOf(5.05), BigDecimal.valueOf(36.02), "2024-07-01", BigDecimal.valueOf(216.12), "0066657858001", "2024-07-01", "2024-07-01", BigDecimal.valueOf(25678.00), "N"));
			depositList.add(new DepositDetail("0066", "RTL", "657858066-001", "0066657858114", "DT", "QAR", "2015-11-08", "1M", "1 Month", BigDecimal.valueOf(3039.11), BigDecimal.valueOf(0), BigDecimal.valueOf(0.00), "2024-06-05", BigDecimal.valueOf(0.00), "0066657858114", "2024-06-05", "0001-01-01", BigDecimal.valueOf(3039.11), "Y"));
			List<ESavingsDetail> eSavingList = new ArrayList<>();
			eSavingList.add(new ESavingsDetail("QAR", BigDecimal.valueOf(11000.00), "0066657858043", "", null, null,
					BigDecimal.valueOf(1.0), BigDecimal.valueOf(1.0), "SUC", null,
					"DEBIT AC NUM 0066657858043- GUID:MBL001652108", 1100L, "0066657858052", "2024-02-21 09:24:05",
					null));
			eSavingList.add(new ESavingsDetail("GBP", BigDecimal.valueOf(12121.00), "0066657858001", "", null, null,
					BigDecimal.valueOf(1.0), BigDecimal.valueOf(1.0), "SUC", null,
					"DEBIT AC NUM 0066657858001- GUID:IBI001790985", 1100L, "0066657858052", "2023-11-01 14:12:34",
					null));
			eSavingList.add(new ESavingsDetail("QAR", BigDecimal.valueOf(25000.00), "0066657858001", "", null, null,
					BigDecimal.valueOf(1.0), BigDecimal.valueOf(1.0), "SUC", null,
					"DEBIT AC NUM 0066657858001- GUID:IB0003195570", 1100L, "0066657858058", "2024-05-19 14:28:31",
					null));
			eSavingList.add(new ESavingsDetail("USD", BigDecimal.valueOf(5000.00), "0066657858001", "", null, null,
					BigDecimal.valueOf(1.0), BigDecimal.valueOf(1.0), "SUC", null,
					"DEBIT AC NUM 0066657858001- GUID:MBN001900767", 1100L, "0066657858057", "2024-04-18 12:16:57",
					null));
			eSavingList.add(new ESavingsDetail("QAR", BigDecimal.valueOf(25312.00), "0066657858043", "", null, null,
					BigDecimal.valueOf(1.0), BigDecimal.valueOf(1.0), "SUC", null,
					"DEBIT AC NUM 0066657858043- GUID:MBM001748976", 1100L, "0066657858054", "2024-03-07 09:12:04",
					null));
			eSavingList.add(new ESavingsDetail("QAR", BigDecimal.valueOf(12555.00), "0066657858001", "", null,
					"E2USDQA657858016", BigDecimal.valueOf(1.0), BigDecimal.valueOf(1.0), "SUC", "ACT",
					"DEBIT AC NUM 0066657858001- GUID:IBI001791804", 1100L, "0066657858055", "2023-11-01 14:57:15",
					null));
			eSavingList.add(new ESavingsDetail("USD", BigDecimal.valueOf(3001.00), "0066657858043", "", null,
					"E2USDQA657858016", BigDecimal.valueOf(1.0), BigDecimal.valueOf(1.0), "SUC", "ACT",
					"DEBIT AC NUM 0066657858043- GUID:IBM002686870", 1100L, "0066657858055", "2024-03-17 15:33:45",
					null));
			eSavingList.add(new ESavingsDetail("QAR", BigDecimal.valueOf(12344.00), "0066657858001", "", null, null,
					BigDecimal.valueOf(1.0), BigDecimal.valueOf(1.0), "SUC", null,
					"DEBIT AC NUM 0066657858001- GUID:IBI001791731", 1100L, "0066657858054", "2023-11-01 14:55:39",
					null));

			GlobalSummaryStatus status = new GlobalSummaryStatus("000000", "PRD : Success", "PRD");

			AccountDetailsResponse accountData = new AccountDetailsResponse();
			accountData.setStatus(status);
			accountData.setGlobalId(globalId);
			accountData.setNationalId("");
			accountData.setESavingsAccounts(eSavingList);
			accountData.setDepositAccounts(depositList);

			AccountDetailResView accountResponse = new AccountDetailResView();
			accountResponse.setResult(resultUtilVo);
			accountResponse.setData(accountData);

			response = ResponseEntity.ok(accountResponse);

			appExeDto.setEndTime(DateUtil.getCurrentDate());

			if (Objects.nonNull(response) && Objects.nonNull(response.getBody())
					&& AppConstant.RESULT_CODE.equals(response.getBody().getResult().getCode())) {
				res.setData(response.getBody());
			}

			header.put(AppConstant.HEADER_UNIT, unitId);
			header.put(AppConstant.CHANNEL_MB, channelId);
			header.put(AppConstant.SERVICE_ID, serviceId);
			header.put(AppConstant.GUID, CommonUtil.generateUniqueID(appExeDto.getChannel()));
		} catch (Exception e) {
			appExeDto.setEndTime(DateUtil.getCurrentDate());
			log.error("Exception in mw account details service :{}", e.getMessage());
			resultUtilVo = new ResultUtilVO(AppConstants.GEN_ERROR_CODE, AppConstants.GEN_ERROR_DESC);

		} finally {
//			rrMessageAuditingService.updateRRMessage(appExeDto, rrMessageRepository, entity.getBody().toString(),
//					response.getBody().toString(), header, null, resultUtilVo, entity.getHeaders().toString());
//			mqJmsMessage.insertMsgQueue(entity.getHeaders(), entity.getBody(), request, response.getBody().getData(),
//					appExeDto, resultUtilVo);
			log.info("MwServiceImpl:getAccountDetails:end");
		}
		res.setStatus(resultUtilVo);
		return res.getData();
	}

	public AccountStmtRes getAccountStmt(String channelId, String unitId, String lang,
			String serviceId, String auid, String startDate, String endDate) {
		log.info("MwServiceImpl:getAccountStmt:start");
		GenericResponse<AccountStmtRes> res = new GenericResponse<>();
		AppExceptionHandlerUtilDto appExeDto = new AppExceptionHandlerUtilDto();
		ResponseEntity<AccountStmtRes> response = null;
		HttpEntity<Map<String, String>> entity = null;
		Map<String, String> header = new HashMap<>();

		try {
			resultUtilVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
			appExeDto.setStartTime(DateUtil.getCurrentDate());
			appExeDto.setChannel(channelId);
			appExeDto.setUnit(unitId);
			appExeDto.setLang(lang);
			appExeDto.setServiceId(serviceId);
			Map<String, String> req = new HashMap<>();
//			req.put("accountNumber", "0027329621001");
//			req.put("currencyCode", "QAR");
//			req.put("startDate", "2023-11-12");
//			req.put("endDate", "2024-05-12");
			entity = new HttpEntity<>(req, CommonUtil.generHeaders(unitId, channelId));

			List<AccountStmtResTxnData> accountTxnList = new ArrayList<>();
			accountTxnList.add(new AccountStmtResTxnData("2023-11-12", "2023-11-12", 246, "FUNDTRANSFER",
					BigDecimal.valueOf(-242.63), BigDecimal.valueOf(1102719.34), "MB231226105976",
					"GB43MIDL40152121277413NIRPTechno", "Familyandlivingexpenses", "NMBMBJ000215386180873261N",
					"26/1213230011MUAT", null, "0011$$MW00000330"));

			AccountStmtResTxnDataRes accountStmtRes = new AccountStmtResTxnDataRes();

			accountStmtRes.setBalance(BigDecimal.valueOf(0));
			accountStmtRes.setCurrency("QAR");
			accountStmtRes.setAccountNumber("0027-329621001");
			accountStmtRes.setTransactions(accountTxnList);

			AccountStmtRes accountRes = new AccountStmtRes();
			accountRes.setResult(new ResultUtilVO(AppConstant.RESULT_CODE,
					AppConstant.SUCCESS, AppConstant.RESULT_CODE, AppConstant.SUCCESS));
			accountRes.setData(accountStmtRes);

			response = ResponseEntity.ok(accountRes);

			appExeDto.setEndTime(DateUtil.getCurrentDate());

			if (Objects.nonNull(response) && Objects.nonNull(response.getBody())
					&& AppConstant.RESULT_CODE.equals(response.getBody().getResult().getMwCode())) {
				res.setData(response.getBody());
			}

			header.put(AppConstant.HEADER_UNIT, unitId);
			header.put(AppConstant.CHANNEL_MB, channelId);
			header.put(AppConstant.SERVICE_ID, serviceId);
			header.put(AppConstant.GUID, CommonUtil.generateUniqueID(appExeDto.getChannel()));
		} catch (Exception e) {
			appExeDto.setEndTime(DateUtil.getCurrentDate());
			log.error("Exception in mw account getAccountStmt service :{}", e.getMessage());
			resultUtilVo = new ResultUtilVO(AppConstants.GEN_ERROR_CODE, AppConstants.GEN_ERROR_DESC);

		} finally {
//		rrMessageAuditingService.updateRRMessage(appExeDto, rrMessageRepository, entity.getBody().toString(),
//				response.getBody().toString(), header, null, resultUtilVo, entity.getHeaders().toString());
//		mqJmsMessage.insertMsgQueue(entity.getHeaders(), entity.getBody(), request, response.getBody().getData(),
//				appExeDto, resultUtilVo);
			log.info("MwServiceImpl:getAccountStmt:end");
		}
		res.setStatus(resultUtilVo);

		return res.getData();
	}

	@Override
	public GenericResponse<FxRateResponse> getFxRate(FxRateRequest fxRateRequest, String unit, String channel,
			String lang, String serviceID) {
		GenericResponse<FxRateResponse> res = new GenericResponse<>();
		AppExceptionHandlerUtilDto appExeDto = null;
		ResponseEntity<FxRateResponse> response = null;
		HttpEntity<Map<String, String>> entity = null;
		FxRateResponse resMap = new FxRateResponse();
		Map<String, String> header = new HashMap<>();
		try {
			resultUtilVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);

			appExeDto = new AppExceptionHandlerUtilDto();
			appExeDto.setUnit(unit);
			appExeDto.setChannel(channel);
			appExeDto.setLang(lang);
			appExeDto.setServiceId(serviceID);

			appExeDto.setEndTime(DateUtil.getCurrentDate());

			// Mocking data for FxRateResponse
			Result result = new Result(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
			resMap.setResult(result);

			FxResponseData responseData = new FxResponseData();
			Status status = new Status(AppConstant.RESULT_CODE, "PRD : Success", "PRD");
			responseData.setStatus(status);
			responseData.setUnitId("PRD");
			responseData.setSysDate("2024-07-11");

			List<Rate> rates = new ArrayList<>();
			rates.add(new Rate("1", "1.1234", "1.1234", "1.2345"));
			rates.add(new Rate("2", "2.2345", "2.2345", "2.3456"));

			List<FxRate> fxRates = new ArrayList<>();
			fxRates.add(new FxRate("USD", "840", "US Dollar", "1.1234", "1.2345", "2", rates));
			fxRates.add(new FxRate("EUR", "978", "Euro", "1.2234", "1.3345", "2", rates));

			responseData.setFxRates(fxRates);

			resMap.setData(List.of(responseData));

			response = ResponseEntity.ok(resMap);
			appExeDto.setEndTime(DateUtil.getCurrentDate());

			if (Objects.nonNull(response) && Objects.nonNull(response.getBody())
					&& AppConstant.RESULT_CODE.equals(response.getBody().getResult().getCode())) {
				res.setData(response.getBody());
			}

			// Constructing headers
			HttpHeaders headers = new HttpHeaders();
			headers.add("channelId", "IB");
			headers.add("dateTime", "2024-07-11 12:47:01.889");
			headers.add("guid", "IB0003512534");
			headers.add("unitId", "PRD");
			headers.add("branchCode", "0011");
			headers.add("Encrypted", "TRUE");
			headers.add("Content-Type", "application/json");
			headers.add("Environment", "SIT");

			header.put("unit", "PRD");
			header.put("channel", "IB");
			header.put("serviceId", "GET_FX_RATE");
			header.put("guid", CommonUtil.generateUniqueID(appExeDto.getChannel()));

			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("united", "");
			entity = new HttpEntity<>(requestBody, headers);

		} catch (Exception e) {
			appExeDto.setEndTime(DateUtil.getCurrentDate());
			log.info("Exception in getFxRate service :{}", e.getMessage());
			resultUtilVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		} finally {
			rrMessageAuditingService.updateRRMessage(appExeDto, rrMessageRepository, entity.getBody().toString(),
					response.getBody().toString(), header, null, resultUtilVo, entity.getHeaders().toString());
			mqJmsMessage.insertMsgQueue(entity.getHeaders(), entity.getBody(), request, response.getBody(), appExeDto,
					resultUtilVo);
		}
		res.setStatus(resultUtilVo);
		return res;
	}

	@Override
	public GenericResponse<IBANResponseDto> getIBANInfo(String accountNumber) {
		GenericResponse<IBANResponseDto> response = new GenericResponse<>();
		ResultUtilVO resultUtilVo = new ResultUtilVO();
		IBANResponseDto ibanResponse = new IBANResponseDto();
		AppExceptionHandlerUtilDto appExeDto = new AppExceptionHandlerUtilDto("PRD", "MB", "en", "getIBAN");
		HttpEntity<Map<String, String>> entity = null;
		Map<String, String> headerMap = new HashMap<>();

		// Prepare the request and header map
		Map<String, String> requestMap = new HashMap<>();
		requestMap.put("accountNumber", accountNumber);

		try {
			headerMap.put("channelId", "MB");
			headerMap.put("dateTime", DateUtil.getCurrentDate().toString());
			headerMap.put("guid", CommonUtil.generateUniqueID("MB"));
			headerMap.put("unitId", "PRD");
			headerMap.put("branchCode", "0011");
			headerMap.put("Encrypted", "TRUE");
			headerMap.put("Content-Type", "application/json");
			headerMap.put("Environment", "SIT");

			entity = new HttpEntity<>(requestMap, CommonUtil.generHeaders(appExeDto.getUnit(), appExeDto.getChannel()));

			// Hardcoded response
			ResultUtilVO result = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.SUCCESS);
			AccountDetails accountDetails = new AccountDetails();
			accountDetails.setIban("QA50QNBA000000000066657858001");
			accountDetails.setAccountNumber(accountNumber);
			accountDetails.setAccountType("CA");
			accountDetails.setCustomerType("EA");
			accountDetails.setCurrency("QAR");
			accountDetails.setShortName("AC" + accountNumber);
			accountDetails.setBankId("");
			accountDetails.setCountry("");
			accountDetails.setLocation("");
			accountDetails.setBranchId("");
			accountDetails.setSwiftCode("QNBAQAQAXXX");

			// ibanResponse.setResult(result);
			ibanResponse.setData(accountDetails);

			resultUtilVo = result;
		} catch (Exception e) {
			log.error("Exception occurred while fetching IBAN info: {}", e.getMessage());
			resultUtilVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		} finally {
			try {
				rrMessageAuditingService.updateRRMessage(appExeDto, rrMessageRepository,
						entity != null ? entity.getBody().toString() : "",
						ibanResponse != null ? ibanResponse.toString() : "", headerMap, null, resultUtilVo,
						entity != null ? entity.getHeaders().toString() : "");

				mqJmsMessage.insertMsgQueue(entity != null ? entity.getHeaders() : null,
						entity != null ? entity.getBody() : null, request, ibanResponse, appExeDto, resultUtilVo);
			} catch (Exception e) {
				log.error("Exception occurred while updating RR message or inserting message queue: {}",
						e.getMessage());
			}
		}

		response.setStatus(resultUtilVo);
		response.setData(ibanResponse);
		return response;
	}
}
