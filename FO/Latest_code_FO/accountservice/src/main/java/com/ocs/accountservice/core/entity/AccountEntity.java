package com.ocs.accountservice.core.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_USER")
public class AccountEntity {

	@Id
	@Column(name = "uid")
	private String uid;

	@Column(name = "unitName")
	private String unitName;

	@Column(name = "accBal")
	private String accBalance;

	@Column(name = "avlBal")
	private String avlBalance;

	@Column(name = "accBalRaw")
	private String accBalRaw;

	@Column(name = "accBalBase")
	private String accBalBase;

	@Column(name = "myIBAN")
	private boolean myIBAN;

	@Column(name = "curr")
	private String curr;

	@Column(name = "accNum")
	private String accNum;

	@Column(name = "accNumFormat")
	private String accNumFormat;

	@Column(name = "accType")
	private String accType;

	@Column(name = "accTypeDesc")
	@Enumerated(EnumType.STRING)
	private AccountType accTypeDesc;

	@Column(name = "custName")
	private String custName;

	@Column(name = "custType")
	private String custType;

	@Column(name = "lastUsedDate")
	@Temporal(TemporalType.DATE)
	private String lastUsedDate;

	@Column(name = "colorCode")
	private String colorCode;

	@Column(name = "unit")
	private String unit;

	@Column(name = "amt2")
	private Long amt2;

	@Column(name = "amt3")
	private Long amt3;

	@Column(name = "mnemonic")
	private String mnemonic;
}
