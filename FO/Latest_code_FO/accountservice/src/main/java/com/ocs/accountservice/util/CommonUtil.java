package com.ocs.accountservice.util;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;

import org.springframework.http.HttpHeaders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CommonUtil {

	private static final Random random = new Random();

	public static String generateUniqueID(String channel) {
		int count = random.nextInt(100000000);
		String numericPart = String.format("%08d", count);
		return channel + numericPart;
	}

	public static HttpHeaders generHeaders(String unit, String channel) {
		HttpHeaders headers = new HttpHeaders();

		headers.add("channelId", channel);
		headers.add("guid", generateUniqueID(channel));
		headers.add("dateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSS")));
		headers.add("unitId", unit);
		headers.add("branchCode", "TEST");
		headers.add("Encrypted", "N");
		headers.add("Content-Type", "application/json");
		headers.add("Environment", "DEV");
		return headers;
	}

	public static <T> T readJsonFromFile(TypeReference<T> typeReference, String filename) {
		ObjectMapper objectMapper = new ObjectMapper();
		T objects = null;

		try {
			objects = objectMapper.readValue(new File(filename), typeReference);
			System.out.println("objects: " + objects);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objects;
	}

	public static <T> List<T> readJsonListFromFile(TypeReference<List<T>> typeReference, String filename) {
		ObjectMapper objectMapper = new ObjectMapper();
		List<T> objects = null;

		try {
			objects = objectMapper.readValue(new File(filename), typeReference);
			System.out.println("objects: " + objects);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objects;
	}

	public static String formatAccNum(String accNum) {
		if (accNum == null || accNum.length() != 13) {
			throw new IllegalArgumentException("accNum must be a 13-digit string");
		}

		// Format: "0066657858001" -> "0066-657858-001"
		return accNum.substring(0, 4) + "-" + accNum.substring(4, 10) + "-" + accNum.substring(10);
	}
}
