package com.ocs.accountservice.core.usecase.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.ocs.common.constants.AppConstants;
import com.ocs.common.dto.GenericResponse;
import com.ocs.common.dto.ResultUtilVO;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ApplicationExceptionHandler {

	public final ResponseEntity<GenericResponse<ResultUtilVO>> commonAppExHandler(Throwable throwable,
			WebRequest request) {

		GenericResponse<ResultUtilVO> responseData = new GenericResponse<>();

		try {
			responseData.setStatus(new ResultUtilVO(AppConstants.GEN_ERROR_CODE, AppConstants.GEN_ERROR_DESC));
		} catch (Exception e) {
			log.error("Exception inside commonAppExHandler {}" + e.getMessage());
		}

		return ResponseEntity.ok(responseData);
	}

	@ExceptionHandler(value = { ClassCastException.class })
	public final ResponseEntity<GenericResponse<ResultUtilVO>> classCasting(Throwable throwable, WebRequest request) {

		GenericResponse<ResultUtilVO> responseData = new GenericResponse<>();

		try {
			responseData.setStatus(new ResultUtilVO(AppConstants.GEN_ERROR_CODE, AppConstants.GEN_ERROR_DESC));
		} catch (Exception e) {
			log.error("Exception inside commonAppExHandler {}" + e.getMessage());
		}

		return ResponseEntity.ok(responseData);
	}

}
