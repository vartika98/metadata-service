package com.ocs.accountservice.adapter.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalSummaryStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	private String code;
	private String description;
	private String unitId;

}
