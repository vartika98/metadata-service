package com.ocs.accountservice.core.entity;

import jakarta.persistence.Column;
import lombok.Data;

@Data
public class DetailEntity {

	@Column(name = "branch")
	private String branch;

	@Column(name = "depositType")
	private String depositType;

	@Column(name = "reference")
	private String reference;

	@Column(name = "number")
	private String number;

	@Column(name = "accountType")
	private String accountType;

	@Column(name = "currency")
	private String currency;

	@Column(name = "startDate")
	private String startDate;

	@Column(name = "periodCode")
	private String periodCode;

	@Column(name = "periodCodeDesc")
	private String periodCodeDescription;

	@Column(name = "depositAmt")
	private String depositAmt;

	@Column(name = "currentInterest")
	private String currentInterest;

	@Column(name = "accruedInterest")
	private String accruedInterest;

	@Column(name = "interestDate")
	private String interestDate;

	@Column(name = "interestAmount")
	private String interestAmount;

	@Column(name = "interestAccount")
	private String interestAccount;

	@Column(name = "renewalDate")
	private String renewalDate;

	@Column(name = "maturityDate")
	private String maturityDate;

	@Column(name = "maturityAmount")
	private String maturityAmount;

	@Column(name = "isAutoRenewal")
	private String isAutoRenewal;
}
