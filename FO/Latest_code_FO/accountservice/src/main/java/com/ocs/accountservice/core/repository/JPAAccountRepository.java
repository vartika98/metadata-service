package com.ocs.accountservice.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ocs.accountservice.core.entity.AccountEntity;

public interface JPAAccountRepository extends JpaRepository<AccountEntity, String> {

}
