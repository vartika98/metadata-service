package com.ocs.accountservice.dto.globalSummaryResponse;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Customer implements Serializable {
	
	
	private static final long serialVersionUID = -6529967426643932794L;
	
	private String nationalId;
	private String mnemonic;
	private String basic;
	private String type;
	private String name;
	private String branch;
	private boolean isDelegate;
	private List<Account> accounts;
	
}
