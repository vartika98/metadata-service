package com.ocs.accountservice.dto.accountDetailsResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultUtilVO implements Serializable {

	private String code;
	private String description;
	private String mwCode;
	private String mwdesc;

}
