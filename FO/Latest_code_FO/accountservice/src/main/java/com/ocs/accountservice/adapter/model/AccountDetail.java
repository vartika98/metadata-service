package com.ocs.accountservice.adapter.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ocs.common.dto.DepositDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDetail implements Serializable {
	private static final long serialVersionUID = 8765457876551L;

	private String uid;
	private String unitName;
	private String accBal;
	private String avlBal;
	private String accBalRaw;
	private BigDecimal accBalBase;
	private boolean myIBAN;
	private String curr;
	private String accNum;
	private String accNumFormat;
	private String accType;
	private String accTypeDesc;
	private String custName;
	private String custType;
	private String lastUsedDate;
	private String colorCode;
	private String unit;
	private Long amt2;
	private Long amt3;
	private String mnemonic;
	private DepositDto details;

}
