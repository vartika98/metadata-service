package com.ocs.accountservice.adapter.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountStmtResTxnData implements Serializable {
	private static final long serialVersionUID = 1764985982873L;

	private String transactionDate;
	private String transactionValueDate;
	private Integer transactionCode;
	private String transactionCodeDescription;
	private BigDecimal transactionAmount;
	private BigDecimal runningBalance;
	private String referenceChequeNo;
	private String narrativeOne;
	private String narrativeTwo;
	private String narrativeThree;
	private String narrativeFour;
	private Boolean withinOneMonth;
	private String hostPostRef;
}
