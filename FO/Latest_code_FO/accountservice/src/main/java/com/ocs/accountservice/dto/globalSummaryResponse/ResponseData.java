package com.ocs.accountservice.dto.globalSummaryResponse;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData {

	private Status status;
	private String globalId;
	private List<Summary> summary;

}
