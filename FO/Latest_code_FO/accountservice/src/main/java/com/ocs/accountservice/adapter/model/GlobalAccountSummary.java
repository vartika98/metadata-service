package com.ocs.accountservice.adapter.model;

import java.io.Serializable;
import java.util.List;

import com.ocs.accountservice.adapter.mw.model.CustomerModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalAccountSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	private GlobalSummaryStatus status;
	private String globalId;
	private List<CustomerModel> summary;
	@Override
	public String toString() {
		return "GlobalAccountSummary [status=" + status + ", globalId=" + globalId + ", summary=" + summary + "]";
	}
}
