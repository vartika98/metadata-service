package com.ocs.accountservice.adapter.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountResView {

	private String id;
	private String name;
	private String currency;
	private String iban;
	private String unitTotal;
	private List<AccountDetail> accounts;
	private List<AccountDetail> savingAccs;
	private List<AccountDetail> deposits;
	private List<AccountDetail> esavingAccs;
}
