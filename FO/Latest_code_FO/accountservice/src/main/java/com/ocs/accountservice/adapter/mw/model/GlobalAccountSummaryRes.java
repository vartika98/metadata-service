package com.ocs.accountservice.adapter.mw.model;

import java.io.Serializable;
import java.util.List;

import com.ocs.common.dto.ResultUtilVO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalAccountSummaryRes implements Serializable {
	private static final long serialVersionUID = 989874598751L;

	private ResultUtilVO result;
	private List<ResponseData> data;
}
