package com.ocs.accountservice.dto.fxRateResponse;

import java.io.Serializable;
import java.util.List;

import com.ocs.accountservice.dto.globalSummaryResponse.Result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FxRateResponse implements Serializable {

	private static final long serialVersionUID = 6892008485382224799L;

	private Result result;
	private List<FxResponseData> data;

}
