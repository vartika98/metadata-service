package com.ocs.accountservice.adapter.mw.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MwResponseStatus {

	private String code;
	private String description;
	private String unitId;
}
