package com.ocs.accountservice.adapter.mw.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountSummary {
	private String nationalId;
	private String mnemonic;
	private String basic;
	private String type;
	private String name;
	private String branch;
	private boolean isDelegate;
	private List<AccountModel> accounts;
}
