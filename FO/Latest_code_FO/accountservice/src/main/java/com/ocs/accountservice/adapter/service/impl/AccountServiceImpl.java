package com.ocs.accountservice.adapter.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocs.accountservice.adapter.model.AccountDetail;
import com.ocs.accountservice.adapter.model.AccountDetailResView;
import com.ocs.accountservice.adapter.model.AccountResView;
import com.ocs.accountservice.adapter.model.AccountStmtRes;
import com.ocs.accountservice.adapter.mw.model.AccountModel;
import com.ocs.accountservice.adapter.mw.model.AccountSummary;
import com.ocs.accountservice.adapter.mw.model.GlobalAccountSummaryRes;
import com.ocs.accountservice.adapter.mw.model.ResponseData;
import com.ocs.accountservice.adapter.repository.PageConfigRepository;
import com.ocs.accountservice.adapter.service.AccountService;
import com.ocs.accountservice.adapter.service.MwService;
import com.ocs.accountservice.constant.AccountConstant;
import com.ocs.accountservice.constant.AppConstant;
import com.ocs.accountservice.dto.fxRateResponse.FxRateResponse;
import com.ocs.accountservice.dto.fxRateResponse.FxResponseData;
import com.ocs.accountservice.dto.ibanResponse.IBANResponseDto;
import com.ocs.accountservice.dto.request.FxRateRequest;
import com.ocs.accountservice.util.CommonUtil;
import com.ocs.accountservice.util.CreateXLS;
import com.ocs.common.constants.AppConstants;
import com.ocs.common.dto.AccountPendingTxnStmtDetailsDTO;
import com.ocs.common.dto.AccountStmtDetailsDTO;
import com.ocs.common.dto.AccountStmtReqDTO;
import com.ocs.common.dto.AccountStmtResDataDTO;
import com.ocs.common.dto.AccountStmtResPendingTxnDataDTO;
import com.ocs.common.dto.AppExceptionHandlerUtilDto;
import com.ocs.common.dto.GenericResponse;
import com.ocs.common.dto.ResultUtilVO;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

	private ResultUtilVO resultUtilVO = new ResultUtilVO();

	@Autowired
	private MwService mwService;
	
	@Autowired
	HttpServletRequest request;
	
	@Autowired
	private PageConfigRepository pageConfigRepository;


	@Override
	public GenericResponse<Map<String, Object>> getAccountDetails(String unit, String channel, String gid, String lang,
			String serviceId) {
		log.info("AccountServiceImpl:getAccountDetails:start");
//		String channelId = "MB";
//		String unitId = "PRD";
//		String lang = "";
//		String serviceId = "GACCSUM";

		GenericResponse<Map<String, Object>> response = new GenericResponse<>();

		Map<String, Object> accountDetailMap = new HashMap<>();

		resultUtilVO = new ResultUtilVO(AppConstant.ACT, AppConstant.SUCCESS);

		AccountResView accountResView = new AccountResView();
		List<AccountResView> unitList = new ArrayList<>();

		GlobalAccountSummaryRes globalResponse = mwService.getGlobalAccountSummary(channel, unit, lang, serviceId);
		for (ResponseData responseData : globalResponse.getData()) {
			AccountDetailResView accountDetailRes = mwService
					.getAccountDetails(channel, unit, lang, serviceId, responseData.getGlobalId());

			log.info("AccountServiceImpl:getAccountDetails:globalResponse:" + responseData);
			log.info("AccountServiceImpl:getAccountDetails:accountDetailResponse:" + accountDetailRes);

			if (Objects.nonNull(responseData) && Objects.nonNull(responseData.getSummary())
					&& !responseData.getSummary().isEmpty()) {

				accountResView.setAccounts(new ArrayList<>());
				accountResView.setSavingAccs(new ArrayList<>());
				accountResView.setDeposits(new ArrayList<>());
				accountResView.setEsavingAccs(new ArrayList<>());

				responseData.getSummary().stream().filter(Objects::nonNull)
						.filter(customer -> !customer.getAccounts().isEmpty())
						.flatMap(customer -> customer.getAccounts().stream()
								.map(resAccount -> formateAccountDetail(channel, unit, customer, resAccount)))
						.forEach(account -> {
							switch (account.getAccType()) {
							case "CA":
								accountResView.getAccounts().add(account);
								break;
							case "DN":
								accountResView.getSavingAccs().add(account);
								break;
							case "DP":
							case "DE":
							case "DT":
								accountResView.getDeposits().add(account);
								break;
							}
						});
			}

			accountResView.setId(unit);
			accountResView.setName("Qatar");
			accountResView.setCurrency("QAR");
			accountResView.setIban("Y");
			accountResView.setUnitTotal("988,999,929,850.46");

			unitList.add(accountResView);
		}
		accountDetailMap.put("globalId", gid);
		accountDetailMap.put("units", unitList);

		response.setData(accountDetailMap);
		response.setStatus(resultUtilVO);

		log.info("AccountServiceImpl:getAccountDetails:end");
		return response;
	}

	@Override
	public GenericResponse<Map<String, Object>> getAccountStmt(String channel, String unit, String lang,
			String serviceId, String auid, String startDate, String endDate) {
		GenericResponse<Map<String, Object>> response = new GenericResponse<>();
		AccountStmtRes accountStmt = mwService.getAccountStmt(channel, unit, lang, serviceId, auid, startDate, endDate);

		return response;
	}

	public AccountDetail formateAccountDetail(String channelId, String unitId, AccountSummary customerDetail,
			AccountModel resAccount) {
		AccountDetail formatedAccount = new AccountDetail();

		formatedAccount.setUid(CommonUtil.generateUniqueID(channelId));
		formatedAccount.setUnitName("");
		formatedAccount.setAccBal(resAccount.getActualBalance());
		formatedAccount.setAvlBal("");
		formatedAccount.setAccBalRaw("");
		formatedAccount.setAccBalBase(BigDecimal.ZERO);
		formatedAccount.setMyIBAN(false);
		formatedAccount.setCurr(resAccount.getCurrency());
		formatedAccount.setAccNum(resAccount.getNumber());
		formatedAccount.setAccNumFormat(CommonUtil.formatAccNum(resAccount.getNumber()));
		formatedAccount.setAccType(resAccount.getType());
		formatedAccount.setAccTypeDesc(resAccount.getDesc());
		formatedAccount.setCustName(customerDetail.getName());
		formatedAccount.setCustType(customerDetail.getType());
		formatedAccount.setLastUsedDate(resAccount.getLastUsedDate());
		formatedAccount.setColorCode("");
		formatedAccount.setUnit(unitId);
		formatedAccount.setMnemonic(customerDetail.getMnemonic());

		return formatedAccount;
	}

	@Override
	public List<FxResponseData> getFxRates(String gId, String unit, String channel, String headerUnit, String serviceId,
			String functionId) {

		log.info("gId: {}, unit: {}, channel: {}, headerUnit: {}, serviceId: {}, functionId: {}", gId, unit, channel,
				headerUnit, serviceId, functionId);

		FxRateRequest fxRateRequest = new FxRateRequest();

		GenericResponse<FxRateResponse> response = mwService.getFxRate(fxRateRequest, unit, channel, "en", serviceId);

		if (response != null && response.getData() != null) {
			return response.getData().getData();
		} else {
			log.error("Failed to get FX rates");
			return null;
		}
	}

	@Override
	public GenericResponse<IBANResponseDto> getIBANInfo(String uid) {
		log.info("Fetching IBAN info for UID: {}", uid);

		GenericResponse<IBANResponseDto> response = mwService.getIBANInfo(uid);

		return response;
	}
	
	@Override
	public GenericResponse<String> generateXLSReport(String unit, String channel, String lang, AccountStmtReqDTO req) {
		ResultUtilVO resultUtilVO = new ResultUtilVO();
		GenericResponse<String> responseData = new GenericResponse<>();
		AppExceptionHandlerUtilDto appExeDto = null;
		ByteArrayOutputStream byteArrayOutStream = null;
		try {
			String cury = "(" + request.getHeader(AccountConstant.CURR) + ")";
			String[] accountSummary = { request.getHeader(AccountConstant.ACCTCARDHOLDERNAME),
					request.getHeader(AccountConstant.ACCNUMFORMAT), request.getHeader(AccountConstant.ACCTYPEDESC),
					request.getHeader(AccountConstant.AVLBAL) + cury,
					request.getHeader(AccountConstant.ACC_ACTUAL_BAL) + cury };

			appExeDto = new AppExceptionHandlerUtilDto(unit, channel, lang, AccountConstant.ACCSTMT);
			// Map<String, String> translationsMap =
			// metadataServiceClient.getPageTranslations(METADATA_GROUP,
			// ACC_STMT_RECEIPT, unit, channel, AppConstants.EN);

			Map<String, String> translationsMap = pageConfigRepository
					.findByUnit_IdAndChannel_ChannelAndLanguage_IdAndPageAndGroup(unit, channel,
							AppConstants.DEFAULT_LANG, AccountConstant.ACC_STMT_RECEIPT,
							AccountConstant.METADATA_GROUP);

			// GenericResponse<AccountStmtResDataDTO> txnDetails = getAccountStmt("PRD",
			// "IB","EN", ACCSTMT, req);
			// GenericResponse<AccountStmtResPendingTxnDataDTO> pendingTxnDetails =
			// getAccountPendingTxnStmt(unit, channel,
			// lang, PENDING_ACCSTMT);
			GenericResponse<AccountStmtResDataDTO> txnDetails = new GenericResponse<>();

			// Create and set AccountStmtResDataDTO
			AccountStmtResDataDTO data = new AccountStmtResDataDTO();
			data.setBalance("32442");
			data.setCcy("ccy");
			data.setAcNo("123456789");

			// Create and set AccountStmtDetailsDTO
			AccountStmtDetailsDTO txnDetail1 = new AccountStmtDetailsDTO();
			txnDetail1.setTxnDate("2023-07-14");
			txnDetail1.setTxnValueDate("2023-07-14");
			txnDetail1.setTxnCode("001");
			txnDetail1.setTxnCodeDesc("Deposit");
			txnDetail1.setTxnType("Credit");
			txnDetail1.setTxnAmount("1000");
			txnDetail1.setRunningBalance("5000");
			txnDetail1.setReferenceChequeNo("12345");
			txnDetail1.setNarrativeOne("Narrative 1");
			txnDetail1.setNarrativeTwo("Narrative 2");
			txnDetail1.setNarrativeThree("Narrative 3");
			txnDetail1.setNarrativeFour("Narrative 4");
			txnDetail1.setSwiftRefNo("SWIFT123");
			txnDetail1.setAuid("AUID123");
			txnDetail1.setWithinOneMonth(true);
			txnDetail1.setShowRepeat(false);
			txnDetail1.setSwiftTxn(true);

			AccountStmtDetailsDTO txnDetail2 = new AccountStmtDetailsDTO();
			txnDetail2.setTxnDate("2023-07-15");
			txnDetail2.setTxnValueDate("2023-07-15");
			txnDetail2.setTxnCode("002");
			txnDetail2.setTxnCodeDesc("Withdrawal");
			txnDetail2.setTxnType("Debit");
			txnDetail2.setTxnAmount("500");
			txnDetail2.setRunningBalance("4500");
			txnDetail2.setReferenceChequeNo("54321");
			txnDetail2.setNarrativeOne("Narrative A");
			txnDetail2.setNarrativeTwo("Narrative B");
			txnDetail2.setNarrativeThree("Narrative C");
			txnDetail2.setNarrativeFour("Narrative D");
			txnDetail2.setSwiftRefNo("SWIFT456");
			txnDetail2.setAuid("AUID456");
			txnDetail2.setWithinOneMonth(false);
			txnDetail2.setShowRepeat(true);
			txnDetail2.setSwiftTxn(false);

			// Add the transaction details to the AccountStmtResDataDTO
			List<AccountStmtDetailsDTO> txnDetailsList = Arrays.asList(txnDetail1, txnDetail2);
			data.setTxnDetails(txnDetailsList);

			// Set the data in the GenericResponse
			txnDetails.setData(data);
			resultUtilVO = new ResultUtilVO(AppConstant.ACT, AppConstant.SUCCESS);
			txnDetails.setStatus(resultUtilVO);

			GenericResponse<AccountStmtResPendingTxnDataDTO> pendingTxnDetails = new GenericResponse<AccountStmtResPendingTxnDataDTO>();
			AccountStmtResPendingTxnDataDTO data1 = new AccountStmtResPendingTxnDataDTO();
			data1.setCcy("USD");
			data1.setAcType("Savings");

			// Create and set AccountPendingTxnStmtDetailsDTO
			AccountPendingTxnStmtDetailsDTO pendingTxnDetails1 = new AccountPendingTxnStmtDetailsDTO();
			pendingTxnDetails1.setHoldReferenceNumber("REF123");
			pendingTxnDetails1.setHoldAmount("1000");
			pendingTxnDetails1.setHoldStartDate("2023-07-01");
			pendingTxnDetails1.setHoldExpiryDate("2023-07-31");
			pendingTxnDetails1.setHoldDeptCode("D001");
			pendingTxnDetails1.setHoldDeptDesc("Department 1");
			pendingTxnDetails1.setHoldReasonCode("R001");
			pendingTxnDetails1.setHoldReasonDesc("Reason 1");
			pendingTxnDetails1.setHoldDescription1("Description 1A");
			pendingTxnDetails1.setHoldDescription2("Description 1B");
			pendingTxnDetails1.setHoldDescription3("Description 1C");
			pendingTxnDetails1.setHoldDescription4("Description 1D");
			pendingTxnDetails1.setHoldDate("2023-07-01");
			pendingTxnDetails1.setTxnType("Type 1");

			AccountPendingTxnStmtDetailsDTO pendingTxnDetails2 = new AccountPendingTxnStmtDetailsDTO();
			pendingTxnDetails2.setHoldReferenceNumber("REF456");
			pendingTxnDetails2.setHoldAmount("500");
			pendingTxnDetails2.setHoldStartDate("2023-07-05");
			pendingTxnDetails2.setHoldExpiryDate("2023-07-25");
			pendingTxnDetails2.setHoldDeptCode("D002");
			pendingTxnDetails2.setHoldDeptDesc("Department 2");
			pendingTxnDetails2.setHoldReasonCode("R002");
			pendingTxnDetails2.setHoldReasonDesc("Reason 2");
			pendingTxnDetails2.setHoldDescription1("Description 2A");
			pendingTxnDetails2.setHoldDescription2("Description 2B");
			pendingTxnDetails2.setHoldDescription3("Description 2C");
			pendingTxnDetails2.setHoldDescription4("Description 2D");
			pendingTxnDetails2.setHoldDate("2023-07-05");
			pendingTxnDetails2.setTxnType("Type 2");

			// Add the transaction details to the AccountStmtResDataDTO
			List<AccountPendingTxnStmtDetailsDTO> txnDetailsList1 = Arrays.asList(pendingTxnDetails1,
					pendingTxnDetails2);
			data1.setTxnDetails(txnDetailsList1);

			// Set the data in the GenericResponse
			pendingTxnDetails.setData(data1);
			resultUtilVO = new ResultUtilVO(AppConstant.ACT, AppConstant.SUCCESS);
			pendingTxnDetails.setStatus(resultUtilVO);

			byteArrayOutStream = new CreateXLS().generateExcel(accountSummary, pendingTxnDetails, txnDetails,
					translationsMap);
			String base64xls = Base64.getEncoder().encodeToString(byteArrayOutStream.toByteArray());
			resultUtilVO.setCode(AppConstants.RESULT_CODE);
			resultUtilVO.setDescription(AppConstants.RESULT_DESC);
			responseData.setStatus(resultUtilVO);
			responseData.setData(base64xls);
		} catch (Exception e) {
			log.error("Exception while downloading accounts statements xls : {}", e);
			responseData.setStatus(resultUtilVO);
			return responseData;
		} finally {
			if (Objects.nonNull(byteArrayOutStream))
				try {
					byteArrayOutStream.flush();
					byteArrayOutStream.close();
				} catch (IOException e) {
					log.error("Exception while downloading accounts statements xls in generateXLSReport: {}",
							e.getMessage());
				}
		}
		return responseData;

	}
	
}
