package com.ocs.accountservice.dto.fxRateResponse;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rate implements Serializable {

	private static final long serialVersionUID = -2795217107958267661L;
	private String retailRateType;
	private String retailRateReciprocal;
	private String buyRate;
	private String sellRate;

}
