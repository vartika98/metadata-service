package com.ocs.accountservice.adapter.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocs.accountservice.adapter.service.AccountService;
import com.ocs.accountservice.constant.AppConstant;
import com.ocs.accountservice.dto.fxRateResponse.FxResponseData;
import com.ocs.accountservice.dto.ibanResponse.IBANResponseDto;
import com.ocs.common.constants.AppConstants;
import com.ocs.common.dto.AccountStmtReqDTO;
import com.ocs.common.dto.GenericResponse;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/acc"+AppConstants.VERSION)
@Slf4j
public class AccountController {

	@Autowired
	private AccountService accountService;

	@PostMapping("/page")
	@ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json", examples = @ExampleObject(value = """
			{
				"kp": "",
				"rk": "bfd0f081-f611-49d5-9144-13a26820804f",
				"ocsEncFlag": "",
				"key": "g1j+QIDAQAB"
			}""")))
	public GenericResponse<Map<String, Object>> getAccountDetails(
			@RequestHeader(name = AppConstant.HEADER_UNIT, required = true) String unit,
			@RequestHeader(name = AppConstant.HEADER_CHANNEL, required = true) String channel,
			@RequestHeader(name = AppConstant.GID, required = true) String gid,
			@RequestHeader(name = AppConstant.HEADER_ACCEPT_LANGUAGE, required = true) String lang,
			@RequestHeader(name = AppConstant.SERVICE_ID, required = true) String serviceId) {
		log.info("AccountController:getAccountDetails:start");
		var res = accountService.getAccountDetails(unit, channel, gid, lang, serviceId);
		log.info("AccountController:getAccountDetails:end");
		return res;
	}

	@PostMapping("/list-fx")
	public List<FxResponseData> getFxRates(@RequestHeader(name = "gId", required = false) String gId,
			@RequestHeader(name = "unitList", required = false) String unit,
			@RequestHeader(name = "channel", required = false) String channel,
			@RequestHeader(name = "unit", required = false) String headerUnit,
			@RequestHeader(name = "serviceId", required = false) String serviceId,
			@RequestHeader(name = "functionId", required = false) String functionId) {

		log.info("gId: {}", gId);

		return accountService.getFxRates(gId, unit, channel, headerUnit, serviceId, functionId);
	}

	@PostMapping("/iban")
	public ResponseEntity<GenericResponse<IBANResponseDto>> getIBANInfo(@RequestBody Map<String, String> requestBody) {
		String uid = requestBody.get("uid");
		log.info("UID: {}", uid);

		GenericResponse<IBANResponseDto> response = accountService.getIBANInfo(uid);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/export")
	public GenericResponse<String> generateXLSReport(
			@RequestHeader(name = AppConstant.HEADER_UNIT, required = true) String unit,
			@RequestHeader(name = AppConstant.HEADER_CHANNEL, required = true) String channel,
			@RequestHeader(name = AppConstant.HEADER_ACCEPT_LANGUAGE, required = true) String lang,
			@RequestHeader(name = AppConstant.SERVICE_ID, required = true) String serviceId,
			@RequestBody AccountStmtReqDTO req ) {
			return accountService.generateXLSReport(unit, channel, lang, req);
	}
}
