package com.ocs.accountservice.adapter.repository;

import org.springframework.stereotype.Repository;

import com.ocs.common.dto.ConfigDto;
import com.ocs.common.mapper.ConfigMapper;
import com.ocs.common.repository.JPAConfigRepository;

@Repository
public class ConfigRepositoryImpl implements ConfigRepository {

	private final JPAConfigRepository jpaConfigRepository;

	private ConfigMapper configMapper;

	public ConfigRepositoryImpl(JPAConfigRepository jpaConfigRepository, ConfigMapper configMapper) {
		this.jpaConfigRepository = jpaConfigRepository;
		this.configMapper = configMapper;
	}

	@Override
	public ConfigDto findByUnit_IdAndChannel_ChannelAndKeyAndStatus(String unit, String channel, String key,
			String status) {
		return configMapper.configToDto(
				jpaConfigRepository.findByUnit_IdAndChannel_ChannelAndKeyAndStatus(unit, channel, key, status));
	}

}