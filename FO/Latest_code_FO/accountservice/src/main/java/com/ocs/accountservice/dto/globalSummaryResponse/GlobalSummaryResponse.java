package com.ocs.accountservice.dto.globalSummaryResponse;

import java.util.List;

import com.ocs.common.dto.ResultUtilVO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GlobalSummaryResponse {

	private Result result;
	private List<ResponseData> data;

}
