package com.ocs.accountservice.adapter.mw.model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerModel implements Serializable {
	private static final long serialVersionUID = 175837392849459L;

	private String nationalId;
	private String mnemonic;
	private String basic;
	private String type;
	private String name;
	private String branch;
	private boolean isDelegate;
	private List<AccountModel> accounts;
}
