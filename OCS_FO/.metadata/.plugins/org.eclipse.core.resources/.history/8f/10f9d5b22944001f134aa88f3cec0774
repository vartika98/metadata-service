package com.ocs.gateway.application.filter;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.ObjectUtils;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.filter.factory.rewrite.ModifyResponseBodyGatewayFilterFactory;
import org.springframework.cloud.gateway.filter.factory.rewrite.RewriteFunction;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ocs.common.constants.AppConstants;
import com.ocs.common.dto.ResultUtilVO;
import com.ocs.common.dto.login.UserAuthContext;
import com.ocs.common.dto.login.UserAuthDetail;
import com.ocs.common.util.CommonUtil;
import com.ocs.common.utils.OCSSec;
import com.ocs.gateway.application.constant.GatewayConstant;
import com.ocs.gateway.application.dto.LoginResponse;
import com.ocs.gateway.application.util.GatewayUtils;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class SessionFilter extends AbstractGatewayFilterFactory<SessionFilter.Config> {

	final Logger logger = LoggerFactory.getLogger(SessionFilter.class);

	private ModifyResponseBodyGatewayFilterFactory modifyResponseBodyFilterFactory;

	public SessionFilter(ModifyResponseBodyGatewayFilterFactory modifyResponseBodyGatewayFilterFactory) {
		super(Config.class);
		this.modifyResponseBodyFilterFactory = modifyResponseBodyGatewayFilterFactory;
	}

	@Override
	public GatewayFilter apply(Config config) {

		return modifyResponseBodyFilterFactory
				.apply(c -> c.setRewriteFunction(String.class, String.class, new Scrubber(config)));
	}

	public static class Config {
	}

	public static class Scrubber implements RewriteFunction<String, String> {

		public Scrubber(Config config) {
		}

		@Override
		@SuppressWarnings("unchecked")
		public Publisher<String> apply(ServerWebExchange exchange, String body) {
			Gson gson = new Gson();
			try {
				log.info("Auth Session FILTER Entered " + body);
				ObjectMapper objectMapper = new ObjectMapper();

				Map<String, Object> map = objectMapper.readValue(body, Map.class);
				LoginResponse loginRes = new LoginResponse();
				HashMap<String, Object> result = (HashMap<String, Object>) map.get("status");

				if (result.get("code").equals(AppConstants.RESULT_CODE)) {
					Map<String, Object> authMap = (Map<String, Object>) GatewayUtils.findJsonObjectByKey(map, "data");

					exchange.getSession().subscribe(session -> {
						String maskedMobileNo = "";
						session.getAttributes().put("VALID_SESSION", true);
						session.setMaxIdleTime(Duration.ofSeconds(GatewayConstant.SESSION_TIMEOUT));
						session.getAttributes().put(GatewayConstant.USER_ID, authMap.get(GatewayConstant.USER_ID));
						session.getAttributes().put(GatewayConstant.USER_NAME, authMap.get(GatewayConstant.USER_NAME));
						session.getAttributes().put(GatewayConstant.USER_NO, authMap.get(GatewayConstant.USER_NO));
						session.getAttributes().put(GatewayConstant.NATIONAL_ID, authMap.get(GatewayConstant.NATIONAL_ID));
						session.getAttributes().put(GatewayConstant.BASE_NO, authMap.get(GatewayConstant.BASE_NO));
						session.getAttributes().put(GatewayConstant.BASE_CURRENCY, authMap.get(GatewayConstant.BASE_CURRENCY));
						session.getAttributes().put(GatewayConstant.GID, authMap.get(GatewayConstant.GID));
						session.getAttributes().put(GatewayConstant.FIRST_NAME, authMap.get(GatewayConstant.FIRST_NAME));
						session.getAttributes().put(GatewayConstant.LAST_NAME, authMap.get(GatewayConstant.LAST_NAME));
						session.getAttributes().put(GatewayConstant.FULL_NAME, authMap.get(GatewayConstant.FULL_NAME));
						session.getAttributes().put(GatewayConstant.PREFERRED_UNIT, authMap.get(GatewayConstant.PREFERRED_UNIT));
						session.getAttributes().put(GatewayConstant.COUNTRY_CODE, authMap.get(GatewayConstant.COUNTRY_CODE));
						session.getAttributes().put(GatewayConstant.USER_MOBILENO, authMap.get(GatewayConstant.USER_MOBILENO));
						session.getAttributes().put(GatewayConstant.USER_EMAIL, authMap.get(GatewayConstant.USER_EMAIL));
						session.getAttributes().put(GatewayConstant.IS_OTP_REQUIRED, authMap.get(GatewayConstant.IS_OTP_REQUIRED));
						session.getAttributes().put(GatewayConstant.OTP_REF_NO, authMap.get(GatewayConstant.OTP_REF_NO));
						session.getAttributes().put(GatewayConstant.NATIONALIDEXPIRYDATE,
								authMap.get(GatewayConstant.NATIONALIDEXPIRYDATE));
						session.getAttributes().put(GatewayConstant.CUST_SEGMENT, authMap.get(GatewayConstant.CUST_SEGMENT));
						session.getAttributes().put(GatewayConstant.CUST_TYPE, authMap.get(GatewayConstant.CUST_TYPE));
						session.getAttributes().put(authMap.get(GatewayConstant.USER_NO) + GatewayConstant.IS_OTP_REQUIRED,
								authMap.get(GatewayConstant.IS_OTP_REQUIRED));
						session.getAttributes().put(GatewayConstant.ACCESS_TOKEN, authMap.get(GatewayConstant.AKTN));
						if (AppConstants.Y.equals(authMap.get(GatewayConstant.IS_OTP_REQUIRED)) && AppConstants.MB_CHANNEL_ID
								.equals(exchange.getRequest().getHeaders().get(GatewayConstant.CHANNEL))) {
							session.getAttributes().put(authMap.get(GatewayConstant.USER_NO) + GatewayConstant.IS_LOGIN_CALLED,
									AppConstants.Y);
						}
						try {
//							authMap.put("atkn", OCSSec.signJWT(authMap.get("atkn").toString()));
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (Objects.nonNull(authMap.get(GatewayConstant.USER_MOBILENO)) && Objects.nonNull(GatewayConstant.USER_MOBILENO)
								&& !ObjectUtils.isEmpty(authMap.get(GatewayConstant.USER_MOBILENO))) {
							 maskedMobileNo =  CommonUtil.maskedMobileNoWithCountryCode(String.valueOf(authMap.get(GatewayConstant.USER_MOBILENO)), String.valueOf(authMap.get(GatewayConstant.COUNTRY_CODE)));
						}
						final UserAuthContext userContext = new UserAuthContext(String.valueOf(authMap.get(GatewayConstant.FULL_NAME)),
								String.valueOf(authMap.get(GatewayConstant.FIRST_NAME)), String.valueOf(authMap.get(GatewayConstant.LAST_NAME)), String.valueOf(authMap.get(GatewayConstant.LAST_LGN_TIME)),
								String.valueOf(authMap.get(GatewayConstant.PREFERRED_UNIT)), String.valueOf(authMap.get(GatewayConstant.PREFFERED_LANG)),
								String.valueOf(authMap.get(GatewayConstant.REGISTERED_UNIT)), String.valueOf(authMap.get(GatewayConstant.IS_OTP_REQUIRED)),String.valueOf(authMap.get(GatewayConstant.OTP_REF_NO)),
								String.valueOf(authMap.get(GatewayConstant.ISPWDEXPIRED)), String.valueOf(authMap.get(GatewayConstant.NATIONALIDEXPIRYDATE)),
								String.valueOf(authMap.get(GatewayConstant.NATIONAL_ID)), maskedMobileNo, String.valueOf(authMap.get(GatewayConstant.USER_NAME)),
								String.valueOf(authMap.get(GatewayConstant.USER_NO)), String.valueOf(authMap.get(GatewayConstant.GID)), String.valueOf(authMap.get(GatewayConstant.CUST_SEGMENT)),
								String.valueOf(authMap.get(GatewayConstant.CUST_TYPE)), String.valueOf(authMap.get(GatewayConstant.FAR)), String.valueOf(authMap.get(GatewayConstant.IDTYPE)));
						session.getAttributes().put("userContext", userContext);
//						LoginResponse loginRes = new LoginResponse();
						loginRes.setData(mappingLoginRes(authMap,maskedMobileNo));
						session.changeSessionId().subscribe();
						
					});
					loginRes.setStatus(new ResultUtilVO(String.valueOf(result.get("code")), String.valueOf(result.get("description"))));
					map = objectMapper.convertValue(loginRes, Map.class);
					String response = gson.toJson(map, Map.class);
					if (GatewayUtils.secureReqResponse("")) {
						response = OCSSec.encrypt(response);
					}
					return Mono.just(response);
				} else {
					return Mono.just(gson.toJson(map, Map.class));
				}

			} catch (Exception ex) {
				log.error("Error occured in session filter " + ex.getMessage());
				return Mono.error(new Exception("json process fail", ex));
			}
		}
	}

	private static UserAuthDetail mappingLoginRes(Map<String, Object> authMap, String maskedMobileNo) {
		UserAuthDetail mappedRes = new UserAuthDetail(String.valueOf(authMap.get("atkn")),String.valueOf(authMap.get("reftkn")),String.valueOf(authMap.get("ttype")),
				String.valueOf(authMap.get("preferredUnit")),String.valueOf(authMap.get("prefLanguage")),String.valueOf(authMap.get("registeredUnit")),String.valueOf(authMap.get("isPassExpired")),
				String.valueOf(authMap.get("isOtpRequired")), String.valueOf(authMap.get("isNationalIdExpired")), String.valueOf(authMap.get("userName")),
				String.valueOf(authMap.get("customerSegment")),String.valueOf(authMap.get("customerType")), String.valueOf(authMap.get("far")) ,
				String.valueOf(authMap.get("mbCode")), String.valueOf(authMap.get("mobileNo")),
				String.valueOf(authMap.get("userNo")), String.valueOf(authMap.get("gid")),
				String.valueOf(authMap.get("otpLength")), String.valueOf(authMap.get("otpType")),String.valueOf(authMap.get("emailId")),
				String.valueOf(authMap.get("nationalId")), String.valueOf(authMap.get("baseNo")), String.valueOf(authMap.get("idType")),
				maskedMobileNo);
		return mappedRes;
		

	}
}
