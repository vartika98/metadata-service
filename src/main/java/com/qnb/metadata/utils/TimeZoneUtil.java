package com.qnb.metadata.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

import org.springframework.http.HttpStatus;

import com.qnb.metadata.exception.ServiceException;

public class TimeZoneUtil {
	private TimeZoneUtil() {
	}

	public static final String DD_MM_YYYY_HH_MM = "dd-MM-yyyy HH:mm";

//	public static List<TimeZone> getUTCTimeZoneMap() {
//		return getTimeZoneMap("UTC");
//	}
//
//	public static List<TimeZone> getGMTTimeZoneMap() {
//		return getTimeZoneMap("GMT");
//	}

//	private static List<TimeZone> getTimeZoneMap(String base) {
//		LocalDateTime now = LocalDateTime.now();
//		return ZoneId.getAvailableZoneIds().stream().map(ZoneId::of).sorted(new ZoneComparator())
//				.map(id -> TimeZone.builder().code(id.getId()).type(base)
//						.displayInfo(String.format("(%s%s)%s", base, getOffset(now, id), id.getId())).build())
//				.collect(Collectors.toList());
//	}

	private static String getOffset(LocalDateTime dateTime, ZoneId id) {
		return dateTime.atZone(id).getOffset().getId().replace("Z", "+00:00");
	}

	private static class ZoneComparator implements Comparator<ZoneId> {
		@Override
		public int compare(ZoneId zoneId1, ZoneId zoneId2) {
			LocalDateTime now = LocalDateTime.now();
			ZoneOffset offset1 = now.atZone(zoneId1).getOffset();
			ZoneOffset offset2 = now.atZone(zoneId2).getOffset();
			return offset1.compareTo(offset2);
		}
	}

	public static Instant getUtcTime(String dateTime, String zoneId) {
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DD_MM_YYYY_HH_MM);
			ZonedDateTime ztime = LocalDateTime.parse(dateTime, dtf).atZone(ZoneId.of(zoneId));
			return ztime.withZoneSameInstant(ZoneOffset.UTC).toInstant();
		} catch (Exception e) {
			throw new ServiceException("DT001",
					String.format("Error in converting date to format dd-MM-yyyy HH:mm with zone %s", zoneId),
					HttpStatus.BAD_REQUEST, e);
		}
	}
}
