package com.qnb.metadata.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.qnb.metadata.exception.WorkflowException;

public class SHA3Hashing {

	private static final String SHA3_256 = "SHA3-256";

	private SHA3Hashing() {
	}

	public static String hashWithMessageDigest(String msg) {
		try {
			final MessageDigest digest = MessageDigest.getInstance(SHA3_256);
			final byte[] hashBytes = digest.digest(msg.getBytes(StandardCharsets.UTF_8));
			return HashingCommonUtil.bytesToString(hashBytes);
		} catch (NoSuchAlgorithmException e) {
			throw new WorkflowException("Exception in calculating hash value");
		}

	}
}
