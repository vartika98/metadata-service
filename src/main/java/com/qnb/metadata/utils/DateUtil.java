package com.qnb.metadata.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateUtil {
    public static String DD_MM_YYYY = "dd-MM-yyyy";
 
    public static Date getDate(String date) {
        return getDate(date, DD_MM_YYYY);
    }
 
    public static Date getDate(String date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            //TODO: check what needs to be done
        }
        return null;
    }
 
    public static String dateInStringFormat(Date date) {
        return dateInStringFormat(date, DD_MM_YYYY);
    }
 
    public static String dateInStringFormat(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
    
    public static Date getDateWithFormat(String date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(DD_MM_YYYY, Locale.ENGLISH);
        try {
            Date dte = formatter.parse(date);
            SimpleDateFormat dteFormatter = new SimpleDateFormat(DD_MM_YYYY, Locale.ENGLISH);
            String dateString = dteFormatter.format(dte);
            return dteFormatter.parse(dateString);
        } catch (ParseException e) {
        	log.error("parse ecxeption "+ e);
        }
        return null;
    }
}
