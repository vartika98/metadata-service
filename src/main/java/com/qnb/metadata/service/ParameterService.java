package com.qnb.metadata.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.qnb.metadata.dto.parameter.ParameterRequestGetResponse;
import com.qnb.metadata.model.GenericResponse;

@Service
public interface ParameterService {
	
	GenericResponse<List<ParameterRequestGetResponse>> getParameterRequestsByIds(List<String> list);

}
