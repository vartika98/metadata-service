package com.qnb.metadata.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.qnb.metadata.dto.appMaintenance.ApplicationRequestResponse;
import com.qnb.metadata.dto.parameter.ParameterRequestGetResponse;
import com.qnb.metadata.model.GenericResponse;

@Service
public interface ApplicationMaintenanceService {
	
	GenericResponse<List<ApplicationRequestResponse>> getApplicationRequestByIds(List<String> requestIds);
}
