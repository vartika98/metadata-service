package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.errorConfig.ErrorConfigRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface ErrorConfigService {

	GenericResponse<List<ErrorConfigRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody);

}
