package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.creditCard.CCRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface CreditCardService {

	GenericResponse<List<CCRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody);

}
