package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.ipo.IPORequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface IPOService {

	GenericResponse<List<IPORequestResponse>> getPendingRequestLst(Map<String, List<String>> reqBody);

}
