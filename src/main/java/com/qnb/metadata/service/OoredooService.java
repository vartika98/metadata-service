package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.ooredoo.OoredooRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface OoredooService {

	GenericResponse<List<OoredooRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody);

}
