package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.ChannelListRes;
import com.qnb.metadata.dto.channel.ChannelRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface ChannelService {


	GenericResponse<ChannelListRes> getAllChannels(String defaultStatus);

	GenericResponse<List<ChannelRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody);

}