package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.role.GroupRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface RoleService {

	GenericResponse<List<GroupRequestResponse>> getRoleRequest(Map<String, List<String>> reqBody);

}
