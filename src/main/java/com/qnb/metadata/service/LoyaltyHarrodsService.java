package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.LoyaltyHarrods.LHPaymentRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface LoyaltyHarrodsService {

	GenericResponse<List<LHPaymentRequestResponse>> getPendingRequestLst(Map<String, List<String>> reqBody);

}
