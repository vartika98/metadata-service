package com.qnb.metadata.service;

import static com.qnb.metadata.enums.ExceptionMessages.PARAMETER_REQUEST_EXISTS_EXCEPTION;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.qnb.metadata.dto.parameter.ParameterDetails;
import com.qnb.metadata.entity.ParamConfigEntity;
import com.qnb.metadata.enums.TxnStatus;
import com.qnb.metadata.exception.ValidationException;

@Service
public class ParameterServiceHelper {
	
	public static void setActionForParameterRequest(ParameterDetails paramReq, List<ParamConfigEntity> existingParamList) {
		paramReq.getParamDetails().forEach(reqParam -> {
			List<ParamConfigEntity> paramDetails = existingParamList.stream()
					.filter(pm -> pm.getTxnId().equals(reqParam.getTxnId()))
					.collect(Collectors.toList());
			if (paramDetails.isEmpty()) {
				reqParam.setAction("ADD");
			} else {
				if ((!paramDetails.get(0).getConfKey().equals(reqParam.getConfKey())
						|| !paramDetails.get(0).getConfValue().equals(reqParam.getConfValue())
						|| (paramDetails.get(0).getRemarks()==null && reqParam.getRemarks() !=null ))
						|| !paramDetails.get(0).getRemarks().equals(reqParam.getRemarks())
						&& !reqParam.getStatus().equals(TxnStatus.IAC)) {
					reqParam.setAction("MODIFY");
				} else if (reqParam.getStatus().equals(TxnStatus.IAC)){
					reqParam.setAction("DELETE");
				} else {
					reqParam.setAction("NO ACTION");
				}
			}
		});
	}
	
	public void checkParameterExists(ParameterDetails paramReq, List<ParamConfigEntity> existingParamList) {
		Set<String> duplicateParams = new HashSet<>();
		paramReq.getParamDetails().forEach(req->{
			List<ParamConfigEntity> duplicate = existingParamList.stream()
					.filter(exist -> (!exist.getTxnId().equals(req.getTxnId())
							&& exist.getConfKey().equals(req.getConfKey())
							&& exist.getConfValue().equals(req.getConfValue())
							))
					.collect(Collectors.toList());
			if(!duplicate.isEmpty()) {
				duplicateParams.add(req.getConfKey());
			}
		});
		if(!duplicateParams.isEmpty()) {
			throw new ValidationException(PARAMETER_REQUEST_EXISTS_EXCEPTION.getErrorCode(),
					String.format("Parameter with key and value pair already exists in the request for the keys: %s", String.join(", ", duplicateParams)), HttpStatus.CONFLICT);
		}
	}
}
