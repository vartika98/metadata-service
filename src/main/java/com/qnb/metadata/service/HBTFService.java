package com.qnb.metadata.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.qnb.metadata.dto.hbtf.HBTFRequestGetResponse;
import com.qnb.metadata.model.GenericResponse;

@Service
public interface HBTFService {
	
	public GenericResponse<List<HBTFRequestGetResponse>> getHBTFRequestByIds(List<String> requestIdLst);
}
