package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.service.workflow.Workflowable;

public interface WorkflowService {

	public List<Message> requestDetails(Map<String,Workflowable> reqObject,List<String> requestIds);
}
