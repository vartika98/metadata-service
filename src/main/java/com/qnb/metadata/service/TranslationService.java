package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.LabelListRes;
import com.qnb.metadata.dto.LabelResponseDto;
import com.qnb.metadata.dto.ManageLabelReqDto;
import com.qnb.metadata.dto.ManageMenuReqDto;
import com.qnb.metadata.dto.MenuListRes;
import com.qnb.metadata.dto.MenuResponseDto;
import com.qnb.metadata.dto.entitlement.TxnEntitlementRequestGetResponse;
import com.qnb.metadata.dto.label.LabelRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface TranslationService {

	GenericResponse<MenuListRes> moduleList(Map<String, String> reqBody);

	GenericResponse<MenuListRes> screenList(Map<String, String> reqBody);

	GenericResponse<LabelListRes> labelList(Map<String, String> reqBody);

	GenericResponse<Map<String, String>> manageLabels(ManageLabelReqDto reqBody);

	GenericResponse<Map<String, String>> manageMenus(ManageMenuReqDto reqBody);

	GenericResponse<List<TxnEntitlementRequestGetResponse>> getEntitlementRequest(Map<String, List<String>> reqBody);

	GenericResponse<List<LabelRequestResponse>> getLabelRequest(Map<String, List<String>> reqBody);

}
