package com.qnb.metadata.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.qnb.metadata.dto.branchList.BranchListResponse;
import com.qnb.metadata.model.GenericResponse;

@Service
public interface BranchListService {

	GenericResponse<List<BranchListResponse>> getBranchListRequestByIds(List<String> requestIdList);
}
