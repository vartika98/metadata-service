package com.qnb.metadata.service;

import java.util.List;

import com.qnb.metadata.dto.UnitDescDto;
import com.qnb.metadata.dto.UnitDto;
import com.qnb.metadata.dto.UnitListRes;
import com.qnb.metadata.dto.UnitMasterDto;
import com.qnb.metadata.entity.Unit;
import com.qnb.metadata.model.GenericResponse;

public interface UnitService {

	GenericResponse<UnitListRes> getAllUnits(String unitStatus);


}
