package com.qnb.metadata.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.qnb.metadata.dto.b2b.B2BResponse;
import com.qnb.metadata.model.GenericResponse;

@Service
public interface B2BService {
	
	GenericResponse<List<B2BResponse>> getB2BRequestByIds(List<String> reqestList); 
}
