package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.user.DeleteUserRequestResponse;
import com.qnb.metadata.dto.user.UserRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface UserService {

	GenericResponse<List<UserRequestResponse>> getUserRequest(Map<String, List<String>> reqBody);

	GenericResponse<List<DeleteUserRequestResponse>> getDeleteUserRequest(Map<String, List<String>> reqBody);

}
