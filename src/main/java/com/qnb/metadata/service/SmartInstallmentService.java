package com.qnb.metadata.service;

import java.util.List;
import java.util.Map;

import com.qnb.metadata.dto.smartInstallment.SmartInstRequestResponse;
import com.qnb.metadata.model.GenericResponse;

public interface SmartInstallmentService {

	GenericResponse<List<SmartInstRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody);

}
