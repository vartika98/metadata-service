package com.qnb.metadata.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.qnb.metadata.dto.appMaintenance.MaintenanceRequestDetailsResponse;
import com.qnb.metadata.model.GenericResponse;

@Service
public interface MessageMaintenanceService {
	
	GenericResponse<List<MaintenanceRequestDetailsResponse>> getMaintenanceRequestsByIds(List<String> requestIds);
}
