package com.qnb.metadata.entity;



import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.dto.user.UnitMaster;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_UNIT_MASTER)
public class Unit extends BaseModel {
 
	private static final long serialVersionUID = 8615705817557328343L;
 
	
	@Column(name = "TXN_ID", length = 3)
	private String id;
 
	@Id
	@Column(name = "UNIT_ID",unique=true)
	private String unitId;
	
	@Column(name = "Remarks", nullable = true, length = 30)
	private String description;
 
	@Column(name = "COLOR", nullable = false, length = 30)
	private String color;
 
	@Column(name = "UNIT_DESC", nullable = false, length = 30)
	private String unitDesc;
 
	@OneToOne
    @JoinColumn(name = "COUNTRY_CODE", referencedColumnName = "COUNTRY_CODE",unique=true)
	private Country countryCode;
 
	@Column(name = "TIME_ZONE")
	private String timeZone;
 
	@Column(name = "BASE_CURRENCY", nullable = false, length = 3)
	private String baseCur;
 
	@Column(name = "CURRENCY_2", nullable = false, length = 3)
	private String cur2;
 
	@Column(name = "CURRENCY_3", nullable = false, length = 3)
	private String cur3;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "IS_IBAN")
	private String iban;
	
	@Column(name = "BRANCH_CODE")
	private String branchCode;
	
	@Column(name = "ORDER_BY")
	private int orderBy;
	
	@Column(name = "COUNTRY_DESC")
	private String countryDesc;
 
	public Unit(String id) {
		super();
		this.id = id;
	}
	
	 public static Unit toEntity(String unitId) {
		 Unit entity = new Unit();
	     entity.setUnitId(unitId);
	     return entity;
	}
 
}
