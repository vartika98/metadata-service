package com.qnb.metadata.entity;

import java.sql.Types;

import org.hibernate.annotations.JdbcTypeCode;

import com.qnb.metadata.dto.b2b.B2BMaster;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_HDFC_STAGING")
public class B2BStagingEntity extends AbstractAuditEntity {
 
	private static final long serialVersionUID = -2448478143296517333L;
 
	@Id
	@Column(name = "REQUEST_ID")
	private String requestId;
 
	@Column(name = "USER_NO")
	@JdbcTypeCode(Types.VARBINARY)
	private byte[] userNo;
 
	@Column(name = "GLOBAL_ID")
	private String globalId;
 
	@Column(name = "USER_ID")
	private String userId;
 
	@Column(name = "REQUEST_COMMENTS")
	private String requestComments;
 
	@Column(name = "WORKFLOW_ID")
	private String workflowId;
 
	@Column(name = "UNIT_ID")
	private String unitId;
 
	@Column(name = "STATUS")
	private String status;
 
	@Column(name = "REF_NO")
	private String refNo;
 
	@Column(name = "DEBIT_ACC_NO")
	private String debitAccountNo;
 
	@Column(name = "REQUESTED_BY")
	private String requestedBy;
 
	@Column(name = "REQUESTED_DATE")
	private String requestedDate;
 
	@Column(name = "REJECTED_BY")
	private String rejectedBy;
 
	@Column(name = "REJECTED_DATE")
	private String rejectedDate;
 
	@Column(name = "REJECT_COUNT")
	private int rejectCount;
 
	@Column(name = "CHECKER_COMMENTS")
	private String checkerComments;
 
	@Column(name = "BENF_NAME")
	private String benfName;
 
	@Column(name = "BENF_BANK_NAME")
	private String benfBankName;
 
	@Column(name = "BENF_ACNT_NO")
	private String benfAccountNo;
 
	@Column(name = "CREDIT_AMOUNT")
	private String creditAmount;
 
	@Column(name = "DEBIT_AMOUNT")
	private String debitAmount;
 
	@Column(name = "SELL_RATE")
	private double sellRate;
 
	public static B2BStagingEntity toEntity(B2BMaster b2bMaster, String requestId, String workflowId,
			String createdBy) {
		B2BStagingEntity entity = new B2BStagingEntity();
		entity.setUserId(b2bMaster.getUserId());
		entity.setGlobalId(b2bMaster.getGlobalId());
		entity.setDebitAccountNo(b2bMaster.getDebitAccountNo());
		entity.setRefNo(b2bMaster.getRefNo());
		entity.setRequestComments(b2bMaster.getRequestComments());
		entity.setRequestedBy(b2bMaster.getRequestedBy());
		entity.setRequestedDate(b2bMaster.getRequestedDate());
		entity.setUnitId(b2bMaster.getUnitId());
		entity.setStatus(b2bMaster.getStatus());
		entity.setRejectedBy(b2bMaster.getRejectedBy());
		entity.setRejectedDate(b2bMaster.getRejectedDate());
		entity.setRejectCount(b2bMaster.getRejectCount());
		entity.setWorkflowId(workflowId);
		entity.setCreatedBy(createdBy);
		entity.setRequestId(requestId);
		entity.setBenfAccountNo(b2bMaster.getBenfAccountNo());
		entity.setBenfName(b2bMaster.getBenfName());
		entity.setBenfBankName(b2bMaster.getBenfBankName());
		entity.setCreditAmount(b2bMaster.getCreditAmount());
		entity.setDebitAmount(b2bMaster.getDebitAmount());
		entity.setSellRate(b2bMaster.getSellRate());
		entity.setUserNo(b2bMaster.getUserNo());
 
		return entity;
	}
 
}
