package com.qnb.metadata.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_SMARTINSTALLMENT_STAGING")
public class SmartInstStagingEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "REQUEST_ID")
	private String requestId;

	@Column(name = "LOGINIP")
	private String loginIp;

	@Column(name = "LOGINUSERID")
	private String loginUserId;

	@Column(name = "GLOBALID")
	private String globalId;

	@Column(name = "USER_ID")
	private String userId;

	@Column(name = "REQUEST_COMMENTS")
	private String requestComments;

	@Column(name = "WORKFLOW_ID")
	private String workflowId;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "CHANNEL_ID")
	private String channel;

	@Column(name = "INST_REFNO")
	private String instRefNo;

	@Column(name = "SORT_KEY")
	private String sortKey;

	@Column(name = "KEY")
	private String key;

	@Column(name = "VALUE")
	private String value;

	@Column(name = "PLAN_SER_NO")
	private String planSerNo;

	@Column(name = "FEE_PERCENTAGE")
	private String feePercentage;

	@Column(name = "TXN_AMOUNT")
	private String txnAmount;

	@Column(name = "AVAIL_BAL")
	private String availBal;

	@Column(name = "TOTAL_OTB")
	private String totalOTB;

	@Column(name = "TOTAL_DEBOT_OS")
	private String totalDebitOS;

	@Column(name = "TOTAL_INST_OTB")
	private String totalInstOTB;

	@Column(name = "TXN_SER_NO")
	private String txnSerNo;

	@Column(name = "CRD_LIMIT")
	private String cardLimit;

	@Column(name = "ENCARD_NO")
	private String encCardNumber;

	@Column(name = "TXN_PARTITION_KEY")
	private String trxnPartitionKey;

	@Column(name = "CRD_NO")
	private String cardNo;

	@Column(name = "FEE_AMOUNT")
	private String feeAmt;

	@Column(name = "CREATED_DATE")
	private String createdDate;

	@Column(name = "INST_AMOUNT")
	private String instAmt;

	@Column(name = "TXN_DATE")
	private String txnDate;

	@Column(name = "TXN_DESC")
	private String txnDesc;
}
