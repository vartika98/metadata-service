package com.qnb.metadata.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.model.BaseModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({ "id", "menu", "desc", "status", "createdBy", "createdTime", "modifiedBy", "modifiedTime",
		"unit", "channel", "screen", })
@Table(name = TableConstants.TABLE_SCREEN_ENT, uniqueConstraints = {
		@UniqueConstraint(columnNames = { "UNIT_ID", "CHANNEL_ID", "MENU_ID", "SCREEN_ID", "WIDGET_KEY" }) })
public class ScreenEntitlement extends BaseModel {

	private static final long serialVersionUID = -2871482094841249567L;

	@Id
	@Column(name = "TXN_ID", length = 20)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIT_ID", referencedColumnName = "UNIT_ID")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID", insertable = false, updatable = false, nullable = false)
	private Channel channel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MENU_ID")
	private Menu menu;

	@Column(name = "SCREEN_ID", nullable = false)
	private String screen;

	@Column(name = "WIDGET_KEY", nullable = false)
	private String widget;

	@Column(name = "IS_ENABLED", nullable = false)
	private String enabled;

	@Column(name = "DESCRIPTION", nullable = true)
	private String desc;

}
