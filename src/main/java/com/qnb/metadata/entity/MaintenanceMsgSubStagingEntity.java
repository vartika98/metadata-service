package com.qnb.metadata.entity;

import java.util.Date;

import com.qnb.metadata.dto.appMaintenance.MaintenanceAppDetail;
import com.qnb.metadata.dto.appMaintenance.MaintenanceDetail;
import com.qnb.metadata.enums.TxnStatus;
import com.qnb.metadata.utils.RandomUuidGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="BKO_T_MAINTENANCE_MSG_SUB_STAGING")
public class MaintenanceMsgSubStagingEntity extends AbstractAuditEntity {
	/**
	 *
	 */
	private static final long serialVersionUID = -6103570800202702844L;
 
	@Id
	@Column(name="ID")				
	private String id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REQUEST_ID")
	private MaintenanceMsgStagingEntity stagingEntity;
	
	@Column(name = "REF_NO")
	private String refNo;
	
	@Column(name = "SERIAL_NO")
	private String serilaNum;
	
	@Column(name="APP_ID")
	private Long appId;
	
	@Column(name = "APPLICATION")
	private String application;
	
	@Column(name = "UNIT_ID")
	private String unitId;
	
	@Column(name = "CHANNEL_ID")
	private String channelId;
 
	@Column(name = "OUTAGE_START_DATE_TIME")
	private Date outageStartDateTime;
	
	@Column(name = "OUTAGE_END_DATE_TIME")
	private Date outageEndDateTime;
	
	@Column(name="STATUS")
	@Enumerated(EnumType.STRING)
	private TxnStatus status;
	
 
	public static MaintenanceMsgSubStagingEntity toEntity(MaintenanceDetail details, MaintenanceMsgStagingEntity msgStaging) {
		MaintenanceMsgSubStagingEntity msgSubStaging = new MaintenanceMsgSubStagingEntity();
		msgSubStaging.setId(RandomUuidGenerator.uUIDAsString());
		msgSubStaging.setStagingEntity(msgStaging);
		msgSubStaging.setRefNo(details.getRefNo());
		msgSubStaging.setApplication(details.getApplication());
		msgSubStaging.setChannelId(details.getChannelIds());
		msgSubStaging.setUnitId(details.getUnitId());
		msgSubStaging.setAppId(details.getAppId());
		msgSubStaging.setOutageStartDateTime(details.getOutageStartDateTime());
		msgSubStaging.setOutageEndDateTime(details.getOutageEndDateTime());
		msgSubStaging.setStatus(details.getStatus());
		
		return msgSubStaging;
	}
	
	public static MaintenanceMsgSubStagingEntity toEntity(MaintenanceAppDetail details, MaintenanceMsgStagingEntity msgStaging) {
		MaintenanceMsgSubStagingEntity msgSubStaging = new MaintenanceMsgSubStagingEntity();
		msgSubStaging.setId(RandomUuidGenerator.uUIDAsString()); 
		msgSubStaging.setStagingEntity(msgStaging);
		msgSubStaging.setRefNo(details.getRefNo() != null ? details.getRefNo().toString() : null);
		msgSubStaging.setApplication(details.getApplication());
		msgSubStaging.setChannelId(details.getChannelIds());
		msgSubStaging.setUnitId(details.getUnitId());
		msgSubStaging.setAppId(details.getAppId());
		msgSubStaging.setOutageStartDateTime(details.getOutageStartDateTime());
		msgSubStaging.setOutageEndDateTime(details.getOutageEndDateTime());
		msgSubStaging.setStatus(details.getStatus());
		
		return msgSubStaging;
	}
}
