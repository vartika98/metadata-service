package com.qnb.metadata.entity;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
 
 
import org.apache.commons.lang3.StringUtils;

import com.qnb.metadata.dto.user.User;
import com.qnb.metadata.enums.UserStatus;
import com.qnb.metadata.enums.UserType;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
 
 
@Getter
@Setter 
@Entity  
@Table(name = "BKO_T_USER")
public class UserEntity extends AbstractAuditEntity {
 
	private static final long serialVersionUID = -5161840300649570621L;
 
	@Id
    @Column(name = "user_id")
    private String userId;
 
    @OneToOne(mappedBy = "userEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private UserAuthEntity userAuthEntity;
 
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<UserGroupsMembershipEntity> groups;
 
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<UserUnitsMembershipEntity> units;
    @Column(name = "first_name")
    private String firstName;
 
    @Column(name = "last_name")
    private String lastName;
 
    @Column(name = "expiry_date")
    @Temporal(TemporalType.DATE)
    private Date expiryDate;
 
    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    private Date dob;
 
    @Column(name = "mobile_no")
    private String mobileNumber;
 
    @Column(name = "email_id")
    private String emailId;
 
    @Column(name = "user_status")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;
 
    @Column(name="user_type")
    @Enumerated(EnumType.STRING)
    private UserType userType;
 
    @Column(name = "created_source_ip")
    private String createdSourceIp;
 
 
    public static UserEntity toEntity(User user) {
        UserEntity userEntity = new UserEntity();
        var userAuthEntity = UserAuthEntity.toEntity(user.getUserAuth(), user.getUserId());
        userEntity.userId = user.getUserId();
        userEntity.firstName = user.getFirstName();
        userEntity.lastName = user.getLastName() != null ? user.getLastName() : " ";
        userEntity.expiryDate = user.getExpiryDate();
        userEntity.dob = user.getDob();
        userEntity.emailId = user.getEmailId();
        userEntity.mobileNumber = user.getMobileNumber();
        userEntity.userStatus = user.getUserStatus();
        userEntity.userType = UserType.FUNCTIONAL;
        userEntity.createdSourceIp = "1:1:1:1";
        userEntity.userAuthEntity = userAuthEntity;
        if (user.getGroups() != null) {
            userEntity.groups = user.getGroups().stream().filter(StringUtils::isNotBlank)
                    .map(userGroup -> UserGroupsMembershipEntity.toEntity(userGroup, userEntity))
                    .collect(Collectors.toSet());
        }
        if(user.getUnits() != null) {
        	userEntity.units = user.getUnits().stream().filter(StringUtils::isNotBlank)
        			.map(userUnit -> UserUnitsMembershipEntity.toEntity(userUnit, userEntity))
        			.collect(Collectors.toSet());
        }
        return userEntity;
    }
    public static UserEntity updateEntity(UserEntity entity, User user) {
    	entity.expiryDate = user.getExpiryDate();
    	// modify groups
        var groupsToRemove = entity.groups.stream()
                .filter(r -> !user.getGroups().contains(r.getGroupId().getGroupId()))
                .collect(Collectors.toSet());
        entity.groups.removeAll(groupsToRemove);
 
        var updatedGroupIds = entity.getGroups().stream().map(g->g.getGroupId().getGroupId()).collect(Collectors.toSet());
 
        var neGroupsTobeAdded = user.getGroups().stream().filter(r -> !updatedGroupIds.contains(r))
                .map(newGroupToMap -> UserGroupsMembershipEntity.toEntity(newGroupToMap, entity))
                .collect(Collectors.toSet());
        entity.groups.addAll(neGroupsTobeAdded);
        // modify units
        var unitsToRemove = entity.units.stream()
                .filter(r -> !user.getUnits().contains(r.getUnitId().getUnitId()))
                .collect(Collectors.toSet());
        entity.units.removeAll(unitsToRemove);
 
        var updatedUnitIds = entity.units.stream().map(g->g.getUnitId().getUnitId()).collect(Collectors.toSet());
 
        var newUnitsTobeAdded = user.getUnits().stream().filter(r -> !updatedUnitIds.contains(r))
                .map(newUnitToMap -> UserUnitsMembershipEntity.toEntity(newUnitToMap, entity))
                .collect(Collectors.toSet());
        entity.units.addAll(newUnitsTobeAdded);
 
        return entity;
    }
}
