package com.qnb.metadata.entity;

import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_LANGUAGE_MASTER)
public class Language extends BaseModel {

	private static final long serialVersionUID = 8615705817557328343L;

	@Id
	@Column(name = "LANG_CODE", nullable = false, length = 2)
	private String id;

	@Column(name = "LANG_DESC", nullable = false, length = 15)
	private String title;

	@Column(name = "REMARKS", nullable = true, length = 30)
	private String description;

}
