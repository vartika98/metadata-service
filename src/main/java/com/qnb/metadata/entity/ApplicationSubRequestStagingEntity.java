package com.qnb.metadata.entity;


import com.qnb.metadata.dto.appMaintenance.ApplicationDetails;
import com.qnb.metadata.utils.RandomUuidGenerator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;

@Getter
@Entity
@Table(name = "BKO_T_APPLICATION_SUB_REQUEST_STAGING")
public class ApplicationSubRequestStagingEntity extends AbstractAuditEntity{
	
	private static final long serialVersionUID = -1545925942758120839L;

	
	@Id
	@Column(name = "ID")
	private String id;
		
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REQUEST_ID")
	private ApplicationRequestStagingEntity stagingEntity;
	
	@Column(name = "UNIT_ID")
	private String unitId;
 
	@Column(name = "CHANNEL_ID")
	private String channelId;
	
	 public static ApplicationSubRequestStagingEntity toEntity(ApplicationDetails appDetails, ApplicationRequestStagingEntity stagingEntity) {
		 ApplicationSubRequestStagingEntity entity = new ApplicationSubRequestStagingEntity();
		 entity.id = RandomUuidGenerator.uUIDAsString();
		 entity.unitId = appDetails.getUnit();
		 entity.channelId = appDetails.getChannel().getChannelId();
		 entity.stagingEntity = stagingEntity;
		 return entity;
		
	 }
 
	
 
}
