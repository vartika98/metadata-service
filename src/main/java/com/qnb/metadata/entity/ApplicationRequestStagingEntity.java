package com.qnb.metadata.entity;

import java.util.Set;
import java.util.stream.Collectors;

import com.qnb.metadata.dto.appMaintenance.Application;
import com.qnb.metadata.enums.ApplicationType;
import com.qnb.metadata.enums.RequestActionType;
import com.qnb.metadata.enums.TxnStatus;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;

@Getter
@Entity
@Table(name = "BKO_T_APPLICATION_REQUEST_STAGING")
public class ApplicationRequestStagingEntity extends AbstractAuditEntity {
 
	private static final long serialVersionUID = -5694258092445053689L;
 
	@Id
	@Column(name = "REQUEST_ID")
	private String requestId;
 
	@OneToMany(mappedBy = "stagingEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Set<ApplicationSubRequestStagingEntity> subStagingEntity;
 
	@Column(name = "WORKFLOW_ID")
	private String workflowId;
	@Column(name = "REQUEST_COMMENTS")
	private String requestComments;
 
	@Column(name = "ACTION")
    @Enumerated(EnumType.STRING)
	private RequestActionType action;
	@Column(name = "EXTERNAL_ID")
	private Long externalId;
	@Column(name = "APPS_NAME")
	private String appName;
 
	@Column(name = "APP_TYPE")
	@Enumerated(EnumType.STRING)
	private ApplicationType appType;
 
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private TxnStatus status;
	public static ApplicationRequestStagingEntity toEntity(Application application, String requestId, String workflowId, String createdBy) {
		ApplicationRequestStagingEntity entity = new ApplicationRequestStagingEntity();
		entity.requestId = requestId;
		entity.workflowId = workflowId;
		entity.requestComments = application.getMakerComments();
		entity.action = application.getAction();
		entity.externalId = application.getTxnId();
		entity.appName = application.getAppName();
		entity.appType = application.getAppType();
		entity.status =application.getStatus();
		if(application.getApplicationDetails() != null) {
			entity.subStagingEntity = application.getApplicationDetails().stream()
					.map(a -> ApplicationSubRequestStagingEntity.toEntity(a, entity))
					.collect(Collectors.toSet());
		}
		entity.setCreatedBy(createdBy);
		return entity;
	}
 
}
