package com.qnb.metadata.entity;

import com.qnb.metadata.dto.parameter.Parameter;
import com.qnb.metadata.enums.TxnStatus;
import com.qnb.metadata.utils.RandomUuidGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="BKO_T_PARAM_CONFIG_SUB_REQ_STAGING")
public class ParamConfigSubStagingEntity extends AbstractAuditEntity{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = -5640974460584694693L;
 
	@Id
	@Column(name="ID")				
	private String id;
 
	@Column(name="EXTERNAL_ID")		
	private Long externalId;
	@Column(name="CONF_KEY ")    
	private String confKey;
 
	@Column(name="CONF_VALUE")      
	private String confValue;
 
	
	@Column(name = "ACTION")
	private String action;
	@Column(name="STATUS") 
	@Enumerated(EnumType.STRING)
	private TxnStatus status;
 
	@Column(name="REMARKS")   
	private String remarks;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REQUEST_ID")
	private ParamConfigStagingEntity stagingEntity;
	 public static ParamConfigSubStagingEntity toEntity(Parameter parameter, ParamConfigStagingEntity stagingEntity) {
 
		 ParamConfigSubStagingEntity entity = new ParamConfigSubStagingEntity();
	        entity.setId(RandomUuidGenerator.uUIDAsString());
	        entity.setExternalId(parameter.getTxnId());
	        entity.setAction(parameter.getAction()!= null ? parameter.getAction() : "ADD");
	        entity.setConfKey(parameter.getConfKey());
	        entity.setConfValue(parameter.getConfValue());
	        entity.setRemarks(parameter.getRemarks());
	        entity.setStatus(parameter.getStatus() != null ? parameter.getStatus() : TxnStatus.ACT);
	        entity.setStagingEntity(stagingEntity);
	        return entity;
	    }
}
