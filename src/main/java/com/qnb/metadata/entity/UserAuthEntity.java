package com.qnb.metadata.entity;

import java.util.Date;

import com.qnb.metadata.dto.user.UserAuth;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
 
 
@Getter
@Setter
@Entity
@Table(name = "BKO_T_USER_AUTH")
public class UserAuthEntity extends AbstractAuditEntity {
 
	private static final long serialVersionUID = 4668801751329197517L;
 
	@Id
    @Column(name = "user_id")
    private String userId;
 
    @Column(name = "password")
    private String password;
 
    @OneToOne  
    @PrimaryKeyJoinColumn(name = "user_id", referencedColumnName = "user_id")
    private UserEntity userEntity;
 
    @Column(name = "mac_id")
    private String macId;
 
    @Column(name = "auth_session_idle_time")
    private int authSessionIdleTime;
 
    @Column(name = "auth_last_pwd_chng_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date authLastPwdChangeTime;
 
    @Column(name = "auth_last_login_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date authLastLoginTime;
 
    @Column(name = "auth_last_sessionid")
    private String authLastSessionId;
 
    @Column(name = "auth_no_wrong_attempt")
    private int authNumberOfWrongAttempt;
 
    @Column(name = "auth_lock_status")
    private int authLockStatus;
 
    @Column(name = "created_source_ip")
    private String createdSourceIp;
 
    public static UserAuthEntity toEntity(UserAuth userAuth, String userId) {
        UserAuthEntity entity = new UserAuthEntity();
        entity.userId = userId;
        entity.macId = "";
        entity.createdSourceIp = "1:1:1:1";
        return entity;
    }
}