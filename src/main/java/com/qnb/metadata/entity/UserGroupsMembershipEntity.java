package com.qnb.metadata.entity;

import com.qnb.metadata.utils.RandomUuidGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_USER_GROUP_MEMBERSHIP")
public class UserGroupsMembershipEntity extends AbstractAuditEntity {

	private static final long serialVersionUID = 8654741976327106031L;

	@Id
	@Column(name = "memb_id")
	private String membershipId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	UserEntity user;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "group_id")
	private GroupDefEntity groupId;

	public static UserGroupsMembershipEntity toEntity(String groupId, UserEntity userEntity) {
		UserGroupsMembershipEntity entity = new UserGroupsMembershipEntity();
		entity.membershipId = RandomUuidGenerator.uUIDAsString();
		entity.groupId = GroupDefEntity.toEntity(groupId);
		entity.user = userEntity;
		return entity;
	}

}
