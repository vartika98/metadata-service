package com.qnb.metadata.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_DELETE_USER_STAGING")
public class DeleteUserStagingEntity extends WorkflowAuditEntity{
	 
    private static final long serialVersionUID = -2448478143296517333L;

    @Id
    @Column(name = "REQUEST_ID")
    private String requestId;

    @Column(name = "USER_ID")
    private String userId;
   
    @Column(name = "REQUEST_COMMENTS")
    private String requestComments;

    @Column(name = "WORKFLOW_ID")
    private String workflowId;
   
    @Column(name = "UNIT_ID")
    private String unitId;
   
    @Column(name = "STATUS")
    private String status;
   
    @Column(name = "GLOBAL_ID")
    private String globalId;
   
    @Column(name = "NATIONAL_ID")
    private String nationalId;
   
    @Column(name = "CUSTOMER_NAME")
    private String customerName;
   
    @Column(name = "BASE_NO")
    private String baseNum;

}
