package com.qnb.metadata.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_PAGE_CONFIG)
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class PageConfig extends BaseModel {

	private static final long serialVersionUID = 8615705817557328343L;
	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_no")
//	@SequenceGenerator(name = "seq_no", sequenceName = "OCS_SCR_CONF_SEQ",allocationSize = 1)
	@Column(name = "TXN_ID", nullable = false, length = 2)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "UNIT_ID", nullable = false)
	private Unit unit;

	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID", nullable = false)
	private Channel channel;

	@Column(name = "SCREEN_ID", nullable = false, length = 30)
	private String page;

	@Column(name = "CONFIG_KEY", nullable = false, length = 150)
	private String key;

	@Column(name = "CONFIG_VALUE", nullable = false, length = 1000)
	private String value;

	@Column(name = "DESCRIPTION", nullable = true, length = 1000)
	private String description;

	@Column(name = "STATUS", nullable = true, length = 30)
	private String status;

}
