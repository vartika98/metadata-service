package com.qnb.metadata.entity;

import com.qnb.metadata.utils.RandomUuidGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
@Entity
@Table(name = "BKO_T_USER_UNIT_MEMBERSHIP")
public class UserUnitsMembershipEntity extends AbstractAuditEntity{
	private static final long serialVersionUID = 6763271469731236601L;
 
	@Id
    @Column(name = "memb_id")
    private String membershipId;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    UserEntity user;
 
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "unit_id")
    private Unit unitId;
    
    public static UserUnitsMembershipEntity toEntity(String unitId, UserEntity userEntity) {
 
    	UserUnitsMembershipEntity entity = new UserUnitsMembershipEntity();
        entity.setMembershipId (RandomUuidGenerator.uUIDAsString());
        entity.setUnitId(Unit.toEntity(unitId));
        entity.user = userEntity;
        return entity;
    }
 
}
