package com.qnb.metadata.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_IPO_ADMIN_STAGING")
public class IPOStagingEntity extends WorkflowAuditEntity {
	 
    private static final long serialVersionUID = -2448478143296517333L;

    @Id
    @Column(name = "REQUEST_ID")
    private String requestId;
   
    @Column(name = "WORKFLOW_ID")
    private String workflowId;
   
    @Column(name = "LOGIN_IP")
    private String loginIp;
   
    @Column(name = "USER_ID")
    private String userId;//base Num
   
    @Column(name = "PRIMARY_NID_NO")
    private String primaryNationalId;
   
    @Column(name = "TITLE")
    private String title;
   
    @Column(name = "NATIONAL_ID")
    private String nationalId;
   
    @Column(name = "FIRST_NAME")
    private String firstName;
   
    @Column(name = "SECOND_NAME")
    private String secondName;
   
    @Column(name = "THIRD_NAME")
    private String thirdName;
   
    @Column(name = "FOURTH_NAME")
    private String fourthName;
   
    @Column(name = "FAMILY_NAME")
    private String familyName;
   
    @Column(name = "NATIONALITY_CODE")
    private String nationalityCode;
   
    @Column(name = "UNIT_ID")
    private String unitId;
   
    @Column(name = "DATE_OF_BIRTH")
    private String dateOfBirth;
   
    @Column(name = "GENDER")
    private String gender;
   
    @Column(name = "RESIDENT_COUNTRY")
    private String residentCountry;
   
    @Column(name = "RESIDENT_CITY")
    private String residentCity;
   
    @Column(name = "POST_BOX_NO")
    private String postBoxNo;
   
    @Column(name = "EMAIL")
    private String email;
   
    @Column(name = "MOBILE_NO")
    private String mobileNo;
   
    @Column(name = "RESIDENT_PHONE_NO")
    private String residentPhoneNo;
   
    @Column(name = "OFFICE_PH_NO")
    private String officePhNo;
   
    @Column(name = "APPLICANT_TYPE")
    private String applicantType;
   
    @Column(name = "GUARDIAN_NAME")
    private String guardianName;
   
    @Column(name = "GUARDIANID_NO")
    private String guardianidNo;
   
    @Column(name = "MOI_CHREF_NUM")
    private String moiChrefNum;
   
    @Column(name = "LOAN_REQ")
    private String loanRequired;
   
    @Column(name = "NID_EXP_DATE")
    private String nationalityExpDate;
   
    @Column(name = "REQUEST_COMMENTS")
    private String requestComments;
   
    @Column(name = "RELATION_TYPE")
    private String relationType;
}
