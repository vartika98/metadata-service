package com.qnb.metadata.entity;

import java.sql.Date;

import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.model.BaseModel;

import ch.qos.logback.core.status.Status;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_UNIT_LANGUAGE)
public class UnitLanguage extends BaseModel {

	private static final long serialVersionUID = 8615705817557328343L;

	@Id
	@Column(name = "TXN_ID", nullable = false, length = 15)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIT_ID", referencedColumnName = "UNIT_ID")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID", insertable = false, updatable = false, nullable = false)
	private Channel channel;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "LANG_CODE")
	private Language language;

	@Column(name = "IS_DEFAULT")
	private String isDefault;
}

//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "unitLanguageSeqGenerator")
//    @SequenceGenerator(name = "unitLanguageSeqGenerator", sequenceName = "OCS_UNIT_LANGUAGE_SEQ", allocationSize = 1)
//    @Column(name = "TXN_ID")
//    private Long txnId;
//
//    @Column(name = "UNIT_ID", nullable = false)
//    private String unitId;
//
//    @Column(name = "CHANNEL_ID", nullable = false)
//    private String channelId;
//
//    @Column(name = "LANG_CODE", nullable = false, columnDefinition = "VARCHAR2(2 BYTE) DEFAULT 'en'")
//    private String langCode;
//
//    @Column(name = "IS_DEFAULT", nullable = false, columnDefinition = "CHAR(1 BYTE) DEFAULT 'Y'")
//    private String isDefault;
//
//    @Column(name = "STATUS", nullable = false, columnDefinition = "VARCHAR2(3 BYTE) DEFAULT 'ACT'")
//    private String status;
//
//    @Column(name = "DESCRIPTION", columnDefinition = "VARCHAR2(1000 BYTE)")
//    private String description;
//
//    @Column(name = "CREATED_BY", columnDefinition = "VARCHAR2(15 BYTE) DEFAULT 'SYSTEM'")
//    private String createdBy;
//
//    @Column(name = "DATE_CREATED", nullable = false, columnDefinition = "DATE DEFAULT SYSDATE")
////    @Temporal(TemporalType.DATE)
//    private Date dateCreated;
//
//    @Column(name = "MODIFIED_BY", columnDefinition = "VARCHAR2(15 BYTE)")
//    private String modifiedBy;
//
//    @Column(name = "DATE_MODIFIED")
////    @Temporal(TemporalType.DATE)
//    private Date dateModified;
//
//    @ManyToOne
//    @JoinColumn(name = "UNIT_ID", referencedColumnName = "UNIT_ID", insertable = false, updatable = false)
//    private Unit unit;
//
//    
//    @JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID", insertable = false, updatable = false)
//    private Channel channel;
//
//    
//    @JoinColumn(name = "LANG_CODE", referencedColumnName = "LANG_CODE", insertable = false, updatable = false)
//    private Language language;
//
////    @ManyToOne
////    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS", insertable = false, updatable = false)
////    private Status statusEntity;
// 
//}
