package com.qnb.metadata.entity;

import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.OCS_T_UNIT_DESC)
public class UnitDesc extends BaseModel {

	private static final long serialVersionUID = 8615705817557328343L;

	@Id
	@Column(name = "TXN_ID")
	private String id;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "UNIT_DESC", nullable = false)
	private String unitDesc;

	@Column(name = "LANG_CODE", nullable = false)
	private String langCode;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "Remarks", nullable = true)
	private String description;

}
