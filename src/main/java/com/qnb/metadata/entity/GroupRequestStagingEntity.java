package com.qnb.metadata.entity;

import java.util.Set;

import com.qnb.metadata.dto.role.GroupDef;
import com.qnb.metadata.enums.ActionType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_GROUP_DEF_REQUEST_STAGING")
public class GroupRequestStagingEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = -5074380764871901887L;

	@Id
	@Column(name = "request_id")
	private String requestId;

	@Column(name = "request_comments")
	private String requestComments;

	@Column(name = "workflow_id")
	private String workflowId;

	@Column(name = "action")
	@Enumerated(EnumType.STRING)
	private ActionType action;

	@Column(name = "external_id")
	private String externalId;

	@Column(name = "group_code")
	private String groupCode;

	@Column(name = "group_description")
	private String groupDescription;

	@Column(name = "roles")
	private String roles;

	@Column(name = "unit")
	private String unit;

	public static GroupRequestStagingEntity toEntity(GroupDef group, String requestId, String workflowId,
			String createdBy) {
		GroupRequestStagingEntity groupEntity = new GroupRequestStagingEntity();
		groupEntity.requestId = requestId;
		groupEntity.requestComments = group.getMakerComments();
		groupEntity.workflowId = workflowId;
		groupEntity.action = group.getAction();
		groupEntity.externalId = group.getGroupId();
		groupEntity.groupCode = group.getGroupCode();
		groupEntity.groupDescription = group.getGroupDescription();
		groupEntity.roles = getGroupRoles(group.getRoles());
		groupEntity.unit = group.getUnit();
		groupEntity.setCreatedBy(createdBy);
		groupEntity.setLastModifiedBy(createdBy);
		return groupEntity;
	}

	private static String getGroupRoles(Set<String> roles) {
		if (roles != null && !roles.isEmpty()) {
			return String.join(",", roles);
		}
		return null;
	}
}
