package com.qnb.metadata.entity;

import com.qnb.metadata.enums.TxnStatus;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "OCS_T_PARAM_CONFIG")
public class ParamConfigEntity extends AbstractOCSAuditEntity{
	private static final long serialVersionUID = 7118610058936131304L;
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARAM_CONFIG_SEQ")
	@SequenceGenerator(sequenceName = "OCS_PARAM_CONFIG_SEQ", allocationSize = 1, name = "PARAM_CONFIG_SEQ")
	@Column(name = "TXN_ID")
	private Long txnId;
	@Column(name = "UNIT_ID")
	private String unitId;
	@Column(name = "CHANNEL_ID")
	private String channelId;
	@Column(name = "CONF_KEY")
	private String confKey;
	@Column(name = "CONF_VALUE")
	private String confValue;
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private TxnStatus status;
	@Column(name = "REMARKS")
	private String remarks;

 
}
