package com.qnb.metadata.entity;

import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.enums.RemarkType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_WORKFLOW_HISTORY")
public class WorkflowHistoryEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = -8787430665649572580L;

	@Id
	@Column(name = "workflow_history_id")
	private String workflowHistoryId;

	@Column(name = "workflow_id")
	private String workflowId;

	@Column(name = "request_id")
	private String requestId;

	@Column(name = "process_id")
	private String processId;

	@Column(name = "seq_number")
	private Integer seqNumber;

	@Column(name = "workflow_status")
	private String workflowStatus;

	@Column(name = "task_id")
	private String taskId;

	@Column(name = "remark")
	private String remark;

	@Column(name = "REMARK_TYPE")
	@Enumerated(EnumType.STRING)
	private RemarkType remarkType;

	@Column(name = "performed_action")
	private String performedAction;

	public static WorkflowHistoryEntity toEntity(WorkflowHistory workflowHistory) {

		WorkflowHistoryEntity entity = new WorkflowHistoryEntity();
		entity.workflowHistoryId = workflowHistory.getWorkflowHistoryId();
		entity.requestId = workflowHistory.getRequestId();
		entity.processId = workflowHistory.getProcessId();
		entity.seqNumber = workflowHistory.getSeqNumber();
		entity.workflowId = workflowHistory.getWorkflowId();
		entity.workflowStatus = workflowHistory.getWorkflowStatus().name();
		entity.taskId = workflowHistory.getTaskId();
		entity.performedAction = workflowHistory.getProcessStatus();
		entity.remark = workflowHistory.getRemarks();
		entity.remarkType = workflowHistory.getRemarkType();
		entity.setCreatedBy(workflowHistory.getCreatedBy());
		entity.setLastModifiedBy(workflowHistory.getCreatedBy());
		return entity;
	}
}
