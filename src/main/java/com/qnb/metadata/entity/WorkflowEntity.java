package com.qnb.metadata.entity;

import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.qnb.metadata.dto.workflow.Workflow;
import com.qnb.metadata.enums.WorkflowStatus;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_WORKFLOW")
public class WorkflowEntity extends WorkflowAuditEntity{
	
	private static final long serialVersionUID = -5244610030322809007L;
	 
    @Id
    @Column(name = "workflow_id")
    private String workflowId;

    @OneToMany(mappedBy = "workflow", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<WorkflowRequestIdentifierEntity> requestIdentifiers;

    @Column(name = "request_id")
    private String requestId;

    @Column(name = "process_id")
    private String processId;

    @Column(name = "task_id")
    private String taskId;

    @Column(name = "assigned_group")
    private String assignedGroup;

    @Column(name = "initiated_group")
    private String initiatedGroup;

    @Column(name = "workflow_group")
    private String workflowGroup;

    @Column(name = "available_actions")
    private String availableActions;

    @Column(name = "last_performed_action")
    private String lastPerformedAction;

    @Column(name = "workflow_status")
    @Enumerated(EnumType.STRING)
    private WorkflowStatus workflowStatus;

    @Column(name = "scheduled_time")
    private Instant scheduledTime;

    @Column(name = "history_count")
    private Integer historyCount;
  
// @Column(name = "CREATED_BY")
// private String CREATED_BY;
    @Column(name = "created_by")
    private String createdBy;

    public static WorkflowEntity toEntity(Workflow workflow, List<String> requestIdentifiers) {
                    WorkflowEntity entity = new WorkflowEntity();
                    entity.requestId = workflow.getRequestId();
                    entity.workflowId = workflow.getWorkflowId();
                    entity.processId = workflow.getProcessId();
                    entity.taskId = workflow.getTaskId();
                    entity.assignedGroup = workflow.getAssignTo();
                    entity.initiatedGroup = workflow.getInitiatedGroup();
                    entity.lastPerformedAction = workflow.getProcessStatus();
                    if (workflow.getAvailableActions() != null) {
                                    entity.availableActions = String.join(",", workflow.getAvailableActions());
                    }
                    entity.workflowStatus = workflow.getStatus();
                    entity.historyCount = workflow.getHistoryCount();
                    entity.setCreatedBy(workflow.getCreatedBy());
                    entity.setLastModifiedBy(workflow.getLastModifiedBy());
                    entity.scheduledTime = workflow.getScheduledTime();
                    entity.workflowGroup = workflow.getWorkflowGroup();

                    if (requestIdentifiers != null && !requestIdentifiers.isEmpty()) {
                                    entity.requestIdentifiers = requestIdentifiers.stream()
                                                                    .map(identifier -> WorkflowRequestIdentifierEntity.toEntity(entity, identifier))
                                                                    .collect(Collectors.toSet());
                    }
                    return entity;
    }

    public WorkflowEntity updateEntity(Workflow workflow) {
                    this.taskId = workflow.getTaskId();
                    this.assignedGroup = workflow.getAssignTo();
                    if (workflow.getAvailableActions() != null) {
                                    this.availableActions = String.join(",", workflow.getAvailableActions());
                    }
                    this.scheduledTime = workflow.getScheduledTime();
                    this.lastPerformedAction = workflow.getProcessStatus();
                    this.workflowStatus = workflow.getStatus();
                    this.setLastModifiedBy(workflow.getLastModifiedBy());
                    this.historyCount = workflow.getHistoryCount();
                    this.requestId = workflow.getRequestId();
                    return this;
    }

}
