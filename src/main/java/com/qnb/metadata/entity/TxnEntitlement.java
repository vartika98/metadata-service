package com.qnb.metadata.entity;

import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = TableConstants.TABLE_TXN_ENTITLEMENT)
public class TxnEntitlement extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "TXN_ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "txn_generator")
	@SequenceGenerator(name = "txn_generator", sequenceName = "TXN_SEQUENCE", allocationSize = 1)
    private String txnId;
 
	@Column(name = "UNIT_ID")
    private String unitId;
 
	@Column(name = "CHANNEL_ID")
    private String channelId;
 
//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID",unique=true)
    private String menuId;
 
    @Column(name = "CUSTOMER_SEGMENT")
    private String customerSegment;
    
    @Column(name = "DISP_PRIORITY")
    private String dispPriority;
    
    @Column(name = "STATUS")
    private String status;
 

}
