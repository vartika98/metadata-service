package com.qnb.metadata.entity;

import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_CHANNEL_MASTER)
public class Channel extends BaseModel {

	private static final long serialVersionUID = -8032944473314781424L;

	@Id
	@Column(name = "TXN_ID", nullable = false)
	private Integer txnId;

	@Column(name = "CHANNEL_ID", nullable = false, length = 3,unique=true)
	private String channelId;

	@Column(name = "CHANNEL_DESC", nullable = true, length = 30)
	private String channelDesc;
	
	@Column(name = "DESCRIPTION", nullable = true, length = 30)
	private String description;
	
	

	public Channel(String channel) {
		super();
		this.channelId = channel;
	}


}
