package com.qnb.metadata.entity;

import java.util.Set;

import com.qnb.metadata.enums.TxnStatus;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "BKO_T_TXN_LABEL_SUB_REQ_STAGING")
public class LabelSubStagingEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = 8978166058182195755L;

	@Id
	@Column(name = "ID")
	private String id;

	@Column(name = "EXTERNAL_ID")
	private String externalId;

	@Column(name = "TRANS_GROUP")
	private String transGroup;

	@Column(name = "TRANS_KEY")
	private String transKey;

	@Column(name = "ACTION")
	private String action;

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private TxnStatus status;

	@Column(name = "DESCRIPTION")
	private String description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REQUEST_ID")
	private LabelStagingEntity stagingEntity;

	@OneToMany(mappedBy = "subStagingEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Set<LabelDecriptionStagingEntity> descStagingEntity;

}
