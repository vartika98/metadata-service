package com.qnb.metadata.entity;

import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "BKO_T_TXN_LABEL_REQ_STAGING")
public class LabelStagingEntity extends WorkflowAuditEntity {
	 
    private static final long serialVersionUID = 2386417898993814082L;

    @Id
    @Column(name = "REQUEST_ID")
    private String requestId;
   
    @Column(name = "WORKFLOW_ID")
    private String workFlowId;
   
    @Column(name = "REQUEST_COMMENTS")
    private String reqComments;
   
    @Column(name="SCREEN_ID")     
    private String screenId;
   
    @Column(name="UNIT_ID")       
    private String unitId;

    @Column(name="CHANNEL_ID")    
    private String channelId;

    @Column(name="MENU_ID")    
    private String menuId;
   
    @OneToMany(mappedBy = "stagingEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<LabelSubStagingEntity> subStagingEntity;

}
