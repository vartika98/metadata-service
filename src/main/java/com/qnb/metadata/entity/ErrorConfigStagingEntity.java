package com.qnb.metadata.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_ERROR_CONFIG_STAGING")
public class ErrorConfigStagingEntity extends WorkflowAuditEntity {
	 
    private static final long serialVersionUID = -2448478143296517333L;

    @Id
    @Column(name = "TXN_ID")
    private String txnId;        
   
    @Column(name = "REQUEST_COMMENTS")
    private String requestComments;

    @Column(name = "WORKFLOW_ID")
    private String workflowId;
   
    @Column(name = "ACTION_TYPE")
    private String actionType;
   
    @Column(name = "UNIT_ID")
    private String unitId;

    @Column(name = "CHANNEL_ID")
    private String channelId; 

    @Column(name = "SERVICE_TYPE")
    private String serviceType;
   
    @Column(name = "MW_ERR_CODE")
    private String mwErrCode;
   
    @Column(name = "OCS_ERR_CODE")
    private String ocsErrCode;
   
    @Column(name = "OCS_ERR_DESC_ENG")
    private String errDescEng;
   
    @Column(name = "OCS_ERR_DESC_ARB")
    private String errDescArb;
   
    @Column(name = "OCS_ERR_DESC_FR")
    private String errDescFr;
   
    @Column(name = "STATUS")
    private String status;
   
    @Column(name = "REMARKS")
    private String remarks;
   
    @Column(name = "CREATED_BY")
    private String createdBy;
   
    @Column(name = "DATE_CREATED", insertable = false, updatable = false)
    private Date dateCreated;
   
    @Column(name = "MODIFIED_BY")
    private String modifiedBy;
   
    @Column(name = "DATE_MODIFIED")
    private Date dateModified;
   
    @Column(name = "WF_REQ_ID")
    private String workFlowReqId;
   
    @Column(name = "ER_CONFIG_ID")
    private Integer erConfigId;
}
