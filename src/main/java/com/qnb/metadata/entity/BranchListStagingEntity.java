package com.qnb.metadata.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.qnb.metadata.dto.branchList.BranchListMaster;
import com.qnb.metadata.dto.branchList.BranchParameterRequest;
import com.qnb.metadata.utils.RandomUuidGenerator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_BRANCH_LIST_STAGING")
public class BranchListStagingEntity extends AbstractAuditEntity {
 
	private static final long serialVersionUID = -2448478143296517333L;
 
	@Id
	@Column(name = "TXN_ID")
	private String txnId;	
	
	@Column(name = "REQUEST_COMMENTS")
	private String requestComments;
 
	@Column(name = "WORKFLOW_ID")
	private String workflowId;
	
	@Column(name = "WF_REQ_ID")
	private String workFlowReqId;
	
	@Column(name = "UNIT_ID")
	private String unitId;
	
	@Column(name = "BRANCH_ID")
	private Integer branchId;
	
	@Column(name = "ACTION_TYPE")
	private String actionType;
	
	@Column(name = "COUNTRY_CODE")
	private String countryCode;
 
	@Column(name = "BRANCH_CODE")
	private String branchCode;
 
	@Column(name = "BRANCH_NAME_EN")
	private String branchDesc;
 
	@Column(name = "BRANCH_NAME_AR")
	private String branchDescAr;
	
	@Column(name = "BRANCH_NAME_FR")
	private String branchDescFr;
	
	@Column(name = "CITY")
	private String city;
	
	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name = "COLLECTION_BRANCH_FLAG")
	private String collectionBranchFlag;
	
	@Column(name = "STATUS")
	private String status;
	
	/*
	 * @Column(name = "CREATED_BY") private String createdBy;
	 */
	
	@Column(name = "DATE_CREATED")
	private Date dateCreated;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "DATE_MODIFIED")
	private Date dateModified;
	
	
	
	public static List<BranchListStagingEntity> toEntity(BranchListMaster branchListStatus, String requestId, String workflowId,
			String createdBy) {
		String unitId=branchListStatus.getUnitId();
		String makerComment=branchListStatus.getRemarks();
		List<BranchListStagingEntity> entityList=new ArrayList<>();
		BranchListStagingEntity entity =null;
		for(BranchParameterRequest param: branchListStatus.getParamDetails()) {
			entity = new BranchListStagingEntity();
			entity.setTxnId(RandomUuidGenerator.uUIDAsString());
			entity.setRequestComments(makerComment);
			entity.setWorkflowId(workflowId);
			entity.setActionType(param.getAction());
			entity.setUnitId(unitId);
			entity.setStatus("ACT");
			entity.setModifiedBy(createdBy);
			entity.setWorkFlowReqId(requestId);
			entity.setBranchId(param.getBranchId());
			entity.setCountryCode(param.getCountryCode());
			entity.setBranchCode(param.getBranchCode());
			entity.setBranchDesc(param.getBranchDesc());
			entity.setBranchDescAr(param.getBranchDescAr());
			entity.setBranchDescFr(param.getBranchDescFr());
			entity.setCity(param.getCity());
			entity.setAddress(param.getAddress());
			entity.setCollectionBranchFlag(param.getCollectionBranchFlag());
			entity.setStatus(param.getStatus());
			entity.setCreatedBy(param.getCreatedBy());
			entity.setDateCreated(param.getDateCreated());
			entity.setModifiedBy(param.getModifiedBy());
			entity.setDateModified(param.getDateModified());
			entityList.add(entity);
		}
		return entityList;
	}
 
}
