package com.qnb.metadata.entity;



import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.dto.LabelDescription;
import com.qnb.metadata.dto.LabelDetails;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = false)
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
@Table(name = TableConstants.TABLE_TRANSLATION)
@NoArgsConstructor
public class Translation extends BaseModel {
 
	private static final long serialVersionUID = 8615705817557328343L;
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_no")
	@SequenceGenerator(name = "seq_no", sequenceName = "OCS_I18N_SEQ",allocationSize = 1)
	@Column(name = "TXN_ID", nullable = false, length = 15)
	@EqualsAndHashCode.Exclude
	private Integer id;
 
	@ManyToOne
	@JoinColumn(name = "UNIT_ID", referencedColumnName = "UNIT_ID")
	private Unit unit;
 
	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID", referencedColumnName = "CHANNEL_ID")
	private Channel channel;
 
	@Column(name = "LANG_CODE")
	private String langCode;	
 
	@Column(name = "SCREEN_ID", nullable = false, length = 200)
	private String page;
 
	@EqualsAndHashCode.Exclude
	@Column(name = "TRANS_GROUP", nullable = false, length = 20)
	private String group;
 
//	@Column(name = "UNIQUE_ID", nullable = false, length = 200)
//	private String uniqueId;
 
	@Column(name = "TRANS_KEY", nullable = false, length = 200)
	private String key;
 
	@EqualsAndHashCode.Exclude
	@Column(name = "TRANS_VALUE", nullable = false, length = 1000)
	private String value;
 
	@EqualsAndHashCode.Exclude
	@Column(name = "DESCRIPTION", nullable = false, length = 1000)
	private String description;
	
	
	   
 
}
