package com.qnb.metadata.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qnb.metadata.utils.RandomUuidGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "BKO_T_TXN_ENTITLEMENT_SUB_REQ_STAGING")
public class TxnEntitlementSubRequestStagingEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = -2343266716486109197L;
	@Id
	@Column(name = "ID")
	private String id;

	@ManyToOne(fetch = FetchType.LAZY,optional = false)
	@JoinColumn(name = "REQUEST_ID",referencedColumnName = "REQUEST_ID",insertable = false,updatable = false)
	@JsonIgnore
	private TxnEntitlementRequestStagingEntity stagingEntity;

	@Column(name = "EXTERNAL_ID")
	private String externalId;

	@Column(name = "PARENT_ID")
	private String parentId;

	@Column(name = "MENU_ID")
	private String menuId;

	@Column(name = "STATUS")
	private String status;

	public static TxnEntitlementSubRequestStagingEntity toEntity(com.qnb.metadata.dto.entitlement.TxnEntitlement txnEntitlement,
			TxnEntitlementRequestStagingEntity stagingEntity) {
		TxnEntitlementSubRequestStagingEntity entity = new TxnEntitlementSubRequestStagingEntity();
		entity.setId(RandomUuidGenerator.uUIDAsString());
		entity.setMenuId(txnEntitlement.getMenuId());
//		entity.setStagingEntity(stagingEntity);
		entity.setParentId(txnEntitlement.getParentId());
		entity.setStatus(txnEntitlement.getStatus());
		return entity;
	}
}
