package com.qnb.metadata.entity;

import java.util.Date;
import java.util.Set;

import com.qnb.metadata.dto.user.User;
import com.qnb.metadata.enums.ActionType;
import com.qnb.metadata.enums.UserStatus;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "BKO_T_USER_REQUEST_STAGING")
public class UserRequestStagingEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = -5074380764871901887L;

	@Id
	@Column(name = "request_id")
	private String requestId;

	@Column(name = "request_comments")
	private String requestComments;

	@Column(name = "workflow_id")
	private String workflowId;

	@Column(name = "action")
	@Enumerated(EnumType.STRING)
	private ActionType action;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "expiry_date")
	@Temporal(TemporalType.DATE)
	private Date expiryDate;

	@Column(name = "date_of_birth")
	@Temporal(TemporalType.DATE)
	private Date dob;

	@Column(name = "mobile_no")
	private String mobileNumber;

	@Column(name = "email_id")
	private String emailId;

	@Column(name = "user_groups")
	private String userGroups;

	@Column(name = "user_units")
	private String userUnits;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private UserStatus status;

	@Column(name = "created_source_ip")
	private String createdSourceIp;

	public static UserRequestStagingEntity toEntity(User user, String requestId, String workflowId, String createdBy) {
		UserRequestStagingEntity userEntity = new UserRequestStagingEntity();
		userEntity.requestId = requestId;
		userEntity.workflowId = workflowId;
		userEntity.action = user.getAction();
		userEntity.requestComments = user.getMakerComments();
		userEntity.userId = user.getUserId();
		userEntity.firstName = user.getFirstName();
		userEntity.lastName = user.getLastName() != null ? user.getLastName() : " ";
		userEntity.expiryDate = user.getExpiryDate();
		userEntity.dob = user.getDob();
		userEntity.mobileNumber = user.getMobileNumber();
		userEntity.emailId = user.getEmailId();
		userEntity.status = user.getUserStatus();
		userEntity.setCreatedBy(createdBy);
		userEntity.createdSourceIp = "1:1:1:1";
		userEntity.setLastModifiedBy(createdBy);
		userEntity.userGroups = getUserGroups(user.getGroups());
		userEntity.userUnits = getUserUnits(user.getUnits());
		return userEntity;
	}

	private static String getUserGroups(Set<String> groups) {
		if (groups != null && !groups.isEmpty()) {
			return String.join(",", groups);
		}
		return null;
	}

	private static String getUserUnits(Set<String> units) {
		if (units != null && !units.isEmpty()) {
			return String.join(",", units);
		}
		return null;
	}
}
