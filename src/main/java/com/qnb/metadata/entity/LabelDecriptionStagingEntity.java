package com.qnb.metadata.entity;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "BKO_T_TXN_LABEL_DESCRIPTION_STAGING")
public class LabelDecriptionStagingEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = -5001972823491646283L;

	@Id
	@Column(name = "ID")
	private String id;

	@Column(name = "REQUEST_ID")
	private String requestId;

	@Column(name = "LANG_CODE")
	private String langCode;

	@Column(name = "TRANS_VALUE")
	private String transValue;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TRANS_KEY")
	private LabelSubStagingEntity subStagingEntity;
}
