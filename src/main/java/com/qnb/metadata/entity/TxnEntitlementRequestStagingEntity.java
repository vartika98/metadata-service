package com.qnb.metadata.entity;

import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qnb.metadata.dto.entitlement.TransactionEntitlementDto;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "BKO_T_TXN_ENTITLEMENT_REQ_STAGING")
public class TxnEntitlementRequestStagingEntity extends WorkflowAuditEntity {
	private static final long serialVersionUID = 8715707055541295441L;

	@Id
	@Column(name = "REQUEST_ID")
	private String requestId;
	
	@OneToMany(mappedBy = "stagingEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true,targetEntity = TxnEntitlementSubRequestStagingEntity.class)
	private Set<TxnEntitlementSubRequestStagingEntity> subStagingEntity;
	
	@Column(name = "WORKFLOW_ID")
	private String workflowId;
	
	@Column(name = "REQUEST_COMMENTS")
	private String requestComments;
	
	@Column(name = "EXECUTE_ACTION")
	private String executeAction;
	
	@Column(name = "SCHEDULED_TIME")
	private String scheduledTime;

	@Column(name = "SCHEDULED_TIMEZONE")
	private String scheduledTimeZone;

	@Column(name = "ACTION")
	private String action;

	@Column(name = "UNIT_ID")
	private String unitId;

	@Column(name = "CHANNEL_ID")
	private String channelId;

	public static TxnEntitlementRequestStagingEntity toEntity(TransactionEntitlementDto txn, String requestId,
			String workflowId, String createdBy) {
		TxnEntitlementRequestStagingEntity entity = new TxnEntitlementRequestStagingEntity();
		entity.setRequestId(requestId);
		entity.setWorkflowId(workflowId);
		entity.setRequestComments(txn.getMakerComments());
		entity.setExecuteAction(txn.getExcuteAction());
		entity.setScheduledTime(txn.getScheduledTimeText());
		entity.setScheduledTimeZone(txn.getScheduledTimeZone());
		entity.setAction(txn.getAction());
		entity.setCreatedBy(createdBy);
		entity.setUnitId(txn.getUnitId());
		entity.setChannelId(txn.getChannelId());

//		if (txn.getTxnEntitlement() != null) {
//			entity.setSubStagingEntity(txn.getTxnEntitlement().stream()
//					.map(s -> TxnEntitlementSubRequestStagingEntity.toEntity(s, entity)).collect(Collectors.toSet()));
//		}
		return entity;
	}
}
