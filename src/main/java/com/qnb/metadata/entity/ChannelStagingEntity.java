package com.qnb.metadata.entity;

import com.qnb.metadata.enums.TxnStatus;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_CHANNEL_MASTER_STAGING")
public class ChannelStagingEntity extends WorkflowAuditEntity {
	 
    private static final long serialVersionUID = -2448478143296517333L;

    @Id
    @Column(name = "REQUEST_ID")
    private String requestId;

    @Column(name = "REQUEST_COMMENTS")
    private String requestComments;

    @Column(name = "WORKFLOW_ID")
    private String workflowId;

    @Column(name = "ACTION")
    private String action;

    @Column(name = "channel_id")
    private String channelId;

    @Column(name = "external_id")
    private Long externalId;

    @Column(name = "channel_desc")
    private String channelDesc;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TxnStatus status;

    @Column(name = "description")
    private String description;

}
