package com.qnb.metadata.entity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.qnb.metadata.dto.role.GroupDef;
import com.qnb.metadata.enums.GroupType;
import com.qnb.metadata.utils.RandomUuidGenerator;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_GROUP_DEF")
public class GroupDefEntity extends WorkflowAuditEntity {

	private static final long serialVersionUID = 3404570722544790592L;

	@Id
	@Column(name = "group_id")
	private String groupId;

	@Column(name = "group_code")
	private String groupCode;

	@Column(name = "group_type")
	@Enumerated(EnumType.STRING)
	private GroupType groupType;

	@Column(name = "group_description")
	private String groupDescription;

	@OneToMany(mappedBy = "groupDef", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private List<GroupRolesEntity> groupRoles;

	@Column(name = "unit")
	private String unit;

	public static GroupDefEntity toEntity(GroupDef groupDef) {

		GroupDefEntity entity = new GroupDefEntity();
		entity.groupId = groupDef.getGroupId();
		entity.groupCode = groupDef.getGroupCode();
		entity.groupType = GroupType.FUNCTIONAL;
		entity.groupDescription = groupDef.getGroupDescription();
		entity.unit = groupDef.getUnit();

		var groupRoles = Optional.ofNullable(groupDef.getRoles()).orElseGet(Collections::emptySet).stream()
				.map(role -> GroupRolesEntity.toEntity(RandomUuidGenerator.uUIDAsString(), entity, role)).collect(Collectors.toList());

		entity.groupRoles = groupRoles;

		return entity;
	}

	public static GroupDefEntity updateEntity(GroupDefEntity entity, GroupDef group) {
		entity.groupDescription = group.getGroupDescription();
		entity.groupCode = group.getGroupCode();
		entity.unit = group.getUnit();
		entity.setLastModifiedBy("testUser");

		var rolesToRemove = entity.getGroupRoles().stream().filter(r -> !group.getRoles().contains(r.getRoleId()))
				.collect(Collectors.toSet());
		entity.getGroupRoles().removeAll(rolesToRemove);

		var updatedRoleIds = entity.getGroupRoles().stream().map(GroupRolesEntity::getRoleId)
				.collect(Collectors.toSet());

		var newRolesTobeAdded = group.getRoles().stream().filter(r -> !updatedRoleIds.contains(r))
				.map(newRoleToMap -> GroupRolesEntity.toEntity(RandomUuidGenerator.uUIDAsString(), entity, newRoleToMap))
				.collect(Collectors.toSet());
		entity.getGroupRoles().addAll(newRolesTobeAdded);

		return entity;
	}

	public static GroupDefEntity toEntity(String groupId) {
		GroupDefEntity entity = new GroupDefEntity();
		entity.setGroupId(groupId);
		return entity;
	}
}