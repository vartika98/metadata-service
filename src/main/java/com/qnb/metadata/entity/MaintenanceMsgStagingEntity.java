package com.qnb.metadata.entity;

import java.util.Set;

import com.qnb.metadata.enums.TxnStatus;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="BKO_T_MAINTENANCE_MSG_STAGING")
public class MaintenanceMsgStagingEntity extends AbstractAuditEntity {
 
	/**
	 *
	 */
	private static final long serialVersionUID = 8391242497632700870L;
 
	@Id
	@Column(name = "REQUEST_ID")
	private String requestId;
	
	@Column(name = "WORKFLOW_ID")
	private String workFlowId;
	
	@Column(name = "REQUEST_COMMENTS")
	private String reqComments;
	
 
	@Column(name = "MESSAGE_TYPE")
	private String messageType;
	
	@Column(name="ACTION")
	private String action;
	
	@Column(name = "REF_NO")
	private String refNo;
	
	@Column(name="STATUS")
	@Enumerated(EnumType.STRING)
	private TxnStatus status;
	
	@Column(name = "PURPOSE_OF_MSG")
	private String purposeOfMsg;
 
	@OneToMany(mappedBy = "stagingEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Set<MaintenanceMsgSubStagingEntity> subStagingEntity;
	
	@OneToMany(mappedBy = "stagingEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Set<MaintenanceMsgLangDetailStagingEntity> descStagingEntity;
 
}