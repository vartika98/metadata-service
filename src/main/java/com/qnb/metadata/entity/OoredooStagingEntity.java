package com.qnb.metadata.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "BKO_T_OOREDOO_PAYMENT_STAGING")
public class OoredooStagingEntity extends WorkflowAuditEntity {
	 
    private static final long serialVersionUID = -2448478143296517333L;

    @Id
    @Column(name = "REQUEST_ID")
    private String requestId;
   
    @Column(name = "GLOBALID")
    private String globalId;
   
    @Column(name = "LOGINIP")
    private String loginIp;
   
    @Column(name = "LOGINUSERID")
    private String loginUserId;
   
    @Column(name = "USER_ID")
    private String userId;
   
    @Column(name = "REQUEST_COMMENTS")
    private String requestComments;

    @Column(name = "WORKFLOW_ID")
    private String workflowId;

    @Column(name = "NATIONAL_ID")
    private String nationalId;
   
    @Column(name = "CUSTOMER_NAME")
    private String customerName;
   
    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;
   
    @Column(name = "SERVICE_NUMBER")
    private String serviceNumber;
   
    @Column(name = "MOBILE_NUMBER")
    private String mobileNumber;
   
    @Column(name = "POINT_BALANCE")
    private String pointBalance;

    @Column(name = "PAYMENT_AMOUNT")
    private String paymentAmount;
   
    @Column(name = "REDEEM_POINTS")
    private String redeemPoints;
   
    @Column(name = "UNIT_ID")
    private String unitId;
   
    @Column(name = "USER_NO")
    private String userNo;
}
