package com.qnb.metadata.entity;

import java.sql.Types;
import org.hibernate.annotations.JdbcTypeCode;
import com.qnb.metadata.dto.hbtf.HBTFMaster;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "BKO_T_HBTF_STAGING")
public class HBTFStagingEntity extends AbstractAuditEntity {
 
	private static final long serialVersionUID = -2448478143296517333L;
 
	@Id
	@Column(name = "REQUEST_ID")
	private String requestId;
	
	@Column(name = "USER_NO")
	@JdbcTypeCode(Types.VARBINARY)
	private byte[] userNo;
	
	@Column(name = "GLOBAL_ID")
	private String globalId;
	
	@Column(name = "USER_ID")
	private String userId;
	
	@Column(name = "REQUEST_COMMENTS")
	private String requestComments;
 
	@Column(name = "WORKFLOW_ID")
	private String workflowId;
	
	@Column(name = "UNIT_ID")
	private String unitId;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "REF_NO")
	private String refNo;
	
	@Column(name = "DEBIT_ACC_NO")
	private String debitAccountNo;
	
	@Column(name = "REQUESTED_BY")
	private String requestedBy;
	
	@Column(name = "REQUESTED_DATE")
	private String requestedDate;
	
	@Column(name = "REJECTED_BY")
	private String rejectedBy;
	
	@Column(name = "REJECTED_DATE")
	private String rejectedDate;
	
	@Column(name = "REJECT_COUNT")
	private int rejectCount;
	
	@Column(name = "CHECKER_COMMENTS")
	private String checkerComments;
 
	@Column(name = "BENF_NAME")
	private String benfName;
	
	@Column(name = "BENF_BANK_NAME")
	private String benfBankName;
	
	@Column(name = "BENF_ACNT_NO")
	private String benfAccountNo;
	
	@Column(name = "CREDIT_AMOUNT")
	private String creditAmount;
	
	@Column(name = "DEBIT_AMOUNT")
	private String debitAmount;
	
	@Column(name = "SELL_RATE")
	private double sellRate;
 
	public static HBTFStagingEntity toEntity(HBTFMaster hbtfMaster, String requestId, String workflowId,
			String createdBy) {
		HBTFStagingEntity entity = new HBTFStagingEntity();
		entity.setUserId(hbtfMaster.getUserId());
		entity.setGlobalId(hbtfMaster.getGlobalId());
		entity.setDebitAccountNo(hbtfMaster.getDebitAccountNo());
		entity.setRefNo(hbtfMaster.getRefNo());
		entity.setRequestComments(hbtfMaster.getRequestComments());
		entity.setRequestedBy(hbtfMaster.getRequestedBy());
		entity.setRequestedDate(hbtfMaster.getRequestedDate());
		entity.setUnitId(hbtfMaster.getUnitId());
		entity.setStatus(hbtfMaster.getStatus());
		entity.setRejectedBy(hbtfMaster.getRejectedBy());
		entity.setRejectedDate(hbtfMaster.getRejectedDate());
		entity.setRejectCount(hbtfMaster.getRejectCount());
		entity.setWorkflowId(workflowId);
		entity.setCreatedBy(createdBy);
		entity.setRequestId(requestId);
		entity.setBenfAccountNo(hbtfMaster.getBenfAccountNo());
		entity.setBenfName(hbtfMaster.getBenfName());
		entity.setBenfBankName(hbtfMaster.getBenfBankName());
		entity.setCreditAmount(hbtfMaster.getCreditAmount());
		entity.setDebitAmount(hbtfMaster.getDebitAmount());
		entity.setSellRate(hbtfMaster.getSellRate());
		entity.setUserNo(hbtfMaster.getUserNo());
 
		return entity;
	}
}
