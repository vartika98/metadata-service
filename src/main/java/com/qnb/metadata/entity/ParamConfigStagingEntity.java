package com.qnb.metadata.entity;

import java.util.Set;
import java.util.stream.Collectors;

import com.qnb.metadata.dto.parameter.ParameterDetails;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="BKO_T_PARAM_CONFIG_REQ_STAGING")
public class ParamConfigStagingEntity extends AbstractAuditEntity{

	private static final long serialVersionUID = 412921086580571751L;
 
	@Id
	@Column(name = "REQUEST_ID")
	private String requestId;
	
	@Column(name = "WORKFLOW_ID")
	private String workFlowId;
	
	@Column(name = "REQUEST_COMMENTS")
	private String reqComments;
	
	@Column(name="UNIT_ID")       
	private String unitId;
 
	@Column(name="CHANNEL_ID")    
	private String channelId;
 
	@OneToMany(mappedBy = "stagingEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Set<ParamConfigSubStagingEntity> subStagingEntity;
 
	public static ParamConfigStagingEntity toEntity(ParameterDetails param, String requestId, String workflowId, String createdBy) {
		ParamConfigStagingEntity paramStaging = new ParamConfigStagingEntity();
		paramStaging.setRequestId(requestId);
		paramStaging.setWorkFlowId(workflowId);
		paramStaging.setReqComments(param.getMakerComments());
		paramStaging.setUnitId(param.getUnitId());
		paramStaging.setChannelId(param.getChannelId());
		paramStaging.setCreatedBy(createdBy);
		if(param.getParamDetails()!= null) {
			Set<ParamConfigSubStagingEntity> subStaging = param.getParamDetails().stream()
					.map(pm ->ParamConfigSubStagingEntity.toEntity(pm, paramStaging))
					.collect(Collectors.toSet());					
			paramStaging.setSubStagingEntity(subStaging);	
		}
		return paramStaging;
	}
}
