package com.qnb.metadata.entity;

import com.qnb.metadata.dto.appMaintenance.MaintenanceLangDetails;
import com.qnb.metadata.dto.appMaintenance.MessageDetails;
import com.qnb.metadata.utils.RandomUuidGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="BKO_T_MAINTENANCE_MSG_DETAILS_STAGING")
public class MaintenanceMsgLangDetailStagingEntity  extends AbstractAuditEntity {
 
	/**
	 *
	 */
	private static final long serialVersionUID = 3098823692143082000L;
 
	@Id
	@Column(name="ID")				
	private String id;
	
	@Column(name="LANG")
	private String lang;
	
	@Column(name="REASON")
	private String reason;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REQUEST_ID")
	private MaintenanceMsgStagingEntity stagingEntity;
	
	public static MaintenanceMsgLangDetailStagingEntity toEntity(MessageDetails detail,  MaintenanceMsgStagingEntity msgStaging) {
		return MaintenanceMsgLangDetailStagingEntity.builder()
				.id(RandomUuidGenerator.uUIDAsString())
				.lang(detail.getLang())
				.reason(detail.getReason())
				.stagingEntity(msgStaging)
				.build();
	}
	
	public static MaintenanceMsgLangDetailStagingEntity toEntity(MaintenanceLangDetails detail,  MaintenanceMsgStagingEntity msgStaging) {
		return MaintenanceMsgLangDetailStagingEntity.builder()
				.id(RandomUuidGenerator.uUIDAsString())
				.lang(detail.getLang())
				.reason(detail.getReason())
				.stagingEntity(msgStaging)
				.build();
	}
}
