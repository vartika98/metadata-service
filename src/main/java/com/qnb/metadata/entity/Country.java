package com.qnb.metadata.entity;

import com.qnb.metadata.constant.TableConstants;
import com.qnb.metadata.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_COUNTRY_MASTER)
public class Country extends BaseModel{
	private static final long serialVersionUID = 8615705817557328343L;
	 
	@Id
	@Column(name = "TXN_ID", length = 3)
	private String id;
 
	@Column(name = "COUNTRY_CODE",unique=true)
	private String countryCode;
	
	@Column(name = "ISO_COUNTRY_CODE")
	private String isoCountryCode;
	
	@Column(name = "COUNTRY_DESC")
	private String countryDesc;
 
	@Column(name = "QCB", nullable = false, length = 30)
	private String color;
 
	@Column(name = "UNIT_DESC", nullable = false, length = 30)
	private String unitDesc;
 
 
}
