package com.qnb.metadata.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.appMaintenance.ApplicationRequestResponse;
import com.qnb.metadata.dto.parameter.ParameterRequestGetResponse;
import com.qnb.metadata.dto.workflow.GetAllReqDetailsRequest;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.ApplicationMaintenanceService;

import jakarta.validation.Valid;

@RestController()
@RequestMapping("/app-maintenance")
public class ApplicationManitenanceController {
	
	@Autowired
	private ApplicationMaintenanceService applicationMaintenanceService;
	
	@PostMapping("/applications/request/details")
	public GenericResponse<List<ApplicationRequestResponse>> getAllApplicationRequestDetails(@RequestBody @Valid GetAllReqDetailsRequest request) {
		return applicationMaintenanceService.getApplicationRequestByIds(request.getRequestIds());
	}
}
