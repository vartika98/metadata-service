package com.qnb.metadata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.role.GroupRequestResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.RoleService;

@RestController
@RequestMapping("/role")
public class RoleController {
	
	@Autowired
	RoleService roleService;

	@PostMapping("/pending-list")
	public GenericResponse<List<GroupRequestResponse>> getRoleRequest(@RequestBody Map<String, List<String>> reqBody) {
		return roleService.getRoleRequest(reqBody);
	}
}
