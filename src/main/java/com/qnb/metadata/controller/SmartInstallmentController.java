package com.qnb.metadata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.smartInstallment.SmartInstRequestResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.SmartInstallmentService;

@RestController
@RequestMapping("/smartinstallment")
public class SmartInstallmentController {

	@Autowired
	SmartInstallmentService smartInstService;

	@PostMapping("/pending-list")
	public GenericResponse<List<SmartInstRequestResponse>> getPendingRequest(@RequestBody Map<String, List<String>> reqBody) {
		return smartInstService.getPendingRequest(reqBody);
	}
}
