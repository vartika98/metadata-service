package com.qnb.metadata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.ipo.IPORequestResponse;
import com.qnb.metadata.dto.role.GroupRequestResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.IPOService;
import com.qnb.metadata.service.RoleService;

@RestController
@RequestMapping("/ipo")
public class IPOController {

	@Autowired
	IPOService ipoService;

	@PostMapping("/pending-list")
	public GenericResponse<List<IPORequestResponse>> getPendingRequestLst(@RequestBody Map<String, List<String>> reqBody) {
		return ipoService.getPendingRequestLst(reqBody);
	}
}
