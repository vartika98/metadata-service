package com.qnb.metadata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.qnb.metadata.dto.branchList.BranchListResponse;
import com.qnb.metadata.dto.workflow.GetAllReqDetailsRequest;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.BranchListService;

import jakarta.validation.Valid;

@RestController()
@RequestMapping("/branch")
public class BranchListController {
	
	@Autowired
	private BranchListService branchList;
	
	@PostMapping("/list/request/details")
	public GenericResponse<List<BranchListResponse>> getAllPendingTransactions(@RequestBody @Valid GetAllReqDetailsRequest request) {
		return branchList.getBranchListRequestByIds(request.getRequestIds());
	}

}
