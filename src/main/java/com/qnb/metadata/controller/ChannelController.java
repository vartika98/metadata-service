package com.qnb.metadata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.constant.UtilConstants;
import com.qnb.metadata.dto.ChannelListRes;
import com.qnb.metadata.dto.channel.ChannelRequestResponse;
import com.qnb.metadata.dto.creditCard.CCRequestResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.ChannelService;

@RestController
@RequestMapping("/channel")
public class ChannelController {

	@Autowired
	private ChannelService channelServices;

	@GetMapping("/list")
	public GenericResponse<ChannelListRes> getAllChannels() {
		return channelServices.getAllChannels(UtilConstants.DEFAULT_STATUS);
	}
	
	@PostMapping("/pending-list")
	public GenericResponse<List<ChannelRequestResponse>> getPendingRequest(@RequestBody Map<String, List<String>> reqBody) {
		return channelServices.getPendingRequest(reqBody);
	}


}
