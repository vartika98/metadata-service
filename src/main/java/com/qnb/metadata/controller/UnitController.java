package com.qnb.metadata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.constant.UtilConstants;
import com.qnb.metadata.dto.UnitDto;
import com.qnb.metadata.dto.UnitListRes;
import com.qnb.metadata.dto.UnitMasterDto;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.UnitService;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/units")
public class UnitController {

	@Autowired
	HttpServletRequest request;

	@Autowired
	private UnitService unitServices;

	@PostMapping("/all")
	public GenericResponse<UnitListRes> unitListToDto() {
		return unitServices.getAllUnits(UtilConstants.DEFAULT_STATUS);
	}
}
