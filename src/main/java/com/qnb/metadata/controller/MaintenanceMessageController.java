package com.qnb.metadata.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.qnb.metadata.dto.appMaintenance.MaintenanceRequestDetailsResponse;
import com.qnb.metadata.dto.workflow.GetAllReqDetailsRequest;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.MessageMaintenanceService;
import jakarta.validation.Valid;

@RestController()
@RequestMapping("/app-maintenance")
public class MaintenanceMessageController {
	
	@Autowired
	private MessageMaintenanceService messageMaintenanceService;
	
	@PostMapping("/request/details")
	public GenericResponse<List<MaintenanceRequestDetailsResponse>> getAllRequestDetails(@RequestBody @Valid GetAllReqDetailsRequest request) {
		return messageMaintenanceService.getMaintenanceRequestsByIds(request.getRequestIds());
	}
}
