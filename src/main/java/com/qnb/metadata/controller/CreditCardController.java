package com.qnb.metadata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.creditCard.CCRequestResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.CreditCardService;

@RestController
@RequestMapping("/cc")
public class CreditCardController {

	@Autowired
	CreditCardService creditcardService;

	@PostMapping("/pending-list")
	public GenericResponse<List<CCRequestResponse>> getPendingRequest(@RequestBody Map<String, List<String>> reqBody) {
		return creditcardService.getPendingRequest(reqBody);
	}
}
