package com.qnb.metadata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.errorConfig.ErrorConfigRequestResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.ErrorConfigService;

@RestController
@RequestMapping("/error/config")
public class ErrorConfigController {

	@Autowired
	ErrorConfigService errorConfigService;

	@PostMapping("/pending-list")
	public GenericResponse<List<ErrorConfigRequestResponse>> getPendingRequest(@RequestBody Map<String, List<String>> reqBody) {
		return errorConfigService.getPendingRequest(reqBody);
	}
}
