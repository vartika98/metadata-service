package com.qnb.metadata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.role.GroupRequestResponse;
import com.qnb.metadata.dto.user.DeleteUserRequestResponse;
import com.qnb.metadata.dto.user.UserRequestResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.RoleService;
import com.qnb.metadata.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/pending-list")
	public GenericResponse<List<UserRequestResponse>> getUserRequest(@RequestBody Map<String, List<String>> reqBody) {
		return userService.getUserRequest(reqBody);
	}
	
	@PostMapping("delete/pending-list")
	public GenericResponse<List<DeleteUserRequestResponse>> getDeleteUserRequest(@RequestBody Map<String, List<String>> reqBody) {
		return userService.getDeleteUserRequest(reqBody);
	}
}
