package com.qnb.metadata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.hbtf.HBTFGetRequest;
import com.qnb.metadata.dto.hbtf.HBTFRequestGetResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.HBTFService;

import jakarta.validation.Valid;

@RestController()
@RequestMapping("/hbtf")
public class HBTFController {
	
	@Autowired
	private HBTFService htbfService;

	@PostMapping("/details")
	public GenericResponse<List<HBTFRequestGetResponse>> getAllPendingTransactions(@RequestBody @Valid HBTFGetRequest request) {
		return htbfService.getHBTFRequestByIds(request.getRequestIds());
	}
}
