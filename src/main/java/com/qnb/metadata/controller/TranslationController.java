package com.qnb.metadata.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.LabelListRes;
import com.qnb.metadata.dto.ManageLabelReqDto;
import com.qnb.metadata.dto.ManageMenuReqDto;
import com.qnb.metadata.dto.MenuListRes;
import com.qnb.metadata.dto.entitlement.TxnEntitlementRequestGetResponse;
import com.qnb.metadata.dto.label.LabelRequestResponse;
import com.qnb.metadata.dto.role.GroupRequestResponse;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.repo.ChannelRespository;
import com.qnb.metadata.repo.TranslationRepository;
import com.qnb.metadata.repo.UnitRespository;
import com.qnb.metadata.service.TranslationService;

@RestController
@RequestMapping("/txn")
public class TranslationController {
	
	@Autowired
	TranslationRepository repo;
	
	@Autowired
	ChannelRespository channelRepo;
	
	@Autowired
	UnitRespository unitRepo;
	
	@Autowired
	TranslationService translationService;
	
	@PostMapping("/menus/list")
	public GenericResponse<MenuListRes> menuList(@RequestBody Map<String,String> reqBody) {
		return translationService.moduleList(reqBody);
	}
	
	@PostMapping("/menus/sub-menu")
	public GenericResponse<MenuListRes> screenList(@RequestBody Map<String,String> reqBody) {
		return translationService.screenList(reqBody);
	}
	
	@PostMapping("/labels")
	public GenericResponse<LabelListRes> labelList(@RequestBody Map<String,String> reqBody) {
		return translationService.labelList(reqBody);
	}
	
	@PostMapping("/manage-labels")
	public GenericResponse<Map<String,String>> manageLabels(@RequestBody ManageLabelReqDto reqBody) {
		return translationService.manageLabels(reqBody);
	}
	
	@PostMapping("/manage-menus")
	public GenericResponse<Map<String, String>> manageMenus(@RequestBody ManageMenuReqDto reqBody) {
		return translationService.manageMenus(reqBody);
	}
	
	@PostMapping("/pending-list")
	public GenericResponse<List<TxnEntitlementRequestGetResponse>> getEntitlementRequest(@RequestBody Map<String, List<String>> reqBody) {
		return translationService.getEntitlementRequest(reqBody);
	}
	
	@PostMapping("labels/pending-list")
	public GenericResponse<List<LabelRequestResponse>> getLabelRequest(@RequestBody Map<String, List<String>> reqBody) {
		return translationService.getLabelRequest(reqBody);
	}
	

}
