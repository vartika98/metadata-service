package com.qnb.metadata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.b2b.B2BResponse;
import com.qnb.metadata.dto.workflow.GetAllReqDetailsRequest;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.B2BService;

import jakarta.validation.Valid;

@RestController()
@RequestMapping("/b2b")
public class B2BController {
	
	@Autowired
	private B2BService b2bService;
	
	@PostMapping("/details")
	public GenericResponse<List<B2BResponse>> getAllPendingTransactions(@RequestBody @Valid GetAllReqDetailsRequest request) {
		return b2bService.getB2BRequestByIds(request.getRequestIds());
	}

}
