package com.qnb.metadata.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qnb.metadata.dto.parameter.ParameterRequestGetResponse;
import com.qnb.metadata.dto.parameter.ParameterGetRequest;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.service.ParameterService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/parmeter")
public class ParameterController {
	
	@Autowired
	private ParameterService parameterService;
	
	@PostMapping("/request/details")
	public GenericResponse<List<ParameterRequestGetResponse>> getDetails(@RequestBody @Valid ParameterGetRequest request) {
		return parameterService.getParameterRequestsByIds(request.getRequestIds());

	}

}
