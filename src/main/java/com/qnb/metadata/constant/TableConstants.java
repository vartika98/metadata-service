package com.qnb.metadata.constant;

public class TableConstants {

	private TableConstants() {

		throw new IllegalStateException("TableConstants class");

	}

	public static final String TABLE_ERROR_CONF = "OCS_T_ERROR_CONFIG";

	public static final String TABLE_CURRENCY_MASTER = "OCS_T_CURRENCY";

	public static final String TABLE_CURRENCY_DESC = "OCS_T_CURRENCY_DESC";

	public static final String TABLE_PRODUCT_MASTER = "OCS_T_PROD_MASTER";

	public static final String TABLE_SUB_PRODUCT_MASTER = "OCS_T_SUB_PROD_MASTER";
	public static final String TABLE_COUNTRY_MASTER = "OCS_T_COUNTRY";

	// metadata

	public static final String TABLE_CHANNEL_MASTER = "OCS_T_CHANNEL_MASTER";

	public static final String TABLE_CONFIG = "OCS_T_PARAM_CONFIG";

	public static final String TABLE_PAGE_CONFIG = "OCS_T_SCREEN_CONFIG";

	public static final String TABLE_TXN_ENTITLEMENT = "OCS_T_TXN_ENTITLEMENT";

	public static final String TABLE_LANGUAGE_MASTER = "OCS_T_LANGUAGE_MASTER";

	public static final String TABLE_MENU_MASTER = "OCS_T_MENU_MASTER";

	public static final String TABLE_TRANSLATION = "OCS_T_I18N";

	public static final String TABLE_MOB_TRANSLATION = "OCS_T_MOB_I18N";

	public static final String TABLE_UNIT_MASTER = "OCS_T_UNIT_MASTER";

	public static final String OCS_T_UNIT_DESC = "OCS_T_UNIT_DESC";

	public static final String TABLE_UNIT_LANGUAGE = "OCS_T_UNIT_LANGUAGE";

	public static final String TABLE_SCREEN_ENT = "OCS_T_SCREEN_ENTITLEMENT";

	// user

	public static final String TABLE_USER_SETTINGS = "OCS_T_USER_SETTINGS";

	public static final String TABLE_USER_LAST_LOGIN = "OCS_T_USER_LAST_LOGIN";

	public static final String TABLE_URL_PROVIDER = "OCS_T_URL_PROVIDER";

	public static final String TABLE_RR_MESSAGE = "OCS_T_RR_MESSAGES";
	
	// App Maintenance
	
	public static final String TABLE_APP_MAINTENANCE_STAGING = "BKO_T_APPLICATION_REQUEST_STAGING";
	public static final String TABLE_APP_MAINTENANCE_SUB_STAGING = "BKO_T_APPLICATION_SUB_REQUEST_STAGING";
	



}
