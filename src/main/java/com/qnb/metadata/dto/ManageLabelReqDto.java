package com.qnb.metadata.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@Builder
public class ManageLabelReqDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String unitId;

	private String channelId;
	
	private String screenId;
	
	private String menuId;
	
	private List<LabelDetails> labelDetails;

}
