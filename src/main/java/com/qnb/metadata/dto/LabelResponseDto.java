package com.qnb.metadata.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@Builder
public class LabelResponseDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String unitId;

	private String channelId;
	
	private String screenId;
	
	private String labelKey;

	private List<LabelDescription> labelValue;
	
	private String status;
	
	private String description;
	
	

}
