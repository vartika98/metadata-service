package com.qnb.metadata.dto.channel;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.enums.TxnStatus;
import com.qnb.metadata.exception.ResourceNotFoundException;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChannelRequestResponse {

	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private ChannelStagedRequestDto channelDetails;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public static ChannelRequestResponse channelRequestResponse(Message message) {
		var channelObject = message.getPayload();
		if (channelObject instanceof ChannelStagedRequest) {
			ChannelStagedRequest channel = (ChannelStagedRequest) channelObject;
			return ChannelRequestResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.makerComments(message.getLatestMakerComment()).checkerComments(message.getLatestCheckerComment())
					.channelDetails(formChannelReq(channel)).workflowHistory(message.getWorkflowHistory()).unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException("NA", "There is no channel request object found", HttpStatus.NOT_FOUND);

	}

	public static ChannelStagedRequestDto formChannelReq(ChannelStagedRequest channel) {
		return ChannelStagedRequestDto.builder().action(channel.getAction()).channelId(channel.getChannelId())
				.channelDesc(channel.getChannelDesc()).status(channel.getStatus()).description(channel.getDescription())
				.makerComments(channel.getMakerComments()).build();
	}

}
