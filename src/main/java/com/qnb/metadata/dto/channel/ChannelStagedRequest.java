package com.qnb.metadata.dto.channel;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.enums.TxnStatus;
import com.qnb.metadata.service.workflow.Workflowable;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChannelStagedRequest implements Workflowable {

	private static final String SALT = "channel";
	
	@NotBlank(message = "{requestId.notEmpty}")
	@Size(max = 50, message = "{requestId.exceedLimit}")
	private String requestId;

	private Long txnId;

	@NotBlank(message = "{channelId.notEmpty}")
	@Size(max = 3, message = "{channelId.exceedLimit}")
	private String channelId;

	@NotBlank(message = "{channelDesc.notEmpty}")
	@Size(max = 50, message = "{channelDesc.exceedLimit}")
	private String channelDesc;

	private TxnStatus status;

	@Size(max = 1000, message = "{description.exceedLimit}")
	private String description;

	@Size(max = 100, message = "{makerComments.exceedLimit}")
	private String makerComments;

	private String action;

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + channelId);
	}

	@Override
	public String getIdempotentRequestDetails() {

		return "Request : Channel Management , ChannelId :" + channelId;
	}
}
