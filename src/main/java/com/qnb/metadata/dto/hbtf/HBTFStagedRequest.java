package com.qnb.metadata.dto.hbtf;

import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HBTFStagedRequest {
 
	@Size(max = 15, message = "{gId.exceedLimit}")
	private String globalId;
 
	@NotBlank(message = "{loginIp.notEmpty}")
	@Size(max = 25, message = "{loginIp.exceedLimit}")
	private String loginIp;
 
	@Size(max = 100, message = "{makerComments.exceedLimit}")
	private String makerComments;
 
	@NotBlank(message = "{requestId.notEmpty}")
	@Size(max = 50, message = "{requestId.exceedLimit}")
	private String requestId;
 
	private String status;
 
	private String type;
 
	@Size(max = 15, message = "{unitId.exceedLimit}")
	private String unitId;
 
	@Size(max = 15, message = "{userId.exceedLimit}")
	private String userId;
 
	private String refNo;
	private String debitAccountNo;
	private String requestedBy;
	private String requestedDate;
	private String rejectedBy;
	private String rejectedDate;
	private int rejectCount;
	private String checkerComments;
	private String benfName;
	private String benfBankName;
	private String benfAccountNo;
	private String creditAmount;
	private String debitAmount;
	private double sellRate;
	private byte[] userNo;
	public static HBTFMaster normalize(HBTFStagedRequest request) {
 
		return HBTFMaster.fromRequest(request.getLoginIp(), request.getUserId(), request.getMakerComments(),
				request.getUnitId(), request.getGlobalId(), request.getRefNo(), request.getDebitAccountNo(),
				request.getRequestedBy(), request.getRequestedDate(), request.getStatus(), request.getRejectedDate(),
				request.getRejectedBy(), request.getRejectCount(), request.getCheckerComments(), request.getBenfName(),
				request.getBenfBankName(), request.getBenfAccountNo(), request.getCreditAmount(),
				request.getDebitAmount(), request.getSellRate(),request.getUserNo());
	}
}
