package com.qnb.metadata.dto.hbtf;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HBTFRequestGetResponse {
	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private HBTFStagedRequest hbtfStagedRequest;
	private List<WorkflowHistory> workflowHistory;
	private String unit;
	
	public static HBTFRequestGetResponse hbtfRequestGetResponse(Message message) {
		var hbtfObject = message.getPayload();
		if (hbtfObject instanceof HBTFMaster) {
			var hbtfDetails = (HBTFMaster) hbtfObject;
			return HBTFRequestGetResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.makerComments(message.getLatestMakerComment())
					.checkerComments(message.getLatestCheckerComment())
					.hbtfStagedRequest(toModifyHBTFStagedRequest(hbtfDetails))
					.workflowHistory(message.getWorkflowHistory())
					.unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException("NA", "There is no credit card request object found",
				HttpStatus.NOT_FOUND);
 
	}
	public static HBTFStagedRequest toModifyHBTFStagedRequest(HBTFMaster request) {
		return  HBTFStagedRequest.builder()
				.globalId(request.getGlobalId())
				.userId(request.getUserId())
				.makerComments(request.getRequestComments())
				.unitId(request.getUnitId())
				.status(request.getStatus())
				.refNo(request.getRefNo())
				.debitAccountNo(request.getDebitAccountNo())
				.requestedBy(request.getRequestedBy())
				.requestedDate(request.getRequestedDate())
				.rejectedBy(request.getRejectedBy())
				.rejectedDate(request.getRejectedDate())
				.rejectCount(request.getRejectCount())
				.build();
	}
}
