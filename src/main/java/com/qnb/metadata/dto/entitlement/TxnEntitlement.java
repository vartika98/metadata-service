package com.qnb.metadata.dto.entitlement;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@Builder
public class TxnEntitlement {

	@EqualsAndHashCode.Exclude
	private final String parentId;
	private final String menuId;
	private final String unitId;
	private final String channelId;
	private final String status;
	private String externalId;

	public static TxnEntitlement toTxnEntitlement(TransactionEntitlementModel txn) {
		return TxnEntitlement.builder().menuId(txn.getMenuId()).unitId(txn.getUnitId()).channelId(txn.getChannelId())
				.parentId(txn.getParentId()).status(txn.getStatus()).build();
	}

	public static TxnEntitlement toTxnEntitlement(String menuId, String unitId, String channelId, String parentId) {
		return TxnEntitlement.builder().menuId(menuId).unitId(unitId).channelId(channelId).parentId(parentId).build();
	}

	public static TxnEntitlement toTxnEntitlementStatus(String menuId, String unitId, String channelId, String status) {
		return TxnEntitlement.builder().menuId(menuId).unitId(unitId).channelId(channelId).status(status).build();
	}
}
