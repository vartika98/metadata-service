package com.qnb.metadata.dto.entitlement;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TxnEntitlementReqResponse {

	private String parentId;
    private String menuId;
    private String unitId;
    private String channelId;
    private String status;
}
