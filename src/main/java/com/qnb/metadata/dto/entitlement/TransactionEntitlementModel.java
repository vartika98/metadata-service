package com.qnb.metadata.dto.entitlement;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionEntitlementModel {

	@NotBlank(message = "{parentId.notEmpty}")
    @Size(max = 255, message = "{parentId.exceedLimit}")
    private String parentId;
    
    @NotBlank(message = "{menuId.notEmpty}")
    @Size(max = 255, message = "{menuId.exceedLimit}")
    private String menuId;
    
    @Size(max = 3, message = "{unitId.exceedLimit}")
    private String unitId;
    
    @Size(max = 3, message = "{channelId.exceedLimit}")
    private String channelId;
    
    @Size(max = 3, message = "{status.exceedLimit}")
    private String status;
}
