package com.qnb.metadata.dto.entitlement;

import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;

import com.qnb.metadata.enums.ExceptionMessages;
import com.qnb.metadata.exception.ValidationException;
import com.qnb.metadata.service.workflow.Workflowable;
import com.qnb.metadata.utils.TimeZoneUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionEntitlementDto implements Workflowable {
	private static final String SALT = "transactionEntitlement";

	private Set<TxnEntitlement> txnEntitlement;
	private String excuteAction;
	private String unitId;
	private String channelId;
	private Instant scheduledTime;
	private String scheduledTimeText;
	private String scheduledTimeZone;
	private String action;
	private String makerComments;

	public static TransactionEntitlementDto fromRequest(List<TransactionEntitlementModel> txnEntitlement,
			String actionTobe, String scheduledTime, String zoneId, String unitId, String channelId,
			String makerComments, String action) {
		var entitlements = getTxnEntitlement(txnEntitlement);
		if (txnEntitlement != null && entitlements.size() < txnEntitlement.size()) {
			throw new ValidationException(ExceptionMessages.TXN_ENTITLEMENT_REQUEST_DUPLICATE.getErrorCode(),
					ExceptionMessages.TXN_ENTITLEMENT_REQUEST_DUPLICATE.getErrorDescription(), HttpStatus.BAD_REQUEST);
		}
		return TransactionEntitlementDto.builder().excuteAction(actionTobe)
				.scheduledTime(getScheduleTime(actionTobe, scheduledTime, zoneId)).scheduledTimeText(scheduledTime)
				.scheduledTimeZone(zoneId).action(action).txnEntitlement(getTxnEntitlement(txnEntitlement))
				.unitId(unitId).channelId(channelId).makerComments(makerComments).build();
	}

	private static Instant getScheduleTime(String actionTobe, String scheduledTime, String zoneId) {
		if ("Scheduled".equalsIgnoreCase(actionTobe)) {
			return TimeZoneUtil.getUtcTime(scheduledTime, zoneId);
		}
		return null;
	}

	public static Set<TxnEntitlement> getTxnEntitlement(List<TransactionEntitlementModel> txnEntitlement) {
		Set<TxnEntitlement> txnEntitlementSet = new LinkedHashSet<>();
		if (txnEntitlement != null) {
			txnEntitlementSet = txnEntitlement.stream().map(TxnEntitlement::toTxnEntitlement)
					.collect(Collectors.toCollection(LinkedHashSet::new));
		}
		return txnEntitlementSet;
	}

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + unitId + channelId);
	}

	@Override
	public String getIdempotentRequestDetails() {

		return String.format("Request : Transaction Entitlement, UnitId : %s, ChannelId : %s", unitId, channelId);
	}

}
