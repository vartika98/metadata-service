package com.qnb.metadata.dto.entitlement;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TxnEntitlementRequestGetResponse {

	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String scheduledTime;
	private String timeZone;
	private String excuteAction;
	private String unitId;
	private String channelId;
	private List<TxnEntitlementReqResponse> txnEntitlement;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public static TxnEntitlementRequestGetResponse txnEntitlementRequestGetResponse(Message message) {
		var txnObject = message.getPayload();
		if (txnObject instanceof TransactionEntitlementDto) {
			var txn = (TransactionEntitlementDto) txnObject;
			return TxnEntitlementRequestGetResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.excuteAction(txn.getExcuteAction()).scheduledTime(txn.getScheduledTimeText())
					.timeZone(txn.getScheduledTimeZone()).unitId(txn.getUnitId()).channelId(txn.getChannelId())
					.makerComments(message.getLatestMakerComment()).checkerComments(message.getLatestCheckerComment())
					.workflowHistory(message.getWorkflowHistory()).txnEntitlement(getTxnEntitlements(txn))
					.unit(message.getUnit()).build();
		}
		throw new ResourceNotFoundException("NA", "There is no txn request entitlement object found",
				HttpStatus.NOT_FOUND);
	}

	private static List<TxnEntitlementReqResponse> getTxnEntitlements(TransactionEntitlementDto txnDomain) {
		if (txnDomain.getTxnEntitlement() != null) {
			return txnDomain.getTxnEntitlement().stream()
					.map(t -> TxnEntitlementReqResponse.builder().menuId(t.getMenuId()).unitId(t.getUnitId())
							.channelId(t.getChannelId()).parentId(t.getParentId()).status(t.getStatus()).build())
					.collect(Collectors.toList());
		}
		return new ArrayList<>();
	}
}
