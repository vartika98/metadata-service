package com.qnb.metadata.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MenusDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String menuId;
	private String parentId;
	private String status;
	

	
}
