package com.qnb.metadata.dto;

import java.io.Serializable;

import java.util.Date;

import java.util.HashMap;

import java.util.Map;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Response implements Serializable {

	private static final long serialVersionUID = 1L;

	private final HttpStatus respcode;

	private final String message;

	private final Map<String, Object> response;

	@JsonProperty(value = "responsedate")

	private final Date date;

	public static class Builder {

		private final Date date;

		private final HttpStatus respcode;

		private final String message;

		private Map<String, Object> response;

		public Builder(final HttpStatus respcode, final String message) {

			this.respcode = respcode;

			this.message = message;

			date = new Date();

		}

		public Builder addResponse(final String key, final Object value) {

			if (response == null) {

				response = new HashMap<>();

			}

			response.put(key, value);

			return this;

		}

		public Response build() {

			return new Response(this);

		}

		public Date getDate() {

			return date;

		}

	}

	public Response() {

		message = null;

		respcode = null;

		response = null;

		date = null;

	}

	public Response(final Builder builder) {

		message = builder.message;

		respcode = builder.respcode;

		response = builder.response;

		date = builder.date;

	}

	public HttpStatus getRespcode() {

		return respcode;

	}

	public String getMessage() {

		return message;

	}

	public Map<String, Object> getresponse() {

		return response;

	}

	public Date getDate() {

		return date;

	}

}
