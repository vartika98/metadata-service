package com.qnb.metadata.dto.role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.enums.ActionType;
import com.qnb.metadata.enums.ExceptionMessages;
import com.qnb.metadata.exception.ResourceNotFoundException;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GroupRequestResponse {

	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String groupId;
	private String groupCode;
	private String groupDescription;
	private List<String> roles;
	private ActionType action;
	private String processStatus;
	private Set<String> availableActions;
	private String makerComments;
	private String checkerComments;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private String unit;
	private List<WorkflowHistory> workflowHistory;

	public static GroupRequestResponse groupRequestResponse(Message message) {
		var groupObject = message.getPayload();
		if (groupObject instanceof GroupDef) {
			var group = (GroupDef) groupObject;
			return GroupRequestResponse.builder().requestId(message.getRequestId()).workflowId(message.getWorkflowId())
					.workFlowStatus(message.getWorkflowStatus().toString()).groupId(group.getGroupId())
					.groupCode(group.getGroupCode()).groupDescription(group.getGroupDescription()).unit(group.getUnit())
					.roles(group.getRoles().stream().sorted((o1, o2) -> o1.compareTo(o2)).collect(Collectors.toList()))
					.action(group.getAction()).processStatus(message.getProcessStatus())
					.availableActions(message.getAvailableActions()).makerComments(message.getLatestMakerComment())
					.checkerComments(message.getLatestCheckerComment()).createdBy(message.getCreatedBy())
					.createdTime(message.getCreatedTime()).lastModifiedBy(message.getLastModifiedBy())
					.lastModifiedTime(message.getLastModifiedTime()).workflowHistory(message.getWorkflowHistory())
					.build();
		}
		throw new ResourceNotFoundException(ExceptionMessages.WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION.getErrorCode(),
				ExceptionMessages.WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION.getErrorDescription(), HttpStatus.NOT_FOUND);

	}
}
