package com.qnb.metadata.dto.role;

import java.util.Set;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UnitRole {

	private String role;

	private Set<String> units;
}
