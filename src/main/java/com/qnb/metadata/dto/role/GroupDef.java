package com.qnb.metadata.dto.role;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.qnb.metadata.enums.ActionType;
import com.qnb.metadata.enums.GroupType;
import com.qnb.metadata.service.workflow.Workflowable;
import com.qnb.metadata.utils.RandomUuidGenerator;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GroupDef implements Workflowable {
	private static final String SALT = "group:";
	private final String groupId;
	private final String groupCode;
	private final GroupType groupType;
	private final String groupDescription;
	private Set<String> roles;
	private final String makerComments;
	private final String unit;
	private final String unitDesc;
	private final ActionType action;

	public static GroupDef fromRequest(ActionType actionType, String groupId, String groupCode, String groupDescription,
			Set<String> roles, String makerComments, String unit) {
		return GroupDef.builder().groupId(actionType == ActionType.ADD ? RandomUuidGenerator.uUIDAsString() : groupId).groupCode(groupCode)
				.groupDescription(groupDescription).roles(roles).makerComments(makerComments).action(actionType)
				.unit(unit).build();
	}

	public static GroupDef fromRequest(ActionType actionType, String groupId, String makerComments) {
		return GroupDef.builder().groupId(groupId).makerComments(makerComments).action(actionType).build();
	}

	public GroupDef createNewGroupDomainByUpdating(ActionType actionType, String makerComments) {
		return GroupDef.builder().groupId(groupId).groupCode(groupCode).groupDescription(groupDescription).roles(roles)
				.makerComments(makerComments).action(actionType).build();
	}

	public static Set<GroupDef> createGroupDefFromId(Set<String> groupId) {
		return groupId.stream().map(id -> GroupDef.builder().groupId(id).build()).collect(Collectors.toSet());
	}

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + groupCode);
	}

	@Override
	public String getIdempotentRequestDetails() {

		return String.format("Request : Role Management, Role : %s", groupCode);
	}
}
