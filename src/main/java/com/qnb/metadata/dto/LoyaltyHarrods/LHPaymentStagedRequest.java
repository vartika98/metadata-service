package com.qnb.metadata.dto.LoyaltyHarrods;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.service.workflow.Workflowable;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LHPaymentStagedRequest implements Workflowable {

	private static final String SALT = "LHP:";

	@NotBlank(message = "{customerName.notEmpty}")
	@Size(max = 100, message = "{customerName.exceedLimit}")
	private String customerName;

	@Size(max = 15, message = "{gId.exceedLimit}")
	private String globalId;

	@NotBlank(message = "{loginIp.notEmpty}")
	@Size(max = 25, message = "{loginIp.exceedLimit}")
	private String loginIp;

	@NotBlank(message = "{accountNumber.notEmpty}")
	@Size(max = 25, message = "{accountNumber.exceedLimit}")
	private String accountNumber;

	@Size(max = 25, message = "{pointBalance.exceedLimit}")
	private String pointBalance;

	@NotBlank(message = "{memberShipNo.notEmpty}")
	@Size(max = 25, message = "{memberShipNo.exceedLimit}")
	private String memberShipNo;

	@Size(max = 15, message = "{mobileNumber.exceedLimit}")
	private String mobileNumber;

	@Size(max = 25, message = "{loginUserId.exceedLimit}")
	private String loginUserId;

	@Size(max = 15, message = "{paymentAmount.exceedLimit}")
	private String paymentAmount;

	@Size(max = 25, message = "{redeemPoints.exceedLimit}")
	private String redeemPoints;

	@Size(max = 25, message = "{reason.exceedLimit}")
	private String reason;

	@Size(max = 100, message = "{makerComments.exceedLimit}")
	private String makerComments;

	@NotBlank(message = "{requestId.notEmpty}")
	@Size(max = 50, message = "{requestId.exceedLimit}")
	private String requestId;

	private String status;

	private String type;

	@Size(max = 15, message = "{unitId.exceedLimit}")
	private String unitId;

	@Size(max = 15, message = "{userId.exceedLimit}")
	private String userId;

	@NotBlank(message = "{nationalId.notEmpty}")
	@Size(max = 25, message = "{nationalId.exceedLimit}")
	private String nationalId;

	private Long txnId;

	private String redeemEqaPoints;

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + globalId + memberShipNo);
	}

	@Override
	public String getIdempotentRequestDetails() {
		return "Request : Loyalty Harrods Payment for user with userId :" + userId;
	}
}
