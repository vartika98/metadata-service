package com.qnb.metadata.dto.LoyaltyHarrods;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LHPaymentRequestResponse {

	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private LHPaymentStagedReqDto lhPaymentDetails;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public static LHPaymentRequestResponse lhPaymentRequestResponse(Message message) {
		var lhPaymentObject = message.getPayload();
		if (lhPaymentObject instanceof LHPaymentStagedRequest) {
			var lhPaymentDetails = (LHPaymentStagedRequest) lhPaymentObject;
			return LHPaymentRequestResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.makerComments(message.getLatestMakerComment()).checkerComments(message.getLatestCheckerComment())
					.lhPaymentDetails(toModifyLHPaymentStagedRequest(lhPaymentDetails)).workflowHistory(message.getWorkflowHistory())
					.unit(message.getUnit()).build();
		}
		throw new ResourceNotFoundException("NA", "There is no credit card request object found", HttpStatus.NOT_FOUND);

	}

	public static LHPaymentStagedReqDto toModifyLHPaymentStagedRequest(LHPaymentStagedRequest request) {
		return LHPaymentStagedReqDto.builder().customerName(request.getCustomerName()).globalId(request.getGlobalId())
				.loginIp(request.getLoginIp()).accountNumber(request.getAccountNumber())
				.pointBalance(request.getPointBalance()).memberShipNo(request.getMemberShipNo())
				.mobileNumber(request.getMobileNumber()).paymentAmount(request.getPaymentAmount())
				.redeemPoints(request.getRedeemPoints()).redeemEqaPoints(request.getRedeemEqaPoints())
				.loginUserId(request.getLoginUserId()).userId(request.getUserId())
				.makerComments(request.getMakerComments()).unitId(request.getUnitId()).status(request.getStatus())
				.nationalId(request.getNationalId()).build();
	}
}
