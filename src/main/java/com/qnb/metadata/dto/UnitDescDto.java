package com.qnb.metadata.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class UnitDescDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String unitId;

	private String unitDesc;
}
