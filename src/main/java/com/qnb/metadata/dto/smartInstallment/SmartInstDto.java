package com.qnb.metadata.dto.smartInstallment;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmartInstDto implements Workflowable {

	private static final String SALT = "OP:";
	
	private String requestId;
	private String globalId;
	private String userId;
	private String requestComments;
	private String workflowId;
	private String unit;
	private String instRefNo;
	private String sortKey;
	private String key;
	private String value;
	private String planSerNo;
	private String feePercentage;
	private String txnAmount;
	private String availBal;
	private String totalOTB;
	private String totalDebitOS;
	private String totalInstOTB;
	private String txnSerNo;
	private String cardLimit;
	private String encCardNumber;
	private String cardNo;
	private String trxnPartitionKey;
	private String channel;
	private String loginIp;
	private String txnDate;
	private String txnDesc;
	
	@Override
    public List<String> getIdempotentRequestIdentifier() {
                    return List.of(SALT + globalId);
    }

    @Override
    public String getIdempotentRequestDetails() {
                    return "Request : Smartinstallment for user with userId :"+/*AuthContextUtil.getUserName()*/"TQA1082";
    }

}
