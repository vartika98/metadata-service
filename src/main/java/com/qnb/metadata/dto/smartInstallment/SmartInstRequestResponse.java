package com.qnb.metadata.dto.smartInstallment;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmartInstRequestResponse {

	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private SmartInstMaster smartInstDetails;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public static SmartInstRequestResponse smartInstRequestResponse(Message message) {
		var smartInstallmentObject = message.getPayload();
		if (smartInstallmentObject instanceof SmartInstDto) {
			var smartInstallment = (SmartInstDto) smartInstallmentObject;
			return SmartInstRequestResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.makerComments(message.getLatestMakerComment()).checkerComments(message.getLatestCheckerComment())
					.workflowHistory(message.getWorkflowHistory())
					.smartInstDetails(toModifySmartinstallmentStagedRequest(smartInstallment)).unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException("NA", "There is no  request object found", HttpStatus.NOT_FOUND);

	}

	public static SmartInstMaster toModifySmartinstallmentStagedRequest(SmartInstDto request) {
		return SmartInstMaster.builder().loginIp(request.getLoginIp()).userId(request.getUserId())
				.globalId(request.getGlobalId()).requestComments(request.getRequestComments()).channel("BO")
				.cardNo(request.getCardNo()).instRefNo(request.getInstRefNo()).txnAmount(request.getTxnAmount())
				.txnDesc(request.getTxnDesc()).txnDate(request.getTxnDate()).key(request.getKey())
				.value(request.getValue()).txnSerNo(request.getTxnSerNo()).build();

	}
}
