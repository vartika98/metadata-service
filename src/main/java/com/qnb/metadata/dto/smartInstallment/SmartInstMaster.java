package com.qnb.metadata.dto.smartInstallment;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmartInstMaster {

	private String requestId;
	private String globalId;
	private String userId;
	private String requestComments;
	private String workflowId;
	private String unit;
	private String instRefNo;
	private String sortKey;
	private String key;
	private String value;
	private String planSerNo;
	private String feePercentage;
	private String txnAmount;
	private String availBal;
	private String totalOTB;
	private String totalDebitOS;
	private String totalInstOTB;
	private String txnSerNo;
	private String cardLimit;
	private String encCardNumber;
	private String cardNo;
	private String trxnPartitionKey;
	private String channel;
	private String loginIp;
	private String txnDate;
	private String txnDesc;
}
