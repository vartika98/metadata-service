package com.qnb.metadata.dto;

import java.io.Serializable;

import com.qnb.metadata.dto.user.UnitMaster;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UnitMasterDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String unitCode;
	private String unitDesc;
	private String countryCode;
	private String countryDesc;
	private String status;
	
	
	public static UnitMasterDto unitMasterResponse(UnitMaster unitMaster) {
		return UnitMasterDto.builder()
				.unitCode(unitMaster.getUnitId())
				.unitDesc(unitMaster.getUnitDesc())
				.countryCode(unitMaster.getCountryCode())
				.countryDesc(unitMaster.getCountryDesc())
				.status(unitMaster.getStatus())
				.build();
	}

}
