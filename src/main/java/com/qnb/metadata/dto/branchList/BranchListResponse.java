package com.qnb.metadata.dto.branchList;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BranchListResponse {
	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private BranchListStagedRequest errorDetails;
	 private List<WorkflowHistory> workflowHistory;
	 private String unit;
	public static BranchListResponse branchListResponse(Message message) {
		var branchListObject = message.getPayload();
		if (branchListObject instanceof BranchListMaster) {
			var errorConfig = (BranchListMaster) branchListObject;
			return BranchListResponse.builder()
					.requestId(message.getRequestId())
					.workflowId(message.getWorkflowId())
					.workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus())
					.availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy())
					.createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy())
					.lastModifiedTime(message.getLastModifiedTime())
					.makerComments(message.getLatestMakerComment())
					.checkerComments(message.getLatestCheckerComment())
					.errorDetails(toModifyBranchListStagedRequest(errorConfig))
					.workflowHistory(message.getWorkflowHistory())
					.unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException("NA", "There is no credit card request object found",
				HttpStatus.NOT_FOUND);
 
	}
	public static BranchListStagedRequest toModifyBranchListStagedRequest(BranchListMaster request) {
		return  BranchListStagedRequest.builder()
				.unitId(request.getUnitId())
				.makerComments(request.getRemarks())
				.paramDetails(request.getParamDetails())
				.build();
		
	}
	
}
