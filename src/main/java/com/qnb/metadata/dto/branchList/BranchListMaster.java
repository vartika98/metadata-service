package com.qnb.metadata.dto.branchList;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BranchListMaster implements Workflowable {
	private static final String SALT = "ERC:";
 
	private int txnId;
	private String unitId;
	private String remarks;
 
	
	private List<BranchParameterRequest> paramDetails;
 
	public static BranchListMaster fromRequest(BranchListStagedRequest request) {
		return BranchListMaster.builder()
				.unitId(request.getUnitId())
				.remarks(request.getMakerComments())
				.paramDetails(request.getParamDetails())
				.build();
	}
 
	
	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + paramDetails.get(0).getModifiedBy() + unitId);
	}
 
	@Override
	public String getIdempotentRequestDetails() {
 
		return "Request : Error Configuration for user with serviceType :" + unitId;
	}
}
