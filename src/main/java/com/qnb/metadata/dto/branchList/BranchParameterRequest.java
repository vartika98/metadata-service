package com.qnb.metadata.dto.branchList;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BranchParameterRequest {
	
	private int txnId;
	
	private String action;
	
	private String countryCode;
 
	private String branchCode;
	
	private String branchDesc;
	
	private String branchDescAr;
	
	private String branchDescFr;
		
	private String city;
	
	private String address;
	
	private String collectionBranchFlag;
	
	private String status;
	
	private String createdBy;
 
	private Date dateCreated;
	
	private String modifiedBy;
	
	private Date dateModified;
	
	private int branchId;
}
