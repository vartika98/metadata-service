package com.qnb.metadata.dto.branchList;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BranchListStagedRequest {
	
	private int txnId;	
 
	private String unitId;
 
	private String makerComments;
	
	private List<BranchParameterRequest> paramDetails;
 
	public static BranchListMaster normalize(BranchListStagedRequest request) {
			
		return BranchListMaster.fromRequest(request);
	}
	
}
