package com.qnb.metadata.dto.user;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UnitMaster {
	private String unitId;
	private String unitDesc;
	private String countryCode;
	private String countryDesc;
	private String status;
	
}
