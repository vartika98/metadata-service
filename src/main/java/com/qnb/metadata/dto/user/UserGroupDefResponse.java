package com.qnb.metadata.dto.user;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.role.GroupDef;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserGroupDefResponse {
    private String groupId;
    private String groupCode;
    private String groupDescription;
    private Set<String> roles;
 
    public static UserGroupDefResponse userGroupDefResponse(GroupDef groupDef) {
        return UserGroupDefResponse.builder()
                .groupId(groupDef.getGroupId())
                .groupCode(groupDef.getGroupCode())
                .groupDescription(groupDef.getGroupDescription())
                .roles(groupDef.getRoles())
                .build();
    }
}
