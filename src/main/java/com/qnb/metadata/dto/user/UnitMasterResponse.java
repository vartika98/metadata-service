package com.qnb.metadata.dto.user;

import com.fasterxml.jackson.annotation.JsonInclude;
 
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
 
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UnitMasterResponse {
	private String unitId;
	private String unitDesc;
	private String countryCode;
	private String countryDesc;
	private String status;
	public static UnitMasterResponse unitMasterResponse(UnitMaster unitMaster) {
		return UnitMasterResponse.builder()
				.unitId(unitMaster.getUnitId())
				.unitDesc(unitMaster.getUnitDesc())
				.countryCode(unitMaster.getCountryCode())
				.countryDesc(unitMaster.getCountryDesc())
				.status(unitMaster.getStatus())
				.build();
	}
 
}
