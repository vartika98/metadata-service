package com.qnb.metadata.dto.user;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.qnb.metadata.dto.role.GroupDef;
import com.qnb.metadata.enums.ActionType;
import com.qnb.metadata.enums.ExceptionMessages;
import com.qnb.metadata.enums.UserStatus;
import com.qnb.metadata.enums.UserType;
import com.qnb.metadata.exception.ValidationException;
import com.qnb.metadata.service.workflow.Workflowable;
import com.qnb.metadata.utils.DateUtil;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class User implements Workflowable{
	private final String userId;
	private String firstName;
	private String lastName;
	private final Date expiryDate;
	private final Date dob;
	private final String mobileNumber;
	private final String emailId;
	private final UserStatus userStatus;
	private final UserType userType;
	private final UserAuth userAuth;
	private final String makerComments;
	private Set<String> groups;
	private Set<GroupDef> groupDefs;
	private Set<String> units;
	private Set<UnitMaster> unitMaster;
	private final ActionType action;

	public static User fromRequest(String userId, String firstName, String lastName, String expiryDate, String dob,
			String mobileNumber, String emailId, String makerComments, Set<String> groups, Set<String> units,
			ActionType action, UserAuth userAuth) {
		return User.builder().userId(userId).firstName(firstName).lastName(lastName)
				.expiryDate(validateDate(expiryDate)).dob(dob != null ? DateUtil.getDate(dob) : null)
				.mobileNumber(mobileNumber).emailId(emailId).userStatus(UserStatus.ENABLED)
				.userType(UserType.FUNCTIONAL).makerComments(makerComments).groups(groups).units(units).action(action)
				.userAuth(userAuth).build();
	}

	public static User fromRequest(String userId, String expiryDate, Set<String> groups, Set<String> units,
			String makerComments, ActionType action) {
		return User.builder().userId(userId).expiryDate(validateDate(expiryDate)).groups(groups).units(units)
				.action(action).makerComments(makerComments).build();
	}

	public static User fromRequest(String userId, String status, String makerComments, ActionType action) {
		return User.builder().userId(userId).userStatus(UserStatus.parseUserStatus(status)).makerComments(makerComments)
				.action(action).build();
	}

	public void validate() {
		if (userStatus == null) {
			throw new ValidationException(ExceptionMessages.USER_STATUS_EXCEPTION.getErrorCode(),
					ExceptionMessages.USER_STATUS_EXCEPTION.getErrorDescription(), HttpStatus.BAD_REQUEST);
		}
	}

	private static Date validateDate(String expiryDate) {
		Date expDate = null;
		if (expiryDate != null) {
			expDate = DateUtil.getDate(expiryDate);
			if (expDate == null || expDate.before(Date.from(Instant.now()))) {
				throw new ValidationException(ExceptionMessages.DATE_NOT_VALID_EXCEPTION.getErrorCode(),
						ExceptionMessages.DATE_NOT_VALID_EXCEPTION.getErrorDescription(), HttpStatus.BAD_REQUEST);
			}
		}
		return expDate;
	}

	public User createNewUserDomainByUpdating(UserStatus status, ActionType action, String makerComments) {
		return User.builder().userId(userId).firstName(firstName).lastName(lastName).expiryDate(expiryDate).dob(dob)
				.mobileNumber(mobileNumber).emailId(emailId).userStatus(status).userType(userType)
				.makerComments(makerComments).groups(groups).groupDefs(groupDefs).units(units).unitMaster(unitMaster)
				.action(action).userAuth(userAuth).build();
	}

	public User createNewUserDomainByUpdating(Date expiryDate, Set<String> groups, Set<String> units, ActionType action,
			String makerComments) {
		return User.builder().userId(userId).firstName(firstName).lastName(lastName).expiryDate(expiryDate).dob(dob)
				.mobileNumber(mobileNumber).emailId(emailId).userStatus(userStatus).userType(userType)
				.makerComments(makerComments).groups(groups).groupDefs(groupDefs).units(units).unitMaster(unitMaster)
				.action(action).userAuth(userAuth).build();
	}

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIdempotentRequestDetails() {
		// TODO Auto-generated method stub
		return null;
	}
}
