package com.qnb.metadata.dto.user;

import lombok.Data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeleteUserStagedRequest implements Workflowable {

	private static final String SALT = "DU";

	private String unit;

	private String baseNum;

	private String customerName;

	private String globalId;

	private String loginIp;

	private String loginUserId;

	private String makerComments;

	private String requestId;

	private String status;

	private String unitId;

	private String userId;

	private String nationalId;

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + userId);
	}

	@Override
	public String getIdempotentRequestDetails() {
		return "Request : Delete user with userId :" + userId;
	}
}
