package com.qnb.metadata.dto.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.role.GroupDef;
import com.qnb.metadata.enums.UserStatus;
import com.qnb.metadata.enums.UserType;
import com.qnb.metadata.utils.DateUtil;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse {

	private String userId;
    private String firstName;
    private String lastName;
    private String expiryDate;
    private String dob;
    private String mobileNumber;
    private String emailId;
    private UserStatus userStatus;
    private UserType userType;
    private List<UserGroupDefResponse> groups;
    private List<UnitMasterResponse> units;
 
    public static UserResponse toResponse(User user){
        return UserResponse.builder()
            .userId(user.getUserId())
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .expiryDate(user.getExpiryDate() != null ? DateUtil.dateInStringFormat(user.getExpiryDate()) : null)
            .dob(user.getDob() != null ? DateUtil.dateInStringFormat(user.getDob()) : null)
            .mobileNumber(user.getMobileNumber())
            .emailId(user.getEmailId())
            .userStatus(user.getUserStatus())
            .userType(user.getUserType())
            .groups(getUserGroupDefResponse(user.getGroupDefs()))
            .units(getUnitMasterResponse(user.getUnitMaster()))
            .build();
    }
 
   private static List<UnitMasterResponse> getUnitMasterResponse(Set<UnitMaster> useUnits) {
		if(useUnits != null) {
			return useUnits.stream().map(UnitMasterResponse::unitMasterResponse).collect(Collectors.toList());
		}
		return new ArrayList<UnitMasterResponse>();
	}
 
	private static List<UserGroupDefResponse> getUserGroupDefResponse(Set<GroupDef> userGroup){
        if (userGroup != null) {
            return userGroup.stream().map(UserGroupDefResponse::userGroupDefResponse).collect(Collectors.toList());
        } 
        return new ArrayList<UserGroupDefResponse>();
    }
}
