package com.qnb.metadata.dto.user;

import java.sql.Date;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserAuth {
    private final String macId;
    private final int authSessionIdleTime;
    private final Date authLastPwdChangeTime;
    private final Date authLastLoginTime;
    private final String authLastSessionId;
    private final int authNumberOfWrongAttempt;
    private final int authLockStatus;
 
    public static UserAuth fromRequest(String macId) {
        return UserAuth.builder()
                .macId(macId)
                .build();
    }
}
