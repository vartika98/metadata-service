package com.qnb.metadata.dto.user;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.UnitMasterDto;
import com.qnb.metadata.dto.role.GroupDef;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.enums.ActionType;
import com.qnb.metadata.enums.ExceptionMessages;
import com.qnb.metadata.exception.ResourceNotFoundException;
import com.qnb.metadata.utils.DateUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRequestResponse {
    private String requestId;
    private String workflowId;
    private String workFlowStatus;
    private String userId;
    private String firstName;
    private String lastName;
    private String expiryDate;
    private String dob;
    private String mobileNumber;
    private String emailId;
    private String macId;
    private ActionType action;
    private String processStatus;
    private Set<String> availableActions;
    private String makerComments;
    private String checkerComments;
    private String createdBy;
    private Instant createdTime;
    private String lastModifiedBy;
    private Instant lastModifiedTime;
    private Set<UserGroupDefResponse> groups;
    private Set<UnitMasterDto> units;
    private List<WorkflowHistory> workflowHistory;
    
    
    public static UserRequestResponse userRequestResponse(Message message) {
        var userObject = message.getPayload();
        if (userObject instanceof User) {
           var user= (User) userObject;
           return UserRequestResponse.builder()
                   .requestId(message.getRequestId())
                   .workflowId(message.getWorkflowId())
                   .workFlowStatus(message.getWorkflowStatus().toString())
                   .userId(user.getUserId())
                   .firstName(user.getFirstName())
                   .lastName(user.getLastName())
                   .dob(user.getDob() != null ? DateUtil.dateInStringFormat(user.getDob()): null)
                   .mobileNumber(user.getMobileNumber())
                   .expiryDate(user.getExpiryDate() != null ? DateUtil.dateInStringFormat(user.getExpiryDate()) : null)
                   .emailId(user.getEmailId())
                   .macId(user.getUserAuth() != null ? user.getUserAuth().getMacId() : null)
                   .action(user.getAction())
                   .processStatus(message.getProcessStatus())
                   .availableActions(message.getAvailableActions())
                   .makerComments(message.getLatestMakerComment())
                   .checkerComments(message.getLatestCheckerComment())
                   .createdBy(message.getCreatedBy())
                   .createdTime(message.getCreatedTime())
                   .lastModifiedBy(message.getLastModifiedBy())
                   .lastModifiedTime(message.getLastModifiedTime())
                   .groups(groupDefResponse(user.getGroupDefs()))
                   .units(getUnitMasterResponse(user.getUnitMaster()))
                   .workflowHistory(message.getWorkflowHistory())
                   .build();
        }
        throw new ResourceNotFoundException(ExceptionMessages.WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION.getErrorCode(), ExceptionMessages.WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION.getErrorDescription(), HttpStatus.NOT_FOUND);
        
 
    }
    
    private static Set<UnitMasterDto> getUnitMasterResponse(Set<UnitMaster> unitMaster) {
		if(unitMaster != null) {
			return unitMaster.stream().map(UnitMasterDto::unitMasterResponse).collect(Collectors.toSet());
		}
		return new HashSet<UnitMasterDto>();
	}
 
	private static Set<UserGroupDefResponse> groupDefResponse(Set<GroupDef> groupDefs) {
		if(groupDefs != null) {
			return groupDefs.stream().map(UserGroupDefResponse::userGroupDefResponse).collect(Collectors.toSet());
		}
		return new HashSet<UserGroupDefResponse>();
	}
}