package com.qnb.metadata.dto.user;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeleteUserRequestResponse {

	private String requestId;
    private String workflowId;
    private String workFlowStatus;
    private String processStatus;
    private String makerComments;
    private String checkerComments;
    private Set<String> availableActions;
    private String createdBy;
    private Instant createdTime;
    private String lastModifiedBy;
    private Instant lastModifiedTime;
    private String userId;
    private String unitId;
    private DeleteUserStagedRequest deleteUserDetails;
    private List<WorkflowHistory> workflowHistory;
    private String unit;

   
    public static DeleteUserRequestResponse deleteUserRequestResponse(Message message) {
                    final Workflowable deleteUserObject = message.getPayload();
                    if (deleteUserObject instanceof DeleteUserStagedRequest) {
                                    var deleteUser = (DeleteUserStagedRequest) deleteUserObject;
                                    return DeleteUserRequestResponse.builder().requestId(message.getRequestId())
                                                                    .workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
                                                                    .processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
                                                                    .createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
                                                                    .lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
                                                                    .makerComments(message.getLatestMakerComment())
                                                                    .checkerComments(message.getLatestCheckerComment())
                                                                    .deleteUserDetails(deleteUser)
                                                                    .workflowHistory(message.getWorkflowHistory())
                                                                    .unit(message.getUnit())
                                                                    .build();
                    }
                    throw new ResourceNotFoundException("NA", "There is no delete user request object found",
                                                    HttpStatus.NOT_FOUND);
    }

}
