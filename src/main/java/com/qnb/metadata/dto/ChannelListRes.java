package com.qnb.metadata.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class ChannelListRes implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<ChannelDto> channels;
}
