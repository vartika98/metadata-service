package com.qnb.metadata.dto.b2b;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class B2BMaster implements Workflowable {
 
	private static final String SALT = "B2B";
	private String requestId;
	private String globalId;
	private String userId;
	private String requestComments;
	private String workflowId;
	private String unitId;
	private String status;
	private String refNo;
	private String debitAccountNo;
	private String requestedBy;
	private String requestedDate;
	private String rejectedBy;
	private String rejectedDate;
	private int rejectCount;
	private String checkerComments;
	private String benfName;
	private String benfBankName;
	private String benfAccountNo;
	private String creditAmount;
	private String debitAmount;
	private double sellRate;
	private byte[] userNo;
 
	public static B2BMaster fromRequest(String loginIp, String userId, String makerComments, String unit,
			String globalId, String refNo, String debitAccountNo, String requestedBy, String requestedDate,
			String status, String rejectedDate, String rejectedBy, int rejectCount, String checkerComments,
			String benfName, String benfBankName, String benfAccountNo, String creditAmount, String debitAmount,
			double sellRate,byte[] userNo) {
		return B2BMaster.builder().userId(userId).globalId(globalId).requestComments(makerComments).unitId(unit)
				.refNo(refNo).debitAccountNo(debitAccountNo).requestedBy(requestedBy).requestedDate(requestedDate)
				.rejectCount(rejectCount).rejectedBy(rejectedBy).rejectedDate(rejectedDate).status(status)
				.checkerComments(checkerComments).benfName(benfName).benfBankName(benfBankName)
				.creditAmount(creditAmount).debitAmount(debitAmount).sellRate(sellRate).userNo(userNo).build();
	}
 
	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + globalId + refNo);
	}
 
	@Override
	public String getIdempotentRequestDetails() {
 
		return "Request : B2B for user with userId :" + userId;
	}
 
}
