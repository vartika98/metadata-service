package com.qnb.metadata.dto.parameter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.metadata.enums.TxnStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class Parameter {
	@EqualsAndHashCode.Exclude
	private Long txnId;
	private String confKey;
	private String confValue;
	private TxnStatus status;
	private String remarks;
	private String action;
 
	public static Parameter toParameter(Parameter pm) {
		return Parameter.builder().confKey(pm.getConfKey()).confValue(pm.getConfValue()).build();
	}
 
	public static ParameterRequest toParameterRequest(Parameter req) {
		return ParameterRequest.builder()
				.txnId(req.getTxnId())
				.action(req.getAction())
				.confKey(req.getConfKey())
				.confValue(req.getConfValue())
				.status(req.getStatus())
				.remarks(req.getRemarks())
				.build();
	}
}
