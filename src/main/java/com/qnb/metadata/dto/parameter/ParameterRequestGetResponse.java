package com.qnb.metadata.dto.parameter;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ParameterRequestGetResponse {
	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private List<WorkflowHistory> workflowHistory; 
	private ParameterDetailsRequest parameterDetails;
	private String unit;
	public static ParameterRequestGetResponse toParameterRequestGetResponse(Message message) {
		var paramObject = message.getPayload();
		if (paramObject instanceof ParameterDetails) {
			var param = (ParameterDetails) paramObject;
			return ParameterRequestGetResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.makerComments(message.getLatestMakerComment())
					.checkerComments(message.getLatestCheckerComment())
					.workflowHistory(message.getWorkflowHistory())
					.parameterDetails(ParameterDetails.toParameterDetailsRequest(param))
					.unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException("NA", "There is no parameter request object found",
				HttpStatus.NOT_FOUND);
	}

 
}
