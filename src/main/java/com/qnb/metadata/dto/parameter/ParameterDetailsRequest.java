package com.qnb.metadata.dto.parameter;

import java.util.List;
import java.util.stream.Collectors;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParameterDetailsRequest {
	@NotBlank(message = "{unitId.notEmpty}")
	@Size(max = 3, message = "{unitId.exceedLimit}")
	private String unitId;
	@NotBlank(message = "{channelId.notEmpty}")
	@Size(max = 3, message = "{channelId.exceedLimit}")
	private String channelId;
	@Valid
	@NotEmpty(message = "{paramDetails.notEmpty}")
	private List<ParameterRequest> paramDetails;
	@Size(max = 100, message = "{makerComments.exceedLimit}")
	private String makerComments;
	public static ParameterDetails normalize(ParameterDetailsRequest req) {
		List<Parameter> paramList = req.getParamDetails().stream().map(ParameterRequest::normalize).collect(Collectors.toList());
		return ParameterDetails.builder()
			.channelId(req.getChannelId())
			.unitId(req.getUnitId())
			.paramDetails(paramList)
			.makerComments(req.getMakerComments())
			.build();
}
 
}
