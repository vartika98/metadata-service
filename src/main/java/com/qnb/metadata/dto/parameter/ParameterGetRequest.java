package com.qnb.metadata.dto.parameter;

import java.util.List;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ParameterGetRequest {
	@NotEmpty(message = "{requestId.notEmpty}")
    private List<String> requestIds;
 
}
