package com.qnb.metadata.dto.parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ParameterDetails implements Workflowable{
 
	private static final String SALT = "parameter:";
	private String unitId;
	private String channelId;
    private List<Parameter> paramDetails;
    private String makerComments;
	@Override
	public List<String> getIdempotentRequestIdentifier() {
		List<String> paramList = new ArrayList<String>();
		for (Parameter param: paramDetails)
		{
			paramList.add( SALT + unitId + channelId+param.getConfKey());
		}
		return paramList;
	}
	public static ParameterDetailsRequest toParameterDetailsRequest(ParameterDetails req) {
		List<ParameterRequest> paramList = req.getParamDetails().stream().map(Parameter::toParameterRequest).collect(Collectors.toList());
		return ParameterDetailsRequest.builder()
			.channelId(req.getChannelId())
			.unitId(req.getUnitId())
			.paramDetails(paramList)
			.makerComments(req.getMakerComments())
			.build();
}
 
	@Override
	public String getIdempotentRequestDetails() {
		return String.format("Request : Parameter Maintenance , UnitId : %s, ChannelId : %s ",unitId,channelId);
	}
 
}
