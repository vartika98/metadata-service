package com.qnb.metadata.dto.parameter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.metadata.enums.TxnStatus;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(Include.NON_NULL)
public class ParameterRequest {
	private Long txnId;
	private String action;
 
	@NotBlank(message = "{confKey.notEmpty}")
	@Size(max = 200, message = "{confKey.exceedLimit}")
	private String confKey;
	@NotBlank(message = "{confValue.notEmpty}")
	@Size(max = 2700, message = "{confValue.exceedLimit}")
	private String confValue;
	private TxnStatus status;
	@Size(max = 1000, message = "{remarks.exceedLimit}")
	private String remarks;
	public static Parameter normalize(ParameterRequest req) {
		return Parameter.builder()
				.txnId(req.getTxnId())
				.confKey(req.getConfKey())
				.confValue(req.getConfValue())
				.status(req.getStatus())
				.remarks(req.getRemarks())
				.build();
	}
}
