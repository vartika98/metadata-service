package com.qnb.metadata.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonIgnoreProperties({ "unit", "channel", "language" })
public class TranslationDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private Integer id;

	private ChannelDto channel;

	private LanguageDto language;

	private UnitDto unit;
	@NotBlank
	private String channelId;
	@NotBlank
	private String languageId;
	@NotBlank
	private String unitId;
	@NotBlank
	private String uniqueId;
	@NotBlank
	private String page;

	@NotBlank
	private String group;

	@NotBlank
	private String key;

	@NotBlank
	private String value;

	@NotBlank
	private String description;

}
