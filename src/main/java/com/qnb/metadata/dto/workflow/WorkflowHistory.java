package com.qnb.metadata.dto.workflow;

import java.time.Instant;

import com.qnb.metadata.enums.RemarkType;
import com.qnb.metadata.enums.WorkflowStatus;

import lombok.Getter;

@Getter
public class WorkflowHistory {

	private final String workflowHistoryId;

	private final String workflowId;

	private String requestId;

	private String processId;

	private Integer seqNumber;

	private final WorkflowStatus workflowStatus;

	private final String taskId;

	private final String remarks;

	private RemarkType remarkType;

	private final String processStatus;

	private final String createdBy;

	private final Instant createdTime;

	public WorkflowHistory(String workflowHistoryId, String workflowId, String requestId, String processId,
			Integer seqNo, WorkflowStatus workflowStatus, String taskId, String remarks, RemarkType remarkType,
			String performedAction, String createdBy, Instant createdTime) {
		this.workflowHistoryId = workflowHistoryId;
		this.workflowId = workflowId;
		this.requestId = requestId;
		this.processId = processId;
		this.seqNumber = seqNo;
		this.workflowStatus = workflowStatus;
		this.taskId = taskId;
		this.remarks = remarks;
		this.remarkType = remarkType;
		this.processStatus = performedAction;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
	}
}
