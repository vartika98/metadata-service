package com.qnb.metadata.dto.workflow;

import java.util.List;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAllReqDetailsRequest {
	@NotEmpty(message = "{requestId.notEmpty}")
	private List<String> requestIds;

}
