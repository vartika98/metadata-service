package com.qnb.metadata.dto.workflow;

import java.time.Instant;
import java.util.Set;

import com.qnb.metadata.enums.WorkflowStatus;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Workflow {
	 private String workflowId;
	 
     private String requestId;

     private String processId;

     private String taskId;

     private String assignTo;

     private String initiatedGroup;

     private String workflowGroup;

     private String requestHash;

     private Set<String> availableActions;

     private WorkflowStatus status;

     private String processStatus;

     private Instant scheduledTime;

     private Integer historyCount;

     private String createdBy;

     private Instant createdTime;

     private String lastModifiedBy;

     private Instant lastModifiedTime;
    
     private Object payLoadList;
    
     private String unit;

     public void addOneToHistoryCount() {
                     if (historyCount == null) {
                                     historyCount = 0;
                     }
                     historyCount += 1;
     }

//     public WorkflowResponse toRestResponse() {
//                     return WorkflowResponse.builder().workflowId(workflowId).requestId(requestId).processId(processId)
//                                                     .taskId(taskId).assignTo(assignTo).initiatedGroup(initiatedGroup).workflowGroup(workflowGroup)
//                                                     .availableActions(availableActions).status(status).processStatus(processStatus)
//                                                     .historyCount(historyCount).scheduledTime(scheduledTime).createdBy(createdBy).createdTime(createdTime)
//                                               .lastModifiedBy(lastModifiedBy).lastModifiedTime(lastModifiedTime).payLoadList(payLoadList).unit(unit).build();
//     }
}

