package com.qnb.metadata.dto.workflow;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import com.qnb.metadata.enums.RemarkType;
import com.qnb.metadata.enums.WorkflowStatus;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Message {

	private String processId;
	private String workflowId;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private String requestId;
	private String taskId;
	private WorkflowStatus workflowStatus;
	private String processStatus;
	private Set<String> availableActions;
	private boolean ended;
	private Workflowable payload;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public String getLatestMakerComment() {
		return getLatestComment(RemarkType.MAKER);
	}

	public String getLatestCheckerComment() {
		return getLatestComment(RemarkType.CHECKER);
	}

	private String getLatestComment(RemarkType remarkType) {
		if (workflowHistory != null && !workflowHistory.isEmpty()) {
			WorkflowHistory history = null;
			for (var rec : workflowHistory) {
				if ((rec.getRemarkType() == remarkType)
						&& (history == null || (history.getSeqNumber() < rec.getSeqNumber()))) {
					history = rec;
				}
			}
			if (history != null) {
				return history.getRemarks();
			}
		}
		return "";
	}
}
