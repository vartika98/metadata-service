package com.qnb.metadata.dto.ipo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class IPOStagedReq implements Workflowable {
	private static final String SALT = "CCP:";
	private String requestId;
	private String workflowId;
	private String loginIp;
	private String userId;
	private String primaryNationalId;
	private String title;
	private String nationalId;
	private String firstName;
	private String secondName;
	private String thirdName;
	private String fourthName;
	private String familyName;
	private String nationalityCode;
	private String unitId;
	private String dateOfBirth;
	private String gender;
	private String genderDesc;
	private String residentCountry;
	private String residentCity;
	private String postBoxNo;
	private String email;
	private String mobileNo;
	private String residentPhoneNo;
	private String officePhNo;
	private String applicantType;
	private String guardianName;
	private String guardianidNo;
	private String moiChrefNum;
	private String nationalityExpDate;
	private String loanRequired;
	private String loanRequiredDesc;
	private String residentCountryDesc;
	private String residentCityDesc;
	private String requestComments;
	private String relationType;

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + primaryNationalId + dateOfBirth);
	}

	@Override
	public String getIdempotentRequestDetails() {
		return "Request : IPO Registration with userId :" + userId;
	}
}
