package com.qnb.metadata.dto.ipo;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IPORequestResponse {

	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTme;
	private IPOStagedReqMaster ipoAdminDetails;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public static IPORequestResponse ipoAdminRequestGetResponse(Message message) {
		var ipoAdminObject = message.getPayload();
		if (ipoAdminObject instanceof IPOStagedReq) {
			var ipoAdmin = (IPOStagedReq) ipoAdminObject;
			return IPORequestResponse.builder().requestId(message.getRequestId()).workflowId(message.getWorkflowId())
					.workFlowStatus(message.getWorkflowStatus().toString()).processStatus(message.getProcessStatus())
					.availableActions(message.getAvailableActions()).createdBy(message.getCreatedBy())
					.createdTime(message.getCreatedTime()).lastModifiedBy(message.getLastModifiedBy())
					.lastModifiedTme(message.getLastModifiedTime()).makerComments(message.getLatestMakerComment())
					.checkerComments(message.getLatestCheckerComment()).ipoAdminDetails(toModifyIpoSatgedStagedRequest(ipoAdmin))
					.workflowHistory(message.getWorkflowHistory()).unit(message.getUnit()).build();
		}
		throw new ResourceNotFoundException("NA", "There is no credit card request object found", HttpStatus.NOT_FOUND);

	}
	
	public static IPOStagedReqMaster toModifyIpoSatgedStagedRequest(IPOStagedReq paymentReq) {
		return  IPOStagedReqMaster.builder()
				.requestId(paymentReq.getRequestId())
				.workflowId(paymentReq.getWorkflowId())
				.loginIp(paymentReq.getLoginIp())
				.userId(paymentReq.getUserId())
				.primaryNationalId(paymentReq.getPrimaryNationalId())
				.title(paymentReq.getTitle())
				.nationalId(paymentReq.getNationalId())
				.firstName(paymentReq.getFirstName())
				.secondName(paymentReq.getSecondName())
				.thirdName(paymentReq.getThirdName())
				.fourthName(paymentReq.getFourthName())
				.familyName(paymentReq.getFamilyName())
				.nationalityCode(paymentReq.getNationalityCode())
				.unitId(paymentReq.getUnitId())
				.dateOfBirth(paymentReq.getDateOfBirth())
				.gender(paymentReq.getGender())
				.genderDesc(paymentReq.getGender().equalsIgnoreCase("M")?"Male":"Female")
				.residentCountry(paymentReq.getResidentCountry())
				.residentCountryDesc(paymentReq.getResidentCountryDesc())
				.residentCity(paymentReq.getResidentCity())
				.residentCityDesc(paymentReq.getResidentCityDesc())
				.postBoxNo(paymentReq.getPostBoxNo())
				.email(paymentReq.getEmail())
				.mobileNo(paymentReq.getMobileNo())
				.residentPhoneNo(paymentReq.getResidentPhoneNo())
				.officePhNo(paymentReq.getOfficePhNo())
				.applicantType(paymentReq.getApplicantType())
				.guardianName(paymentReq.getGuardianName())
				.guardianidNo(paymentReq.getGuardianidNo())
				.moiChrefNum(paymentReq.getMoiChrefNum())
				.loanRequired(paymentReq.getLoanRequired())
				.loanRequiredDesc(paymentReq.getLoanRequired().equalsIgnoreCase("N")?"No":"Yes")
				.requestComments(paymentReq.getRequestComments())
				.nationalityExpDate(paymentReq.getNationalityExpDate())
				.build();

	}
}
