package com.qnb.metadata.dto;

import java.io.Serializable;

import java.util.List;

import lombok.Data;

import lombok.EqualsAndHashCode;

import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MenuDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String id;

	private String title;

	private String description;

	private Integer priority;

	private String menuType;

	private String pageId;

	private String link;

	private List<MenuDto> subMenus;

}
