package com.qnb.metadata.dto;

import java.io.Serializable;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import lombok.EqualsAndHashCode;

import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class PageConfigDto implements Serializable {

	private static final long serialVersionUID = 3548997014390137250L;

	private Integer id;

	@NotBlank
	private String unitId;

	@NotBlank
	private String channelId;

	@NotBlank
	private String page;

	@NotBlank
	private String key;

	@NotBlank
	private String value;

	@NotBlank
	private String description;

}
