package com.qnb.metadata.dto;

import java.io.Serializable;

import lombok.Data;

import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ScreenEntDTO implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String widget;

	private String enabled;

}
