package com.qnb.metadata.dto.label;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.channel.ChannelStagedRequest;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LabelRequestResponse {
	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private List<WorkflowHistory> workflowHistory;
	private Label label;
	private String unit;

	public static LabelRequestResponse toLabelRequestResponse(Message message) {
		var txnObject = message.getPayload();
		if (txnObject instanceof Label) {
			var txn = (Label) txnObject;
			return LabelRequestResponse.builder().requestId(message.getRequestId()).workflowId(message.getWorkflowId())
					.workFlowStatus(message.getWorkflowStatus().toString()).processStatus(message.getProcessStatus())
					.availableActions(message.getAvailableActions()).createdBy(message.getCreatedBy())
					.createdTime(message.getCreatedTime()).lastModifiedBy(message.getLastModifiedBy())
					.lastModifiedTime(message.getLastModifiedTime()).makerComments(message.getLatestMakerComment())
					.checkerComments(message.getLatestCheckerComment()).workflowHistory(message.getWorkflowHistory())
					.label(toLabelReq(txn)).unit(message.getUnit()).build();
		}
		throw new ResourceNotFoundException("NA", "There is no txn request entitlement object found",
				HttpStatus.NOT_FOUND);
	}
	
	public static Label toLabelReq(Label req) {
		return Label.builder().unitId(req.getUnitId()).channelId(req.getChannelId()).screenId(req.getScreenId())
				.labelDetails(req.getLabelDetails()).menuId(req.getMenuId()).makerComment(req.getMakerComment()).build();

	}
}
