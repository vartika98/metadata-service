package com.qnb.metadata.dto.label;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.LabelDescription;
import com.qnb.metadata.enums.TxnStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LabelDetail {

	private Long txnId;
	 
    private String menuId;

    private String transGroup;
   
    private String action;
   
    private String transKey;
   
    private List<LabelDescription> labelValues;
   
    private TxnStatus status;
   
    private String description;
}
