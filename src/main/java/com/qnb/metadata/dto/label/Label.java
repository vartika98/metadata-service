package com.qnb.metadata.dto.label;

import java.util.List;
import java.util.stream.Collectors;

import com.qnb.metadata.dto.LabelDetails;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Label implements Workflowable {
	private static final String SALT = "label";
	List<LabelDetail> labelDetails;
	private String makerComment;
	private String channelId;
	private String screenId;
	private String unitId;
	private String menuId;

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + unitId + channelId + menuId + screenId);
	}

	@Override
	public String getIdempotentRequestDetails() {
		return String.format("Request : Label Maintenance , UnitId : %s > ChannelId : %s > Menuid :%s > ScreenId : %s ",
				unitId, channelId, menuId, screenId);
	}

	

}
