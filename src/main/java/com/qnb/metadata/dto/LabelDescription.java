package com.qnb.metadata.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.metadata.entity.Translation;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@Builder
public class LabelDescription {
	
	private String langCode;

	private String langValue;
	
	
	public static LabelDescription toLabelDescription(Translation lb) {
		return LabelDescription.builder().langCode(lb.getLangCode()).langValue(lb.getValue()).build();
	}	

}

