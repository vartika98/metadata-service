package com.qnb.metadata.dto;

import java.io.Serializable;

import lombok.Data;

import lombok.EqualsAndHashCode;

import lombok.NoArgsConstructor;

@Data

@EqualsAndHashCode(callSuper = false)

@NoArgsConstructor

public class LanguageDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String id;

	private String title;

	private String description;

}
