package com.qnb.metadata.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class ManageMenuReqDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String unitId;
	private String channelId;
	private List<MenusDto> txnDetails;

}
