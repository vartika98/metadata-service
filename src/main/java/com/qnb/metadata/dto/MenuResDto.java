package com.qnb.metadata.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MenuResDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String menuId;

	private String description;

}
