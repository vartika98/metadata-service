package com.qnb.metadata.dto.errorConfig;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorConfigStagedReqMaster {

	private int txnId;

	private String unitId;

	private String channelId;

	private String makerComments;

	private String createdBy;

	private Date dateCreated;

	private String modifiedBy;

	private Date dateModified;

	private String serviceType;

	private List<ErrorParameterRequest> paramDetails;
}
