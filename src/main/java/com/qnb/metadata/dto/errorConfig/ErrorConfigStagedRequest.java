package com.qnb.metadata.dto.errorConfig;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorConfigStagedRequest implements Workflowable {

	private static final String SALT = "ERC:";

	private int txnId;

	private String unitId;

	private String channelId;

	private String makerComments;

	private String createdBy;

	private Date dateCreated;

	private String modifiedBy;

	private Date dateModified;

	private String serviceType;

	private List<ErrorParameterRequest> paramDetails;

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + unitId + channelId + serviceType + createdBy);
	}

	@Override
	public String getIdempotentRequestDetails() {
		return "Request : Error Configuration for user with serviceType :" + serviceType;
	}
}
