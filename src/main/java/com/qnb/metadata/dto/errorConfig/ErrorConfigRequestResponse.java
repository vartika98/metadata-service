package com.qnb.metadata.dto.errorConfig;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorConfigRequestResponse {

	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private ErrorConfigStagedReqMaster errorDetails;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public static ErrorConfigRequestResponse errorConfigRequestGetResponse(Message message) {
		var errorConfigObject = message.getPayload();
		if (errorConfigObject instanceof ErrorConfigStagedRequest) {
			var errorConfig = (ErrorConfigStagedRequest) errorConfigObject;
			return ErrorConfigRequestResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.makerComments(message.getLatestMakerComment()).checkerComments(message.getLatestCheckerComment())
					.errorDetails(toModifyErrorConfigStagedRequest(errorConfig)).workflowHistory(message.getWorkflowHistory()).unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException("NA", "There is no credit card request object found", HttpStatus.NOT_FOUND);

	}

	public static ErrorConfigStagedReqMaster toModifyErrorConfigStagedRequest(ErrorConfigStagedRequest request) {
		return ErrorConfigStagedReqMaster.builder()
				.unitId(request.getUnitId())
				.channelId(request.getChannelId())
				.serviceType(request.getServiceType())
				.makerComments(request.getMakerComments())
				.createdBy(request.getCreatedBy())
				.dateCreated(request.getDateCreated())
				.modifiedBy(request.getModifiedBy())
				.dateModified(request.getDateModified())
				.paramDetails(request.getParamDetails())
				.build();

	}
}
