package com.qnb.metadata.dto.errorConfig;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.enums.TxnStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorParameterRequest {

	private int txnId;
    
    private String action;

    private String lagCode;
   
    private String serviceType;
   
    private String mwErrCode;
   
    private String ocsErrCode;
                   
    private String errDescEng;
   
    private String errDescArb;
   
    private String errDescFr;
   
    private TxnStatus status;
   
    private String remarks;
   
    private int erConfigId;
    
}
