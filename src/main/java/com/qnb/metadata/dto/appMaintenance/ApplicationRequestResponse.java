package com.qnb.metadata.dto.appMaintenance;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.enums.RequestActionType;
import com.qnb.metadata.exception.ResourceNotFoundException;

import static com.qnb.metadata.enums.ExceptionMessages.WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApplicationRequestResponse {
	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private Long txnId;
	private String appName;
	private String appType;
	private String status;
	private RequestActionType action;
	private Set<String> units;
	private Set<ApplicationChannelResponse> channels;
	private String processStatus;
	private Set<String> availableActions;
	private String makerComments;
	private String checkerComments;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private List<WorkflowHistory> workflowHistory;
	private String unit;
 
	public static ApplicationRequestResponse applicationRequestResponse(Message message) {
		var appObject = message.getPayload();
		if (appObject instanceof Application) {
			var application = (Application) appObject;
			var units = new HashSet<String>();
			if(application.getApplicationDetails() != null) {
				for(var appDetail : application.getApplicationDetails()) {
					units.add(appDetail.getUnit());
				}
			}
	
			Set<ApplicationChannelResponse> channels = application.getApplicationDetails().stream()
					.map(appdtls -> ApplicationChannelResponse.toApplicationChannelResponse(appdtls.getChannel())).collect(Collectors.toSet());
			
			return ApplicationRequestResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.txnId(application.getTxnId()).appName(application.getAppName())
					.appType(application.getAppType().toString()).status(application.getStatus().toString())
					.action(application.getAction())
					.units(units)
					.channels(channels)
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.makerComments(message.getLatestMakerComment()).checkerComments(message.getLatestCheckerComment())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.workflowHistory(message.getWorkflowHistory())
					.unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException(WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION.getErrorCode(),
				WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION.getErrorDescription(), HttpStatus.NOT_FOUND);
	}
 
}