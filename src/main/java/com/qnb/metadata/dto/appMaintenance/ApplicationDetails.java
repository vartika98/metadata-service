package com.qnb.metadata.dto.appMaintenance;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
@EqualsAndHashCode
@Builder
public class ApplicationDetails {
	private final Long txnId;
	private final Long appId;
	private final String unit;
	private final ApplicationChannel channel;

	public static ApplicationDetails fromRequest(String unit, String channelId) {
		return ApplicationDetails.builder()
				.unit(unit)
				.channel(ApplicationChannel.toApplicationChannel(channelId, null))
				.build();
	}
 
	public static ApplicationDetails fromRequest(String unit, ApplicationChannel channel) {
		return ApplicationDetails.builder()
				.unit(unit)
				.channel(channel)
				.build();
	}
}
