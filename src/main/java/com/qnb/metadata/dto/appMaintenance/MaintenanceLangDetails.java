package com.qnb.metadata.dto.appMaintenance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MaintenanceLangDetails {
 
	private String lang;
	
	private String reason;
	
	public static MaintenanceLangDetails toMessageDetail(MessageDetails details) {
		return MaintenanceLangDetails.builder()
				.lang(details.getLang())
				.reason(details.getReason())
				.build();
	}
	
	public static MaintenanceLangDetails toMessageDetail(String lang, String reason) {
		return MaintenanceLangDetails.builder()
				.lang(lang)
				.reason(reason)
				.build();
	}
}
