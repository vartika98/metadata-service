package com.qnb.metadata.dto.appMaintenance;

import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageDetails {
 
	@Size(max = 2, message = "{langCode.exceedLimit}")
	private String lang;
	@Size(max = 300, message = "{reason.exceedLimit}")
	private String reason;
	public static MessageDetails toResponse(MaintenanceLangDetails details) {
		return MessageDetails.builder()
				.lang(details.getLang())
				.reason(details.getReason())
				.build();
	}
	public static MaintenanceLangDetails toMessageDetail(MessageDetails details) {
		return MaintenanceLangDetails.builder()
				.lang(details.getLang())
				.reason(details.getReason())
				.build();
	}
}
