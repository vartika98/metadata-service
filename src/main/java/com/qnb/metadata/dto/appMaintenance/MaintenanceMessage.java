package com.qnb.metadata.dto.appMaintenance;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.qnb.metadata.enums.TxnStatus;
import com.qnb.metadata.service.workflow.Workflowable;
import com.qnb.metadata.utils.DateUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MaintenanceMessage implements Workflowable{
	
	private static final String SALT = "PlannedUnplanned";
	
	private List<MaintenanceAppDetail> applicationDetails;
	private List<MaintenanceLangDetails>  messageDetails;
	private String makerComments;
	private String messageType;
	private String purposeOfMsg;
	private Long refNo;
	private TxnStatus status;
	private String action;
	private Set<String> units;
	private Set<String> applications;
	
	@Override
	public List<String> getIdempotentRequestIdentifier() {
		
		if ("U".equalsIgnoreCase(messageType)) {
			return List.of("UnplannedMaintance");
		}
 
		if (applicationDetails != null && !applicationDetails.isEmpty()) {
			
			return applicationDetails.stream()
					.map(u-> SALT+ u.getUnitId()
						+ u.getUnitId()
						+ u.getAppId()
						+ DateUtil.dateInStringFormat(u.getOutageStartDateTime())
						+ DateUtil.dateInStringFormat(u.getOutageEndDateTime()))							
					.collect(Collectors.toList());
		}
		return List.of(""+Calendar.getInstance().getTimeInMillis());
	}
 
	@Override
	public String getIdempotentRequestDetails() {
		StringBuilder msg = new StringBuilder();
		if ("U".equalsIgnoreCase(messageType)) {
			msg.append("Request : UnplannedMaintance");
		} else {
			msg.append("Request : Planned Maintenance -> ");
			boolean isFirstRec=true;
			for (MaintenanceAppDetail dtl : applicationDetails) {
				if(isFirstRec) {
					isFirstRec=false;
				}
				else {
					msg.append(",");
				}
				msg.append("UnitID  :")
				.append( dtl.getUnitId())
				.append( "AppId :")
				.append( dtl.getAppId())
				.append("| Application :")
				.append(dtl.getApplication())
				.append("| Start DateTime :")
				.append( DateUtil.dateInStringFormat(dtl.getOutageStartDateTime()))
				.append("| End DateTime :")
				.append(DateUtil.dateInStringFormat(dtl.getOutageEndDateTime()));
 
			}
		}
 
		return msg.toString();
	}
	
	public Set<String> getUnitSet(){
		return applicationDetails.stream().map(MaintenanceAppDetail::getUnitId)
				.collect(Collectors.toSet());
	}
	
	public Set<Long> getAppIdSet(){
		return  applicationDetails.stream().map(MaintenanceAppDetail::getAppId)
				.collect(Collectors.toSet());
	}
	
	public Date getStartDate() {
		return applicationDetails.stream().map(MaintenanceAppDetail::getOutageStartDateTime)
		.min(Date::compareTo).orElse(null);
	}
	
	public Date getEndDate() {
		return applicationDetails.stream().map(MaintenanceAppDetail::getOutageEndDateTime).max(Date::compareTo)
				.orElse(null);
	}
	
	private boolean validateDateRange() {
		List<MaintenanceAppDetail> details = applicationDetails.stream()
				.filter(dtls -> (dtls.getOutageEndDateTime() != null && dtls.getOutageStartDateTime() != null
						&& dtls.getOutageEndDateTime().before(dtls.getOutageStartDateTime())))
				.collect(Collectors.toList());
		return details.isEmpty();
	}
	
	private boolean hasInvalidApplicationDetails() {
		return applicationDetails.stream()
				.anyMatch(dtls -> dtls.getOutageEndDateTime() == null || dtls.getOutageStartDateTime() == null
						|| dtls.getUnitId() == null || dtls.getChannelIds() == null
						|| dtls.getAppId() == null
						|| dtls.getApplication() == null
						|| (dtls.getOutageEndDateTime() == null && dtls.getOutageStartDateTime() == null
						&& dtls.getOutageEndDateTime().before(dtls.getOutageStartDateTime())));
	}
	
	public void validateMaintenanceMessage() throws Exception{
		if (applicationDetails.isEmpty() || hasInvalidApplicationDetails()) {
			throw new Exception("Planned outage details cannot not be empty");
		}
		
		if(!validateDateRange()) {
			throw new Exception("Start date cannot be higher than end date");
		}
		
		if(getStartDate().before(new Date()) || getEndDate().before(new Date())) {
			throw new Exception("Date should be the upcoming date");
		}
	}
	
	public boolean isValidMessageType() {
		if (!messageType.equals("U") && !messageType.equals("P")){
			return false;
		}
		return true;
	}
 
}
