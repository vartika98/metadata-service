package com.qnb.metadata.dto.appMaintenance;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
@EqualsAndHashCode
@Builder
public class ApplicationChannelResponse {
	private String channelId;
	private String channelDesc;

	public static ApplicationChannelResponse toApplicationChannelResponse(ApplicationChannel appChannel) {
		return ApplicationChannelResponse.builder()
				.channelId(appChannel.getChannelId())
				.channelDesc(appChannel.getChannelDesc())
				.build();
		}
 
}
