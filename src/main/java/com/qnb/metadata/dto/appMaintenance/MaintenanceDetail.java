package com.qnb.metadata.dto.appMaintenance;

import java.util.Date;

import com.qnb.metadata.enums.TxnStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MaintenanceDetail {
	
	private String refNo;
	
	private Long appId;
	
	private String application;
	
	private String unitId;
	
	private String channelIds;
		
	private Date outageStartDateTime;
	
	private Date outageEndDateTime;
	
	private TxnStatus status;
	
	private String action;
 
}
