package com.qnb.metadata.dto.appMaintenance;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
 
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.qnb.metadata.enums.TxnStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(Include.NON_NULL)
public class MaintainanaceDetailResponse {
 
	private Long refNo;
 
	private TxnStatus status;
 
	private String purposeOfMsg;
 
	private String messageType;
 
	private String action;
 
	private String makerComments;
	private Set<String> units;
 
	private Set<String> applications;
	private List<MaintenanceDetails> applicationDetails;
 
	private List<MessageDetails> messageDetails;
 
	public static MaintainanaceDetailResponse toResponse(MaintenanceMessage res) {
		List<MaintenanceDetails> maintenanceDetailList = res.getApplicationDetails() != null
				? res.getApplicationDetails().stream().map(MaintenanceDetails::toResponse).collect(Collectors.toList())
				: null;
		List<MessageDetails> msgDetails = res.getMessageDetails() != null
				? res.getMessageDetails().stream().map(MessageDetails::toResponse).collect(Collectors.toList())
				: null;
		return MaintainanaceDetailResponse.builder().makerComments(res.getMakerComments())
				.applicationDetails(maintenanceDetailList).messageDetails(msgDetails).messageType(res.getMessageType())
				.purposeOfMsg(res.getPurposeOfMsg()).action(res.getAction()).refNo(res.getRefNo())
				.units(res.getUnits())
				.applications(res.getApplications())
				.status(res.getStatus()).build();
	}
}
