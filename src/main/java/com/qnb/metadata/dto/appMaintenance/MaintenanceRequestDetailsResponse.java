package com.qnb.metadata.dto.appMaintenance;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.enums.ExceptionMessages;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MaintenanceRequestDetailsResponse {
	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	MaintainanaceDetailResponse maintenanceDetails;
	private String processStatus;
	private Set<String> availableActions;
	private String makerComments;
	private String checkerComments;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private List<WorkflowHistory> workflowHistory;
	private String unit;
	
	public static MaintenanceRequestDetailsResponse toMaintenanceRequestDetailsResponse(Message message) {
		var maintenance = message.getPayload();
		if (maintenance instanceof MaintenanceMessage) {
			var plannedUnplanned = (MaintenanceMessage) maintenance;
			return MaintenanceRequestDetailsResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.maintenanceDetails(MaintainanaceDetailResponse.toResponse(plannedUnplanned))
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.makerComments(message.getLatestMakerComment()).checkerComments(message.getLatestCheckerComment())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.workflowHistory(message.getWorkflowHistory())
					.unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException(ExceptionMessages.WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION.getErrorCode(),
				ExceptionMessages.WORKFLOW_REQUEST_NOT_FOUND_EXCEPTION.getErrorDescription(), HttpStatus.NOT_FOUND);
	}
}
