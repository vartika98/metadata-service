package com.qnb.metadata.dto.appMaintenance;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@Builder
public class ApplicationChannel {
	private String channelId;
	private String channelDesc;
	
	
	public static ApplicationChannel toApplicationChannel(String channelId, String channelDesc) {
		return ApplicationChannel.builder()
				.channelId(channelId)
				.channelDesc(channelDesc)
				.build();
		}
 
}
