package com.qnb.metadata.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonIgnoreProperties({ "status", "createdBy", "createdTime", "modifiedBy", "modifiedTime" })
public class UnitDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private String id;

	private String description;

	private String color;

	private String title;

	private String baseCur;

	private String cur2;

	private String cur3;

	private String country;

	private String timeZone;
	private String iban;
	private String branchCode;
	private String smsUnitId;

}
