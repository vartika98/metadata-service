package com.qnb.metadata.dto.creditCard;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.service.workflow.Workflowable;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CCPaymentStagedRequest implements Workflowable{

	private static final String SALT = "CCP:";
	
	private String customerName;

	private String globalId;

	private String loginIp;

	private String accountNumber;

	private String pointBalance;

	private String cardNumber;

	private String mobileNumber;

	private String loginUserId;

	private String paymentAmount;

	private String redeemPoints;

	private String reason;

	private String makerComments;

	private String requestId;

	private String status;

	private String type;

	private String unitId;

	private String userId;

	private String nationalId;

	private String accountType;

	private Long txnId;

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + globalId+cardNumber);
	}

	@Override
	public String getIdempotentRequestDetails() {
		return "Request : Credit Card Payment for user with userId :"+userId;
	}

	
}
