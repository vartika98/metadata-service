package com.qnb.metadata.dto.creditCard;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.channel.ChannelStagedRequest;
import com.qnb.metadata.dto.channel.ChannelStagedRequestDto;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CCRequestResponse {
	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private CCPaymentStagedRequestDto ccPaymentDetails;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public static CCRequestResponse ccPaymentRequestGetResponse(Message message) {
		var ccPaymentObject = message.getPayload();
		if (ccPaymentObject instanceof CCPaymentStagedRequest) {
			var cardPay = (CCPaymentStagedRequest) ccPaymentObject;
			return CCRequestResponse.builder().requestId(message.getRequestId()).workflowId(message.getWorkflowId())
					.workFlowStatus(message.getWorkflowStatus().toString()).processStatus(message.getProcessStatus())
					.availableActions(message.getAvailableActions()).createdBy(message.getCreatedBy())
					.createdTime(message.getCreatedTime()).lastModifiedBy(message.getLastModifiedBy())
					.lastModifiedTime(message.getLastModifiedTime()).makerComments(message.getLatestMakerComment())
					.checkerComments(message.getLatestCheckerComment())
					.ccPaymentDetails(toModifyCCPaymentStagedRequest(cardPay))
					.workflowHistory(message.getWorkflowHistory()).unit(message.getUnit()).build();
		}
		throw new ResourceNotFoundException("NA", "There is no credit card request object found", HttpStatus.NOT_FOUND);

	}

	public static CCPaymentStagedRequestDto toModifyCCPaymentStagedRequest(CCPaymentStagedRequest request) {
		return CCPaymentStagedRequestDto.builder().customerName(request.getCustomerName())
				.globalId(request.getGlobalId()).loginIp(request.getLoginIp()).accountNumber(request.getAccountNumber())
				.pointBalance(request.getPointBalance()).cardNumber(request.getCardNumber())
				.mobileNumber(request.getMobileNumber()).paymentAmount(request.getPaymentAmount())
				.redeemPoints(request.getRedeemPoints()).loginUserId(request.getLoginUserId())
				.userId(request.getUserId()).makerComments(request.getMakerComments()).unitId(request.getUnitId())
				.globalId(request.getGlobalId()).status(request.getStatus()).nationalId(request.getNationalId())
				.accountType(request.getAccountType()).build();
	}
}
