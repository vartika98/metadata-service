package com.qnb.metadata.dto;

import java.io.Serializable;

import lombok.Data;

import lombok.EqualsAndHashCode;

import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class ChannelDto implements Serializable {

	private static final long serialVersionUID = 8615705817557328343L;

	private Integer txnId;

	private String channelId;

	private String channelDesc;
	
	private String status;
	
	private String description;

}
