package com.qnb.metadata.dto.ooredoo;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LifRewardSumMaster {

	private String userId;
	private String requestComments;
	private String unit;
	private String searchBy;
	private String searchValue;
	private String serviceNo;
	private String refNoString;
	private String date;
	private String type;
	private String reqBy;
	private String qId;
	private String globalId;
	private String customerName;
	private String accountNumber;
	private String mobileNo;
	private String pointBal;
	private String pointBalUpdated;
	private String osBal;
	private String paymentAmount;
	private String redeemPt;
	private String loginIp;
	private String userNo;
	private String requestId;
}
