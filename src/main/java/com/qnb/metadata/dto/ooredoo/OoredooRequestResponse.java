package com.qnb.metadata.dto.ooredoo;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OoredooRequestResponse {

	private String requestId;
	private String workflowId;
	private String workFlowStatus;
	private String processStatus;
	private String makerComments;
	private String checkerComments;
	private Set<String> availableActions;
	private String createdBy;
	private Instant createdTime;
	private String lastModifiedBy;
	private Instant lastModifiedTime;
	private LifRewardSumMaster ooredooPaymentDetails;
	private List<WorkflowHistory> workflowHistory;
	private String unit;

	public static OoredooRequestResponse ooredooRequestResponse(Message message) {
		var ooredooPaymentObject = message.getPayload();
		if (ooredooPaymentObject instanceof LifRewardSumDto) {
			var ooredoo = (LifRewardSumDto) ooredooPaymentObject;
			return OoredooRequestResponse.builder().requestId(message.getRequestId())
					.workflowId(message.getWorkflowId()).workFlowStatus(message.getWorkflowStatus().toString())
					.processStatus(message.getProcessStatus()).availableActions(message.getAvailableActions())
					.createdBy(message.getCreatedBy()).createdTime(message.getCreatedTime())
					.lastModifiedBy(message.getLastModifiedBy()).lastModifiedTime(message.getLastModifiedTime())
					.makerComments(message.getLatestMakerComment()).checkerComments(message.getLatestCheckerComment())
					.workflowHistory(message.getWorkflowHistory())
					.ooredooPaymentDetails(toModifyOoredooPaymentStagedRequest(ooredoo)).unit(message.getUnit())
					.build();
		}
		throw new ResourceNotFoundException("NA", "There is no  request object found", HttpStatus.NOT_FOUND);

	}

	public static LifRewardSumMaster toModifyOoredooPaymentStagedRequest(LifRewardSumDto request) {
		return LifRewardSumMaster.builder().loginIp(request.getLoginIp()).userId(request.getUserId())
				.requestComments(request.getRequestComments()).unit(request.getUnit()).globalId(request.getGlobalId())
				.accountNumber(request.getAccountNumber()).mobileNo(request.getMobileNo())
				.pointBal(request.getPointBal()).paymentAmount(request.getPaymentAmount())
				.redeemPt(request.getRedeemPt()).userNo(request.getUserNo()).customerName(request.getCustomerName())
				.build();

	}

}
