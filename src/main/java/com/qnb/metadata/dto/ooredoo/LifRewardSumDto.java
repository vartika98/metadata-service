package com.qnb.metadata.dto.ooredoo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qnb.metadata.entity.OoredooStagingEntity;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LifRewardSumDto implements Workflowable {

	private static final String SALT = "OP:";

	private String userId;
	private String requestComments;
	private String unit;
	private String searchBy;
	private String searchValue;
	private String serviceNo;
	private String refNoString;
	private String date;
	private String type;
	private String reqBy;
	private String qId;
	private String globalId;
	private String customerName;
	private String accountNumber;
	private String mobileNo;
	private String pointBal;
	private String pointBalUpdated;
	private String osBal;
	private String paymentAmount;
	private String redeemPt;
	private String loginIp;
	private String userNo;
	private String requestId;
	
	public static LifRewardSumDto fromEntity(OoredooStagingEntity entity) {
        return LifRewardSumDto.builder()
                                        .loginIp(entity.getLoginIp())
                                        .userId(entity.getUserId())
                                        .globalId(entity.getGlobalId())
                                        .requestComments(entity.getRequestComments())
                                        .unit(entity.getUnitId())
                                        .customerName(entity.getCustomerName())
                                        .accountNumber(entity.getAccountNumber())
                                        .mobileNo(entity.getMobileNumber())
                                        .pointBal(entity.getPointBalance())
                                        .paymentAmount(entity.getPaymentAmount())
                                        .redeemPt(entity.getRedeemPoints())
                                        .serviceNo(entity.getServiceNumber())
                                        .qId(entity.getNationalId())
                                        .userNo(entity.getUserNo())
                                        .build();
	}
	

	@Override
	public List<String> getIdempotentRequestIdentifier() {
		return List.of(SALT + globalId);
	}

	@Override
	public String getIdempotentRequestDetails() {
		return "Request : Ooredoo Payment for user with userId :" + userId;
	}
}
