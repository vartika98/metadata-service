package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.Menu;

@Repository
public interface MenuRepository extends JpaRepository<Menu, String> {

	@Query(value = "SELECT menu.MENU_ID ,menu.description,menu.CREATED_BY,menu.DATE_CREATED,menu.MODIFIED_BY,menu.DATE_MODIFIED,menu.STATUS,"
			+ "menu.LINK,menu.I18N_SCREEN_ID,menu.PARENT_MENU_ID,menu.MENU_TYPE,menu.PAGE_ID,menu.PRIORITY from OCS_T_MENU_MASTER menu "
			+ "INNER JOIN OCS_T_TXN_ENTITLEMENT txn on txn.MENU_ID = menu.MENU_ID where menu.parent_menu_id is NULL "
			+ "AND txn.unit_id=:unitId and txn.channel_id =:channelId AND txn.status='ACT' AND menu.status='ACT'"
			+ "ORDER BY menu.MENU_ID", nativeQuery = true)
	List<Menu> getModuleList(@Param("unitId") String unitId, @Param("channelId") String channelId);

	@Query(value = "SELECT menu.menu_id ,menu.description ,menu.CREATED_BY,menu.DATE_CREATED,menu.MODIFIED_BY,menu.DATE_MODIFIED,menu.STATUS,"
			+ " menu.LINK,menu.I18N_SCREEN_ID,menu.PARENT_MENU_ID,menu.MENU_TYPE,menu.PAGE_ID,menu.PRIORITY from OCS_T_MENU_MASTER menu "
			+ "INNER JOIN OCS_T_TXN_ENTITLEMENT txn on txn.menu_id = menu.menu_id where menu.parent_menu_id=:menuId "
			+ "AND txn.unit_id=:unitId and txn.channel_id =:channelId AND menu.I18N_SCREEN_ID IS NOT NULL AND txn.status='ACT' AND menu.status='ACT'"
			+ "ORDER BY menu.menu_id", nativeQuery = true)
	List<Menu> getScreenList(@Param("unitId") String unitId, @Param("channelId") String channelId,
			@Param("menuId") String menuId);

}
