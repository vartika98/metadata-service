package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.GroupDefEntity;
import com.qnb.metadata.entity.UserGroupsMembershipEntity;
@Repository
public interface UserGroupMembershipRepository extends JpaRepository<UserGroupsMembershipEntity, String> {

    @Query("SELECT ug FROM UserGroupsMembershipEntity ug where ug.user.userId=:userId")
    List<UserGroupsMembershipEntity> findByUser(@Param("userId") String userId);

    Boolean existsByGroupId(GroupDefEntity groupDefEntity);

}
