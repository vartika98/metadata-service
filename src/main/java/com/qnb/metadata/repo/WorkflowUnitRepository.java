package com.qnb.metadata.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.WorkflowUnit;

@Repository
public interface WorkflowUnitRepository extends JpaRepository<WorkflowUnit, String>{

	WorkflowUnit findByWorkflowId(String workflowId);

}
