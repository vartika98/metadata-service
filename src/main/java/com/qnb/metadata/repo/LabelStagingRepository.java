package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.LabelStagingEntity;

@Repository
public interface LabelStagingRepository extends JpaRepository<LabelStagingEntity, String> {
	
	List<LabelStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIds);

}
