package com.qnb.metadata.repo;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.UserRequestStagingEntity;

@Repository
public interface UserRequestStagingRepository extends JpaRepository<UserRequestStagingEntity, String>{

	Set<UserRequestStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIdLst);

}
