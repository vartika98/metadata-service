package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.UnitDesc;

@Repository
public interface UnitDescRespository extends JpaRepository<UnitDesc, String> {

	UnitDesc findByUnitIdAndLangCodeAndStatus(String unitId, String lang, String status);

	List<UnitDesc> findByLangCodeAndStatus(String lang, String status);

}
