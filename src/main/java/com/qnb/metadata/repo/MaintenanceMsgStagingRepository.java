package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.MaintenanceMsgStagingEntity;

@Repository
public interface MaintenanceMsgStagingRepository extends JpaRepository<MaintenanceMsgStagingEntity, String>{
	
	List<MaintenanceMsgStagingEntity> findByRequestIdIn(List<String> requestIdLst);
}
