package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.IPOStagingEntity;
import com.qnb.metadata.entity.LHPaymentStagingEntity;

@Repository
public interface LHPaymentStagedRepository extends JpaRepository<LHPaymentStagingEntity, String>{

	List<LHPaymentStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIds);
}
