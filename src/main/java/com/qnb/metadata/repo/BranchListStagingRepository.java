package com.qnb.metadata.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.qnb.metadata.entity.BranchListStagingEntity;

@Repository
public interface BranchListStagingRepository extends JpaRepository<BranchListStagingEntity, String>{

	List<BranchListStagingEntity> findByWorkFlowReqIdIn(List<String> requestIdList);
}
