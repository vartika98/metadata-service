package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qnb.metadata.entity.Translation;

@org.springframework.stereotype.Repository
public interface TranslationRepository extends JpaRepository<Translation, Integer>{

	@Query(value = "SELECT * from OCS_T_I18N lbl"
			+ " where lbl.unit_id=:unitId and lbl.channel_id=:channelId AND lbl.SCREEN_ID=:screenId",nativeQuery=true)
	List<Translation> getLabelList(@Param("unitId") String unitId,@Param("channelId") String channelId,@Param("screenId") String screenId);

	List<Translation> findByUnit_UnitIdAndChannel_ChannelIdAndPage(String unitId, String channelId, String screenId);
}
