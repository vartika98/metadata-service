package com.qnb.metadata.repo;

import java.util.List;
import java.util.Set;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.TxnEntitlementRequestStagingEntity;
import com.qnb.metadata.entity.TxnEntitlementSubRequestStagingEntity;

import jakarta.transaction.Transactional;
@Transactional
@Repository
public interface TxnEntitlementRequestStagingRepository extends JpaRepository<TxnEntitlementRequestStagingEntity, String> {
	
//	@EntityGraph(attributePaths = "subStagingEntity")
	Set<TxnEntitlementRequestStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> list);
	
//	@Query("SELECT txn FROM TxnEntitlementRequestStagingEntity txn WHERE txn.requestId IN :requestIds")
	//Set<TxnEntitlementRequestStagingEntity> getRequests(@Param("requestIds")List<String> list);
	
	
//	Set<TxnEntitlementRequestStagingEntity> findWithSubStagingEntityByRequestIdInOrderByCreatedTimeAsc(List<String> list);
	
//	@Query("SELECT p FROM TxnEntitlementRequestStagingEntity p JOIN FETCH p.subStagingEntity WHERE p.requestId IN :requestIds")
//	Set<TxnEntitlementRequestStagingEntity> findWithSubStagingEntityByRequestIdInOrderByCreatedTimeAsc(@Param("requestIds")List<String> requestIds);


}
