package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.ChannelStagingEntity;
@Repository
public interface ChannelStagedRespository extends JpaRepository<ChannelStagingEntity, String>{

	List<ChannelStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIds);

}
