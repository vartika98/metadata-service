package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.Unit;

@Repository
public interface UnitRespository extends JpaRepository<Unit, String> {

	
//	@Query(value = "SELECT * FROM OCS_T_UNIT_MASTER UM INNER JOIN "
//			+ "OCS_T_COUNTRY CM ON UM.COUNTRY_CODE = CM.COUNTRY_CODE AND UM.STATUS =:status",nativeQuery = true)
	List<Unit> findByStatus(@Param("status") String status);

	Unit findByUnitId(String unitId);

}
