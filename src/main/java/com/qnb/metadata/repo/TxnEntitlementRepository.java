package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.TxnEntitlement;

@Repository
public interface TxnEntitlementRepository extends JpaRepository<TxnEntitlement, String> {

	
	List<TxnEntitlement> findByChannelIdAndUnitId(String channelId,String unitId);
}
