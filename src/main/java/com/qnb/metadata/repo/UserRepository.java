package com.qnb.metadata.repo;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.UserEntity;
import com.qnb.metadata.enums.UserStatus;
import com.qnb.metadata.enums.UserType;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {

	Boolean existsByUserId(String userId);

	@Query(value = "SELECT user FROM UserEntity user WHERE user.userId = :id")
	Optional<UserEntity> searchByUserId(String id);

	UserEntity findByUserId(String id);

	@Query("SELECT user FROM UserEntity user WHERE user.userId=:userId and user.userStatus=:status and user.expiryDate >:currentTime")
	UserEntity checkActiveUser(@Param("userId") String userId, @Param("currentTime") Date currentTime,
			@Param("status") UserStatus status);

	@Query("SELECT user FROM UserEntity user WHERE user.userStatus IN :status AND user.userType = :userType "
			+ "ORDER BY user.firstName ASC")
	List<UserEntity> getUserListByStatus(@Param("status") List<UserStatus> status,
			@Param("userType") UserType userType);

}
