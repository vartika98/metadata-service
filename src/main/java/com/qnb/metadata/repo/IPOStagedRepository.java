package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.IPOStagingEntity;

@Repository
public interface IPOStagedRepository extends JpaRepository<IPOStagingEntity, String>{

	List<IPOStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIds);

}
