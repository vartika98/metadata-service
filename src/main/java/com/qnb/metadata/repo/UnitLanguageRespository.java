package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.UnitLanguage;

@Repository

public interface UnitLanguageRespository extends JpaRepository<UnitLanguage, String> {

	List<UnitLanguage> findByUnit_UnitIdAndChannel_ChannelId(String unit,String channel);

}
