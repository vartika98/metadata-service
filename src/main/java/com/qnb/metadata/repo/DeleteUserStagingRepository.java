package com.qnb.metadata.repo;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.DeleteUserStagingEntity;

@Repository
public interface DeleteUserStagingRepository extends JpaRepository<DeleteUserStagingEntity, String> {

	Set<DeleteUserStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIdLst);

}
