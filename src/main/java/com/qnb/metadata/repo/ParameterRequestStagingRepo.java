package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.ParamConfigStagingEntity;

@Repository
public interface ParameterRequestStagingRepo extends JpaRepository<ParamConfigStagingEntity, String>{
	List<ParamConfigStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIdLst);
}
