package com.qnb.metadata.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.qnb.metadata.entity.HBTFStagingEntity;

@Repository
public interface HBTFStagingRepository extends JpaRepository<HBTFStagingEntity, String>{
	
	List<HBTFStagingEntity> findAllByRequestIdIn(List<String> requestIdLst);
}
