package com.qnb.metadata.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.qnb.metadata.entity.ParamConfigEntity;
import com.qnb.metadata.enums.TxnStatus;

@Repository
public interface ParamConfigRepository extends JpaRepository<ParamConfigEntity, Integer> {

	List<ParamConfigEntity> findByUnitIdAndChannelIdAndStatusOrderByConfKeyAsc(String unitId, String channelId, TxnStatus status);
	
	List<ParamConfigEntity> findByConfKeyAndStatus(String key, TxnStatus status);
	
	List<ParamConfigEntity> findByConfKey(String key);
}
