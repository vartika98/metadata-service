package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.GroupDefEntity;

@Repository
public interface GroupDefRepository extends JpaRepository<GroupDefEntity, String>{

	@Query("SELECT ug FROM GroupDefEntity ug where ug.groupId=:groupId")
    GroupDefEntity findByGroupCode(@Param("groupId") String groupId);
 
    Boolean existsByGroupCode(String groupCode);

    Boolean existsByGroupId(String groupId);
 
    @Query("SELECT ug FROM GroupDefEntity ug where ug.groupId <> :groupId"
    		+ " ORDER BY ug.groupCode ASC")
	List<GroupDefEntity> getAllGroupsExceptSystemGroup(@Param("groupId") String groupId);
}
