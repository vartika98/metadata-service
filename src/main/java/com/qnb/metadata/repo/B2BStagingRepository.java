package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.B2BStagingEntity;

@Repository
public interface B2BStagingRepository extends JpaRepository<B2BStagingEntity, String>{

	List<B2BStagingEntity> findByRequestIdIn(List<String> requestIdList);
}
