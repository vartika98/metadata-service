package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.IPOStagingEntity;
import com.qnb.metadata.entity.SmartInstStagingEntity;

@Repository
public interface SmartInstStagedRepository extends JpaRepository<SmartInstStagingEntity, String>{

	List<SmartInstStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIds);

}
