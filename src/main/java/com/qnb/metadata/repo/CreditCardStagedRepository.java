package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.CreditCardStagingEntity;

@Repository
public interface CreditCardStagedRepository extends JpaRepository<CreditCardStagingEntity, String>{

	List<CreditCardStagingEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIds);

}
