package com.qnb.metadata.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qnb.metadata.entity.WorkflowEntity;

@Repository
public interface WorkflowRepository extends JpaRepository<WorkflowEntity, String> {

	List<WorkflowEntity> findByRequestIdInOrderByCreatedTimeAsc(List<String> requestIds);

	
}
