package com.qnb.metadata.exception;

import org.springframework.http.HttpStatus;

public class ServiceException extends BaseRuntimeException {
	   
    private static final long serialVersionUID = 1986615807100508145L;

    public ServiceException(String errorCode, String errorMessage, HttpStatus status, Throwable cause) {
                    super(errorCode, errorMessage, status, cause);
    }
   
    public ServiceException(String errorCode, String errorMessage, HttpStatus status) {
                    super(errorCode, errorMessage, status);
    }

    public ServiceException(String errorCode, String errorMessage) {
                    super(errorCode, errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
