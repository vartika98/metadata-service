package com.qnb.metadata.exception;

import org.springframework.http.HttpStatus;

public class ValidationException extends BaseRuntimeException {

	private static final long serialVersionUID = -1543656882726696837L;

	public ValidationException(String errorCode, String errorMessage, HttpStatus status, Throwable cause) {
		super(errorCode, errorMessage, status, cause);
	}

	public ValidationException(String errorCode, String errorMessage, HttpStatus status) {
		super(errorCode, errorMessage, status);
	}

	public ValidationException(String errorCode, String errorMessage) {
		super(errorCode, errorMessage, HttpStatus.BAD_REQUEST);
	}
}
