package com.qnb.metadata.exception;

public class ChannelException extends Exception {

	private static final long serialVersionUID = -1L;

	private String strMessage = null;

	public ChannelException(String strMString) {

		super(strMString);

		this.strMessage = strMString;

	}

	@Override
	public String toString() {

		return String.format("ChannelException [strMessage=%s]", strMessage);

	}

}
