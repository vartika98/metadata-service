package com.qnb.metadata.exception;

import lombok.Getter;

@Getter
public class WorkflowException extends RuntimeException {
	private static final long serialVersionUID = -8810027151436992519L;
	private final String message;
	private final String code;

	public WorkflowException(String message) {
		this.message = message;
		this.code = null;
	}

	public WorkflowException(String code, String message) {
		this.message = message;
		this.code = code;
	}
}
