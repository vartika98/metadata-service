package com.qnb.metadata.enums;

public enum RequestActionType {
	ADD,
    MODIFY,
    DELETE,
    ENABLE,
    DISABLE,
    TERMINATE,
    NA
}
