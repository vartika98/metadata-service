package com.qnb.metadata.enums;

public enum TxnStatus {
	ACT,
	IAC,
	TER;
 
	public static TxnStatus parseTxnStatus(String status) {
		for(var stat : TxnStatus.values()) {
			if(stat.name().equalsIgnoreCase(status)) {
				return stat;
			}
		}
		return null;
	}
}
