package com.qnb.metadata.enums;

public enum UserType {
    FUNCTIONAL,
    SYSTEM
}
