package com.qnb.metadata.enums;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum GroupType {

	FUNCTIONAL,
	   SYSTEM
}
