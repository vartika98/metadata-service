package com.qnb.metadata.enums;

public enum UserStatus {
    OPEN,
    ENABLED,
    DISABLED,
    CLOSED,
    TERMINATED;
 
    public static UserStatus parseUserStatus(String status) {
        for (var stat : UserStatus.values()) {
            if (stat.name().equalsIgnoreCase(status)) {
                return stat;
            }
        }
        return null;
    }
 
}
