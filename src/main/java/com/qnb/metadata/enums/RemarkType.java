package com.qnb.metadata.enums;

public enum RemarkType {
	SYSTEM, MAKER, CHECKER
}
