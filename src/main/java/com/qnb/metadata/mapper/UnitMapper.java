package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;

import org.mapstruct.ReportingPolicy;

import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.UnitDto;
import com.qnb.metadata.entity.Unit;


@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UnitMapper {

	UnitDto unitToDto(Unit unit);

	Unit unitDtoToUnit(UnitDto unitDto);

}
