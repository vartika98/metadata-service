package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.smartInstallment.SmartInstDto;
import com.qnb.metadata.entity.SmartInstStagingEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface SmartInstStagedMapper {

	SmartInstDto smartInstStagedEntityToDto(SmartInstStagingEntity smartInstStagedEntity);
}
