package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.user.DeleteUserStagedRequest;
import com.qnb.metadata.entity.DeleteUserStagingEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface DeleteUserStagedMapper {

	DeleteUserStagedRequest delUserStagedEntityToDto(DeleteUserStagingEntity delUserStagedEntity);
}
