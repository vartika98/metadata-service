package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;

import org.mapstruct.ReportingPolicy;

import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.LanguageDto;
import com.qnb.metadata.entity.Language;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface LanguageMapper {

	LanguageDto languageToDto(Language language);

	Language languageDtotoLanguage(LanguageDto languageDto);

}
