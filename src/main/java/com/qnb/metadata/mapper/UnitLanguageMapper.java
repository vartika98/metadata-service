package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.UnitLanguageDto;
import com.qnb.metadata.entity.UnitLanguage;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UnitLanguageMapper {

	UnitLanguageDto unitLanguageToDto(UnitLanguage unitLanguage);

	UnitLanguage unitLanguageDtoToUnitLanguage(UnitLanguageDto unitLanguageDto);

}
