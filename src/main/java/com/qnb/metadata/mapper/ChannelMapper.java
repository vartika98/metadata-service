package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.ChannelDto;
import com.qnb.metadata.dto.channel.ChannelStagedRequest;
import com.qnb.metadata.entity.Channel;
import com.qnb.metadata.entity.ChannelStagingEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ChannelMapper {

	ChannelDto channelModeltoDto(Channel channel);

	Channel channelDtotoModel(ChannelDto channelDto);
	
	ChannelStagedRequest channelStagedEntityToDto(ChannelStagingEntity channelStagedEntity);

}
