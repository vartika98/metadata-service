package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.LoyaltyHarrods.LHPaymentStagedRequest;
import com.qnb.metadata.entity.LHPaymentStagingEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface LHPaymentStagedMapper {

	LHPaymentStagedRequest lhPaymentStagedEntityToDto(LHPaymentStagingEntity ipoStagedEntity);
}
