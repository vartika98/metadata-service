package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.ooredoo.LifRewardSumDto;
import com.qnb.metadata.entity.OoredooStagingEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface OoredooStagedMapper {

	LifRewardSumDto ooredooStagedEntityToDto(OoredooStagingEntity ooredooStagedEntity);
}
