package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.ipo.IPOStagedReq;
import com.qnb.metadata.entity.IPOStagingEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface IPOStagedMapper {

	IPOStagedReq ipoStagedEntityToDto(IPOStagingEntity ipoStagedEntity);
}




