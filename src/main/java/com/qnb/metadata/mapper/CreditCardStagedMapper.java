package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.creditCard.CCPaymentStagedRequest;
import com.qnb.metadata.entity.CreditCardStagingEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CreditCardStagedMapper {

	CCPaymentStagedRequest cCardStagedEntityToDto(CreditCardStagingEntity ccStagedEntity);
}
