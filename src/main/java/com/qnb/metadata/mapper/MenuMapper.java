package com.qnb.metadata.mapper;

import java.awt.Menu;

import org.mapstruct.Mapper;

import org.mapstruct.ReportingPolicy;

import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.MenuDto;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface MenuMapper {

	MenuDto menuToDto(Menu menu);

	Menu menuDtoToMenu(MenuDto menuDto);

}
