package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.errorConfig.ErrorConfigStagedRequest;
import com.qnb.metadata.entity.ErrorConfigStagingEntity;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ErrorConfigStagedMapper {

	ErrorConfigStagedRequest errConfigStagedEntityToDto(ErrorConfigStagingEntity ipoStagedEntity);
}
