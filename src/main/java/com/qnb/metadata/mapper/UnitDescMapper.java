package com.qnb.metadata.mapper;

import org.mapstruct.Mapper;

import org.mapstruct.Mapping;

import org.mapstruct.ReportingPolicy;

import org.springframework.stereotype.Component;

import com.qnb.metadata.dto.UnitDescDto;
import com.qnb.metadata.entity.UnitDesc;

@Component
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UnitDescMapper {

	@Mapping(source = "unitDesc", target = "unitDesc")

	UnitDescDto unitDescToDto(UnitDesc unitDesc);

}
