package com.qnb.metadata.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;

import lombok.Data;

import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultUtilVO implements Serializable {

	private static final long serialVersionUID = 4191307163490295428L;

	private String code;

	private String description;

	@JsonIgnore
	private String mwCode;

	@JsonIgnore
	private String mwdesc;

	public ResultUtilVO(String code, String description) {
		super();
		this.code = code;
		this.description = description;

	}

}
