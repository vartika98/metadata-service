package com.qnb.metadata.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;

import lombok.Data;

import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponse<T> implements Serializable {

	private static final long serialVersionUID = -848439782168993599L;

	private ResultUtilVO status;

	private T data;

}
