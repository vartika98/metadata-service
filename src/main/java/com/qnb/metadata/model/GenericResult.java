package com.qnb.metadata.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;

import lombok.Data;

import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericResult implements Serializable {

	private static final long serialVersionUID = 3852970767080738045L;

	private ResultUtilVO result;

}
