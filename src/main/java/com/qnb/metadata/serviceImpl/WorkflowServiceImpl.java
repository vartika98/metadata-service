package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.dto.workflow.Workflow;
import com.qnb.metadata.dto.workflow.WorkflowHistory;
import com.qnb.metadata.entity.WorkflowEntity;
import com.qnb.metadata.entity.WorkflowHistoryEntity;
import com.qnb.metadata.entity.WorkflowUnit;
import com.qnb.metadata.enums.WorkflowStatus;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.WorkflowHistoryRepository;
import com.qnb.metadata.repo.WorkflowRepository;
import com.qnb.metadata.repo.WorkflowUnitRepository;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WorkflowServiceImpl implements WorkflowService {

	@Autowired
	WorkflowRepository workflowRepository;

	@Autowired
	WorkflowUnitRepository workflowUnitRepo;

	ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	WorkflowHistoryRepository workflowHistoryRepo;

	public List<Workflow> workflowByRequestIds(List<String> requestIds) {
		log.info("Workflow requestId:{}", requestIds);
		try {
			List<WorkflowEntity> workflows = workflowRepository.findByRequestIdInOrderByCreatedTimeAsc(requestIds);
			if (Objects.nonNull(workflows) && !workflows.isEmpty()) {
				return workflows.stream().map(this::fromEntity).collect(Collectors.toList());
			}
		} catch (Exception e) {
			log.info("Exception in calling workflowByRequestIds :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		return null;
	}

	private Workflow fromEntity(WorkflowEntity entity) {
		WorkflowUnit wuEntity = workflowUnitRepo.findByWorkflowId(entity.getWorkflowId());
		return Workflow.builder().workflowId(entity.getWorkflowId()).requestId(entity.getRequestId())
				.processId(entity.getProcessId()).initiatedGroup(entity.getInitiatedGroup())
				.workflowGroup(entity.getWorkflowGroup())
				.availableActions(availableActions(entity.getAvailableActions())).taskId(entity.getTaskId())
				.assignTo(entity.getAssignedGroup()).status(entity.getWorkflowStatus())
				.scheduledTime(entity.getScheduledTime()).createdBy(entity.getCreatedBy())
				.processStatus(entity.getLastPerformedAction()).historyCount(entity.getHistoryCount())
				.lastModifiedBy(entity.getLastModifiedBy()).createdTime(entity.getCreatedTime().toInstant())
				.lastModifiedTime(entity.getLastModifiedTime().toInstant())
				.unit(wuEntity != null ? wuEntity.getUnitId() : "").build();
	}

	private Set<String> availableActions(String actions) {
		Set<String> avlAction = new HashSet<>();
		if (actions != null) {
			avlAction = Set.of(actions.split(","));
		}
		return avlAction;
	}

	public List<Message> requestDetails(Map<String,Workflowable> reqObject,List<String> requestIds){
		List<Message> messageRes = new ArrayList<>();
		try {
			var workflows = workflowByRequestIds(requestIds);
			messageRes = workflows.stream().filter(w -> reqObject.get(w.getRequestId())!=null)
			  .map(workflow->buildMessage(workflow,workflowHistory(workflow.getWorkflowId()),reqObject.get(workflow.getRequestId())))
				.collect(Collectors.toList());
		} catch (Exception e) {
			log.info("Exception in calling workflowByRequestIds :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		return messageRes;
	}

	private Message buildMessage(Workflow workflow, List<WorkflowHistory> workflowHistory, Workflowable payload) {
		return Message.builder().payload(payload).workflowId(workflow.getWorkflowId())
				.requestId(workflow.getRequestId()).createdBy(workflow.getCreatedBy())
				.createdTime(workflow.getCreatedTime()).lastModifiedBy(workflow.getLastModifiedBy())
				.lastModifiedTime(workflow.getLastModifiedTime()).workflowStatus(workflow.getStatus())
				.workflowHistory(workflowHistory).processStatus(workflow.getProcessStatus())
				.availableActions(workflow.getAvailableActions()).unit(workflow.getUnit()).build();
	}

	public List<WorkflowHistory> workflowHistory(String workflowId) {
		var workflowHistories = workflowHistoryRepo.findByWorkflowIdOrderBySeqNumber(workflowId);
		if (workflowHistories != null) {
			return workflowHistories.stream().map(this::formWorkflowHis).collect(Collectors.toList());
		}
		return List.of();
	}
	
	public WorkflowHistory formWorkflowHis(WorkflowHistoryEntity workflowEntity) {
		return new WorkflowHistory(workflowEntity.getWorkflowHistoryId(),workflowEntity.getWorkflowId(),
				workflowEntity.getRequestId(),workflowEntity.getProcessId(),workflowEntity.getSeqNumber(),
				WorkflowStatus.valueOf(workflowEntity.getWorkflowStatus()),workflowEntity.getTaskId(),workflowEntity.getRemark(),
				workflowEntity.getRemarkType(),workflowEntity.getPerformedAction(),workflowEntity.getCreatedBy(),
				workflowEntity.getCreatedTime().toInstant());
	}

}
