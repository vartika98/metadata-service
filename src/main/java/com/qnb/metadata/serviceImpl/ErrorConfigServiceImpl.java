package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.errorConfig.ErrorConfigRequestResponse;
import com.qnb.metadata.dto.errorConfig.ErrorConfigStagedRequest;
import com.qnb.metadata.dto.errorConfig.ErrorParameterRequest;
import com.qnb.metadata.dto.smartInstallment.SmartInstDto;
import com.qnb.metadata.dto.smartInstallment.SmartInstRequestResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.ErrorConfigStagingEntity;
import com.qnb.metadata.enums.TxnStatus;
import com.qnb.metadata.mapper.ErrorConfigStagedMapper;
import com.qnb.metadata.mapper.SmartInstStagedMapper;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.ErrorConfigStagedRepository;
import com.qnb.metadata.repo.SmartInstStagedRepository;
import com.qnb.metadata.service.ErrorConfigService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ErrorConfigServiceImpl implements ErrorConfigService {

	@Autowired
	WorkflowService workflowService;

	@Autowired
	ErrorConfigStagedRepository errConfigStagedRepo;

	@Autowired
	ErrorConfigStagedMapper errConfigStagedMapper;

	ResultUtilVO resultVo = new ResultUtilVO();

	@Override
	public GenericResponse<List<ErrorConfigRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody) {
		GenericResponse<List<ErrorConfigRequestResponse>> response = new GenericResponse<>();
		List<ErrorConfigRequestResponse> responseMap = new ArrayList<>();
		Map<String, Workflowable> resData = new HashMap<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var errConfigReqLst = errConfigStagedRepo.findByWorkFlowReqIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if (Objects.nonNull(errConfigReqLst) && !errConfigReqLst.isEmpty()) {
				for (var errorConfig : errConfigReqLst) {
					if (resData.containsKey(errorConfig.getWorkFlowReqId())) {
						ErrorConfigStagedRequest oldEntity = (ErrorConfigStagedRequest) resData
								.get(errorConfig.getWorkFlowReqId());
						List<ErrorParameterRequest> paramReq = oldEntity.getParamDetails();
						paramReq.addAll(formErrParameter(errorConfig));
						oldEntity.setParamDetails(paramReq);
					} else {
						resData.put(errorConfig.getWorkFlowReqId(), fromEntity(errorConfig));
					}
				}
				List<Message> message = workflowService.requestDetails(resData, reqBody.get("requestIds"));

				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(ErrorConfigRequestResponse::errorConfigRequestGetResponse)
							.collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getPendingRequest inside SmartInstallmentServiceImpl:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	private static List<ErrorParameterRequest> formErrParameter(ErrorConfigStagingEntity entity) {
		List<ErrorParameterRequest> errorParameterReq = new ArrayList<>();
		ErrorParameterRequest req = new ErrorParameterRequest();
		if (entity != null) {
			req.setAction(entity.getActionType());
			req.setMwErrCode(entity.getMwErrCode());
			req.setOcsErrCode(entity.getOcsErrCode());
			req.setRemarks(entity.getRemarks());
			req.setErrDescEng(entity.getErrDescEng());
			req.setErrDescArb(entity.getErrDescArb());
			req.setErrDescFr(entity.getErrDescFr());
			req.setServiceType(entity.getServiceType());
			req.setErConfigId(Objects.nonNull(entity.getErConfigId()) ? entity.getErConfigId() : 0);
			req.setStatus(TxnStatus
					.valueOf(Objects.nonNull(entity.getStatus()) ? entity.getStatus() : TxnStatus.TER.toString()));
			errorParameterReq.add(req);
		}
		return errorParameterReq;
	}
	
	private ErrorConfigStagedRequest fromEntity(ErrorConfigStagingEntity entity) {
        return ErrorConfigStagedRequest.builder()
                                        .unitId(entity.getUnitId())
                                        .channelId(entity.getChannelId())
                                        .createdBy(entity.getCreatedBy())
                                        .dateCreated(entity.getDateCreated())
                                        .modifiedBy(entity.getModifiedBy())
                                        .dateModified(entity.getDateModified())
                                        .serviceType(entity.getServiceType())
                                        .paramDetails(formErrParameter(entity))
                                        .build();
}

}
