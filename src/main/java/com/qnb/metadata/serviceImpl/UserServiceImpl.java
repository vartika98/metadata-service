package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.ooredoo.LifRewardSumDto;
import com.qnb.metadata.dto.ooredoo.OoredooRequestResponse;
import com.qnb.metadata.dto.role.GroupDef;
import com.qnb.metadata.dto.role.GroupRequestResponse;
import com.qnb.metadata.dto.user.DeleteUserRequestResponse;
import com.qnb.metadata.dto.user.DeleteUserStagedRequest;
import com.qnb.metadata.dto.user.UnitMaster;
import com.qnb.metadata.dto.user.User;
import com.qnb.metadata.dto.user.UserRequestResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.GroupDefEntity;
import com.qnb.metadata.entity.GroupRequestStagingEntity;
import com.qnb.metadata.entity.Unit;
import com.qnb.metadata.entity.UserRequestStagingEntity;
import com.qnb.metadata.mapper.DeleteUserStagedMapper;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.DeleteUserStagingRepository;
import com.qnb.metadata.repo.GroupDefRepository;
import com.qnb.metadata.repo.GroupRequestStagingRepository;
import com.qnb.metadata.repo.UnitRespository;
import com.qnb.metadata.repo.UserRequestStagingRepository;
import com.qnb.metadata.service.UserService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

	ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	UserRequestStagingRepository userRequestStagingRepo;

	@Autowired
	WorkflowService workflowService;

	@Autowired
	UnitRespository unitRespository;

	@Autowired
	GroupDefRepository groupDefRepository;
	
	@Autowired
	DeleteUserStagingRepository delUserStagingRepo;
	
	@Autowired
	DeleteUserStagedMapper delUserStagedMapper;

	@Override
	public GenericResponse<List<UserRequestResponse>> getUserRequest(Map<String, List<String>> reqBody) {
		GenericResponse<List<UserRequestResponse>> response = new GenericResponse<>();
		List<UserRequestResponse> responseMap = new ArrayList<>();
		try {
			var reqUserLst = userRequestStagingRepo.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if (Objects.nonNull(reqUserLst) && !reqUserLst.isEmpty()) {
				Map<String, Workflowable> resData = new HashMap<>();
				for (var userReq : reqUserLst) {
					resData.put(userReq.getRequestId(), fromEntity(userReq));
				}
				// Map message calling
				List<Message> message = workflowService.requestDetails(resData, reqBody.get("requestIds"));
				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(UserRequestResponse::userRequestResponse)
							.collect(Collectors.toList());
					response.setData(responseMap);
					resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getUserRequest :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	private User fromEntity(UserRequestStagingEntity userEntity) {
		final var groupDefs = getGroupDomainObjects(userEntity.getUserGroups());
		final var unitMasters = getUnitDomainObjects(userEntity.getUserUnits());
		var groups = (userEntity.getUserGroups() != null) ? Set.of(userEntity.getUserGroups().split(","))
				: new HashSet<String>();
		var units = (userEntity.getUserUnits() != null) ? Set.of(userEntity.getUserUnits().split(","))
				: new HashSet<String>();
		return User.builder().userId(userEntity.getUserId()).firstName(userEntity.getFirstName())
				.lastName(userEntity.getLastName()).dob(userEntity.getDob()).mobileNumber(userEntity.getMobileNumber())
				.expiryDate(userEntity.getExpiryDate()).emailId(userEntity.getEmailId()).groups(groups)
				.groupDefs(groupDefs).units(units).userStatus(userEntity.getStatus())
				.unitMaster(
						unitMasters.stream().filter(unit -> !unit.getStatus().isBlank()).collect(Collectors.toSet()))
				.action(userEntity.getAction()).makerComments(userEntity.getRequestComments()).build();
	}

	private Set<UnitMaster> getUnitDomainObjects(String persistedUnits) {
		if (StringUtils.isNotBlank(persistedUnits)) {
			final var units = persistedUnits.split(",");
			var unitDefEntities = unitRespository.findAllById(List.of(units));
			return unitDefEntities.stream().map(this::fromEntity).collect(Collectors.toSet());
		}
		return new HashSet<UnitMaster>();
	}

	private UnitMaster fromEntity(Unit unitMasterEntity) {
		return UnitMaster.builder().unitId(unitMasterEntity.getUnitId()).unitDesc(unitMasterEntity.getUnitDesc())
				.countryCode(
						(unitMasterEntity.getCountryCode() != null) ? unitMasterEntity.getCountryCode().getCountryCode()
								: "")
				.countryDesc((unitMasterEntity.getCountryCode() != null) ? unitMasterEntity.getCountryCode().getStatus()
						: "")
				.status((unitMasterEntity.getCountryCode() != null) ? unitMasterEntity.getCountryCode().getCountryDesc()
						: "")
				.build();
	}

	private Set<GroupDef> getGroupDomainObjects(String persistedGroups) {
		if (StringUtils.isNotBlank(persistedGroups)) {
			final var groups = persistedGroups.split(",");
			var groupDefEntities = groupDefRepository.findAllById(List.of(groups));
			return groupDefEntities.stream().map(this::fromEntity).collect(Collectors.toSet());
		}
		return new HashSet<GroupDef>();
	}

	private GroupDef fromEntity(GroupDefEntity groupDefEntity) {
		return GroupDef.builder().groupId(groupDefEntity.getGroupId()).groupCode(groupDefEntity.getGroupCode())
				.groupDescription(groupDefEntity.getGroupDescription()).build();
	}

	@Override
	public GenericResponse<List<DeleteUserRequestResponse>> getDeleteUserRequest(Map<String, List<String>> reqBody) {
		GenericResponse<List<DeleteUserRequestResponse>> response = new GenericResponse<>();
		List<DeleteUserRequestResponse> responseMap = new ArrayList<>();
		Map<String, Workflowable> resData = new HashMap<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var deleteUsrReqLst = delUserStagingRepo.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if (Objects.nonNull(deleteUsrReqLst) && !deleteUsrReqLst.isEmpty()) {
				List<DeleteUserStagedRequest> delUserReqLstDto = deleteUsrReqLst.stream()
						.map(delUserStaged -> delUserStagedMapper.delUserStagedEntityToDto(delUserStaged)).collect(Collectors.toList());
				resData = delUserReqLstDto.stream().collect(Collectors.toMap(DeleteUserStagedRequest::getRequestId, item -> item));
				List<Message> message = workflowService.requestDetails(resData, reqBody.get("requestIds"));

				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(DeleteUserRequestResponse::deleteUserRequestResponse)
							.collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getPendingRequestLst :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
