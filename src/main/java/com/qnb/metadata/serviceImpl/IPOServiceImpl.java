package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.ipo.IPORequestResponse;
import com.qnb.metadata.dto.ipo.IPOStagedReq;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.mapper.IPOStagedMapper;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.IPOStagedRepository;
import com.qnb.metadata.service.IPOService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IPOServiceImpl implements IPOService{
	
	ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	IPOStagedRepository ipoStagedRepository;
	
	@Autowired
	IPOStagedMapper ipoStagedMapper;
	
	@Autowired
	WorkflowService workflowService;
	
	@Override
	public GenericResponse<List<IPORequestResponse>> getPendingRequestLst(Map<String, List<String>> reqBody) {
		GenericResponse<List<IPORequestResponse>> response = new GenericResponse<>();
		List<IPORequestResponse> responseMap = new ArrayList<>();
		Map<String,Workflowable> resData = new HashMap<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var ipoReqLst = ipoStagedRepository.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if(Objects.nonNull(ipoReqLst) && !ipoReqLst.isEmpty()) {
				List<IPOStagedReq> iporeqLstDto = ipoReqLst.stream()
						.map(ipoStaged ->ipoStagedMapper.ipoStagedEntityToDto(ipoStaged)).collect(Collectors.toList());
				resData = iporeqLstDto.stream().collect(Collectors.toMap(IPOStagedReq::getRequestId, item -> item));
				List<Message> message = workflowService.requestDetails(resData,reqBody.get("requestIds"));
				
				if(Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(IPORequestResponse::ipoAdminRequestGetResponse).collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		}catch (Exception e) {
			log.info("Exception in calling getPendingRequestLst :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
