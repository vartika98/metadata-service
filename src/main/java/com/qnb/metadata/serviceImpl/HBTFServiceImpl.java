package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.hbtf.HBTFMaster;
import com.qnb.metadata.dto.hbtf.HBTFRequestGetResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.HBTFStagingEntity;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.HBTFStagingRepository;
import com.qnb.metadata.service.HBTFService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HBTFServiceImpl implements HBTFService{
	
	@Autowired
	private HBTFStagingRepository hbtfStagingRepository;
	@Autowired
	private WorkflowService workflowService;
	private ResultUtilVO resultVo = new ResultUtilVO();
	
	@Override
	public GenericResponse<List<HBTFRequestGetResponse>> getHBTFRequestByIds(List<String> requestIdLst) {
		GenericResponse<List<HBTFRequestGetResponse>> response = new GenericResponse<>();
		List<HBTFRequestGetResponse> responseList = new ArrayList<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var requiredList = hbtfStagingRepository.findAllByRequestIdIn(requestIdLst);
			Map<String, Workflowable> requestObjects = new HashMap<>();
			if(requiredList != null && !requiredList.isEmpty()) {
				for(var entity : requiredList) {
					requestObjects.put(entity.getRequestId(), fromEntity(entity));
				}
			}
			List<Message> message = workflowService.requestDetails(requestObjects,requestIdLst);
			if (Objects.nonNull(message) && !message.isEmpty()) {
				responseList = message.stream()
						.map(HBTFRequestGetResponse::hbtfRequestGetResponse).collect(Collectors.toList());
				response.setData(responseList);
				
			}
		} catch (Exception e) {
			log.info("Exception in calling HBTF details :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	
	private HBTFMaster fromEntity(HBTFStagingEntity entity) {
		return HBTFMaster.builder()
				.userId(entity.getUserId())
				.globalId(entity.getGlobalId())
				.requestComments(entity.getRequestComments())
				.requestId(entity.getRequestId())
				.unitId(entity.getUnitId())
				.debitAccountNo(entity.getDebitAccountNo())
				.requestedBy(entity.getRequestedBy())
				.requestedDate(entity.getRequestedDate())
				.refNo(entity.getRefNo())
				.status(entity.getStatus())
				.rejectedBy(entity.getRejectedBy())
				.rejectedDate(entity.getRejectedDate())
				.rejectCount(entity.getRejectCount())
				.workflowId(entity.getWorkflowId())
				.checkerComments(entity.getCheckerComments())
				.requestId(entity.getRequestId())
				.build();
	}
}