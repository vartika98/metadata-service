package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.creditCard.CCPaymentStagedRequest;
import com.qnb.metadata.dto.creditCard.CCRequestResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.mapper.CreditCardStagedMapper;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.CreditCardStagedRepository;
import com.qnb.metadata.service.CreditCardService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CreditCardServiceImpl implements CreditCardService {

	ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	CreditCardStagedRepository ccStagedRepository;

	@Autowired
	CreditCardStagedMapper creditCardStagedMapper;

	@Autowired
	WorkflowService workflowService;

	@Override
	public GenericResponse<List<CCRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody) {
		GenericResponse<List<CCRequestResponse>> response = new GenericResponse<>();
		List<CCRequestResponse> responseMap = new ArrayList<>();
		Map<String, Workflowable> resData = new HashMap<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var creditCardReqLst = ccStagedRepository.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if (Objects.nonNull(creditCardReqLst) && !creditCardReqLst.isEmpty()) {
				List<CCPaymentStagedRequest> cCardReqLstDto = creditCardReqLst.stream()
						.map(ccStaged -> creditCardStagedMapper.cCardStagedEntityToDto(ccStaged)).collect(Collectors.toList());
				resData = cCardReqLstDto.stream().collect(Collectors.toMap(CCPaymentStagedRequest::getRequestId, item -> item));
				List<Message> message = workflowService.requestDetails(resData, reqBody.get("requestIds"));

				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(CCRequestResponse::ccPaymentRequestGetResponse)
							.collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getPendingRequestLst of CreditCardServiceImpl :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
