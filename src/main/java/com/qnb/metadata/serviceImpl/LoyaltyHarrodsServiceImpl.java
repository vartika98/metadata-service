package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.LoyaltyHarrods.LHPaymentRequestResponse;
import com.qnb.metadata.dto.LoyaltyHarrods.LHPaymentStagedRequest;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.mapper.LHPaymentStagedMapper;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.LHPaymentStagedRepository;
import com.qnb.metadata.service.LoyaltyHarrodsService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoyaltyHarrodsServiceImpl implements LoyaltyHarrodsService {

	ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	LHPaymentStagedRepository lhPaymentStagedRepo;

	@Autowired
	LHPaymentStagedMapper lhPaymentStagedMapper;

	@Autowired
	WorkflowService workflowService;

	@Override
	public GenericResponse<List<LHPaymentRequestResponse>> getPendingRequestLst(Map<String, List<String>> reqBody) {
		GenericResponse<List<LHPaymentRequestResponse>> response = new GenericResponse<>();
		List<LHPaymentRequestResponse> responseMap = new ArrayList<>();
		Map<String, Workflowable> resData = new HashMap<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var lhPaymentReqLst = lhPaymentStagedRepo.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if (Objects.nonNull(lhPaymentReqLst) && !lhPaymentReqLst.isEmpty()) {
				List<LHPaymentStagedRequest> lhPaymentReqLstDto = lhPaymentReqLst.stream()
						.map(lhPaymentStaged -> lhPaymentStagedMapper.lhPaymentStagedEntityToDto(lhPaymentStaged)).collect(Collectors.toList());
				resData = lhPaymentReqLstDto.stream().collect(Collectors.toMap(LHPaymentStagedRequest::getRequestId, item -> item));
				List<Message> message = workflowService.requestDetails(resData, reqBody.get("requestIds"));

				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(LHPaymentRequestResponse::lhPaymentRequestResponse)
							.collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getPendingRequestLst :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
