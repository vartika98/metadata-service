package com.qnb.metadata.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.ChannelDto;
import com.qnb.metadata.dto.UnitListRes;
import com.qnb.metadata.dto.UnitMasterDto;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.UnitRespository;
import com.qnb.metadata.service.UnitService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UnitServiceImpl implements UnitService {

	@Autowired
	private UnitRespository unitMasterRespository;

	ResultUtilVO resultVo = new ResultUtilVO();
	
	@Override
	public GenericResponse<UnitListRes> getAllUnits(final String unitStatus) {
		GenericResponse<UnitListRes> response = new GenericResponse<>();
		UnitListRes resData = new UnitListRes();
		try {
			List<UnitMasterDto> unitLst = unitMasterRespository.findByStatus(unitStatus).stream()
					.map(c -> UnitMasterDto.builder().unitCode(c.getUnitId()).unitDesc(c.getUnitDesc())
							.countryCode(c.getCountryCode().getCountryCode())
							.countryDesc(c.getCountryCode().getCountryDesc()).status(c.getStatus()).build())
					.collect(Collectors.toList());
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			resData.setUnits(unitLst);
			response.setData(resData);
		} catch (Exception e) {
			log.info("Exception in calling getAllChannels :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
