package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.appMaintenance.MaintenanceAppDetail;
import com.qnb.metadata.dto.appMaintenance.MaintenanceLangDetails;
import com.qnb.metadata.dto.appMaintenance.MaintenanceMessage;
import com.qnb.metadata.dto.appMaintenance.MaintenanceRequestDetailsResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.MaintenanceMsgStagingEntity;
import com.qnb.metadata.entity.MaintenanceMsgSubStagingEntity;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.MaintenanceMsgStagingRepository;
import com.qnb.metadata.service.MessageMaintenanceService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MessageMaintenanceServiceImpl implements MessageMaintenanceService{
	
	ResultUtilVO resultVo = new ResultUtilVO();
	@Autowired
	WorkflowService workflowService;
	@Autowired
	private MaintenanceMsgStagingRepository maintenanceMsgStagingRepository;
	
	@Override
	public GenericResponse<List<MaintenanceRequestDetailsResponse>> getMaintenanceRequestsByIds(List<String> requestIds) {
		GenericResponse<List<MaintenanceRequestDetailsResponse>> response = new GenericResponse<>();
		List<MaintenanceRequestDetailsResponse> responseList = new ArrayList<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var reqList = maintenanceMsgStagingRepository.findByRequestIdIn(requestIds);
			Map<String, Workflowable> requestObjects = new HashMap<>();
			if(reqList != null) {
				for(var entity : reqList) {
					requestObjects.put(entity.getRequestId(), fromEntity(entity));
				}
			}
			List<Message> message = workflowService.requestDetails(requestObjects,requestIds);
			if (Objects.nonNull(message) && !message.isEmpty()) {
				responseList = message.stream()
						.map(MaintenanceRequestDetailsResponse::toMaintenanceRequestDetailsResponse).collect(Collectors.toList());
				response.setData(responseList);
			}
		} catch (Exception e) {
			log.info("Exception in calling App Maintenance Msg request details :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	
	private MaintenanceMessage fromEntity(MaintenanceMsgStagingEntity stagingEntity) {
		final var appDetailsDomainSet = getApplicationDetailDomainObjects(stagingEntity.getSubStagingEntity());
		final var langDetails = stagingEntity.getDescStagingEntity().stream()
				.map(langDtl -> MaintenanceLangDetails.toMessageDetail(langDtl.getLang(), langDtl.getReason()))
				.sorted(Comparator.comparing(MaintenanceLangDetails::getLang))
				.collect(Collectors.toList());
		return MaintenanceMessage.builder().messageType(stagingEntity.getMessageType())
				.purposeOfMsg(stagingEntity.getPurposeOfMsg()).messageDetails(langDetails)
				.status(stagingEntity.getStatus())
				.refNo(stagingEntity.getRefNo() != null ? Long.parseLong(stagingEntity.getRefNo()) : null)
				.action(stagingEntity.getAction())
				.applicationDetails(appDetailsDomainSet).build();
	}
	
	private List<MaintenanceAppDetail> getApplicationDetailDomainObjects(
			Set<MaintenanceMsgSubStagingEntity> subStagingSet) {
		List<MaintenanceAppDetail> maintenanceDetailList = new ArrayList<>();
		Map<String, List<MaintenanceMsgSubStagingEntity>> appGroup = subStagingSet.stream().filter(sub->sub.getApplication() != null)
				.collect(Collectors.groupingBy(MaintenanceMsgSubStagingEntity::getApplication));
	
		appGroup.entrySet().forEach(entry ->
			maintenanceDetailList.addAll(entry.getValue().stream().map(this::toMaintenanceDetail)
					.collect(Collectors.toList()))
		);
		return maintenanceDetailList;
	}
	
	private MaintenanceAppDetail toMaintenanceDetail(MaintenanceMsgSubStagingEntity subStaging) {
		return MaintenanceAppDetail.builder().application(subStaging.getApplication()).unitId(subStaging.getUnitId())
				.channelIds(subStaging.getChannelId())
				.outageStartDateTime(subStaging.getOutageStartDateTime())
				.outageEndDateTime(subStaging.getOutageEndDateTime())
				.status(subStaging.getStatus()).refNo(subStaging.getRefNo() != null ? Long.parseLong(subStaging.getRefNo()) : null)
				.appId(subStaging.getAppId()).build();
	}
}