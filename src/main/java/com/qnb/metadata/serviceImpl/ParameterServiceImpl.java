package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qnb.metadata.enums.TxnStatus;
import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.parameter.Parameter;
import com.qnb.metadata.dto.parameter.ParameterDetails;
import com.qnb.metadata.dto.parameter.ParameterRequestGetResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.ParamConfigEntity;
import com.qnb.metadata.entity.ParamConfigStagingEntity;
import com.qnb.metadata.entity.ParamConfigSubStagingEntity;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.service.ParameterService;
import com.qnb.metadata.service.ParameterServiceHelper;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

import 	com.qnb.metadata.repo.ParameterRequestStagingRepo;
import com.qnb.metadata.repo.ParamConfigRepository;

@Service
@Slf4j
public class ParameterServiceImpl implements ParameterService{
	
	ResultUtilVO resultVo = new ResultUtilVO();
	@Autowired
	ParameterRequestStagingRepo parameterRequestStagingRepo; 
	@Autowired
	WorkflowService workflowService;
	@Autowired
	ParamConfigRepository paramConfigRepository;
	@Autowired
	ParameterServiceHelper parameterServiceHelper;
	
	@Override
	public GenericResponse<List<ParameterRequestGetResponse>> getParameterRequestsByIds(List<String> requestIds) {
		GenericResponse<List<ParameterRequestGetResponse>> response = new GenericResponse<List<ParameterRequestGetResponse>>();
		List<ParameterRequestGetResponse> responseList = new ArrayList<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var reqParamList = parameterRequestStagingRepo.findByRequestIdInOrderByCreatedTimeAsc(requestIds);
			System.out.println(reqParamList);
			Map<String, Workflowable> requestObjects = new HashMap<>();
			if(Objects.nonNull(reqParamList) && !reqParamList.isEmpty()) {
				for(var entity : reqParamList) {
					final var paramDomainSet = getParamDomainObjects(entity.getSubStagingEntity());
					requestObjects.put(entity.getRequestId(), getParameterDetails(entity, paramDomainSet));
				}
				List<Message> message = workflowService.requestDetails(requestObjects,requestIds);
				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseList = message.stream().map(ParameterRequestGetResponse::toParameterRequestGetResponse)
							.collect(Collectors.toList());
					response.setData(responseList);
					}
			}
		}catch(Exception e) {
			log.info("Exception in calling Parameter Request :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	
	private List<Parameter> getParamDomainObjects(Set<ParamConfigSubStagingEntity> paramStaging) {
		if (paramStaging != null) {
			return paramStaging.stream().map(this::fromEntity).collect(Collectors.toList());}
		return new ArrayList<>();
	}

	private Parameter fromEntity(ParamConfigSubStagingEntity paramStaging) {
		return new Parameter(paramStaging.getExternalId(), paramStaging.getConfKey(), paramStaging.getConfValue(), 
				paramStaging.getStatus(), paramStaging.getRemarks(), paramStaging.getAction());
	}
	
	@Transactional
	private ParameterDetails getParameterDetails(ParamConfigStagingEntity paramStaging, List<Parameter> paramDetails) {
		ParameterDetails param = new ParameterDetails(paramStaging.getUnitId(), paramStaging.getChannelId(), 
				paramDetails, paramStaging.getReqComments());
		
		final List<ParamConfigEntity> existingParamList = paramConfigRepository.findByUnitIdAndChannelIdAndStatusOrderByConfKeyAsc(param.getUnitId(), 
				param.getChannelId(),TxnStatus.ACT);
		ParameterServiceHelper.setActionForParameterRequest(param, existingParamList);
		return param;
	}
}
