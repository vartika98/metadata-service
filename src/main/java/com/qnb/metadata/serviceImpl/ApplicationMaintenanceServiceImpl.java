package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.appMaintenance.Application;
import com.qnb.metadata.dto.appMaintenance.ApplicationChannel;
import com.qnb.metadata.dto.appMaintenance.ApplicationDetails;
import com.qnb.metadata.dto.appMaintenance.ApplicationRequestResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.ApplicationRequestStagingEntity;
import com.qnb.metadata.entity.ApplicationSubRequestStagingEntity;
import com.qnb.metadata.entity.Channel;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.AppMaintenanceRequestStagingRepository;
import com.qnb.metadata.repo.ChannelRespository;
import com.qnb.metadata.service.ApplicationMaintenanceService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class ApplicationMaintenanceServiceImpl implements ApplicationMaintenanceService{
	
	ResultUtilVO resultVo = new ResultUtilVO();
	@Autowired
	private AppMaintenanceRequestStagingRepository appMaintenanceRequestStagingRepo;
	@Autowired
	private ChannelRespository channelRespository;
	@Autowired
	WorkflowService workflowService;
	
	@Override
	public GenericResponse<List<ApplicationRequestResponse>> getApplicationRequestByIds(List<String> requestIds) {
		GenericResponse<List<ApplicationRequestResponse>> response = new GenericResponse<>();
		List<ApplicationRequestResponse> responseList = new ArrayList<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var reqList = appMaintenanceRequestStagingRepo.findByRequestIdIn(requestIds);
			Map<String, Workflowable> requestObjects = new HashMap<>();
			if(reqList != null && !reqList.isEmpty()) {
				for(var entity : reqList) {
					requestObjects.put(entity.getRequestId(), fromEntity(entity));
				}
			}
			List<Message> message = workflowService.requestDetails(requestObjects,requestIds);
			if (Objects.nonNull(message) && !message.isEmpty()) {
				responseList = message.stream()
						.map(ApplicationRequestResponse::applicationRequestResponse).collect(Collectors.toList());
				response.setData(responseList);
				}
		} catch (Exception e) {
			log.info("Exception in calling App Maintenance request details :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	
	private Application fromEntity(ApplicationRequestStagingEntity entity) {
		List<String> channelIds = entity.getSubStagingEntity()
				.stream().map(ApplicationSubRequestStagingEntity :: getChannelId).collect(Collectors.toList());
		List<Channel> channelMasterDetails = getApplicationChannel(channelIds);
		var appDetails = entity.getSubStagingEntity()!=null ? entity.getSubStagingEntity().stream()
				.map(subEntity -> fromEntity(subEntity,channelMasterDetails)).collect(Collectors.toSet())
				: new HashSet<ApplicationDetails>();
						
		return Application.builder().txnId(entity.getExternalId()).appName(entity.getAppName()).appType(entity.getAppType())
				.status(entity.getStatus()).action(entity.getAction()).applicationDetails(appDetails).build();
	}
	
	private List<Channel> getApplicationChannel(List<String> channelIds) {
		List<Channel> channelList = channelRespository.findByChannelIdIn(channelIds);
		return channelList;
	}
	
	private ApplicationDetails fromEntity(ApplicationSubRequestStagingEntity entity, List<Channel> channelMasterDetails) {
		List<Channel> channelDesc = channelMasterDetails
				.stream().filter(chnl -> chnl.getChannelId().equals(entity.getChannelId())).collect(Collectors.toList());
 
		return ApplicationDetails.builder()
				.unit(entity.getUnitId())
				.channel(ApplicationChannel.toApplicationChannel(entity.getChannelId(), channelDesc.get(0).getChannelDesc()))
				.build();
	}
}