package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.ChannelDto;
import com.qnb.metadata.dto.ChannelListRes;
import com.qnb.metadata.dto.channel.ChannelRequestResponse;
import com.qnb.metadata.dto.channel.ChannelStagedRequest;
import com.qnb.metadata.dto.creditCard.CCPaymentStagedRequest;
import com.qnb.metadata.dto.creditCard.CCRequestResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.mapper.ChannelMapper;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.ChannelRespository;
import com.qnb.metadata.repo.ChannelStagedRespository;
import com.qnb.metadata.service.ChannelService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ChannelServiceImpl implements ChannelService{

	@Autowired
	ChannelRespository channelRepo;
	
	@Autowired
	private ChannelMapper channelMapper;
	
	@Autowired
	WorkflowService workflowService;
	
	@Autowired
	ChannelStagedRespository channelStagedRepo;
	
	ResultUtilVO resultVo = new ResultUtilVO();
	
	@Override
	public GenericResponse<ChannelListRes> getAllChannels(String defaultStatus) {
		GenericResponse<ChannelListRes> response = new GenericResponse<>();
		ChannelListRes resData = new ChannelListRes();
		try {
			List<ChannelDto> channelLst = channelRepo.findByStatus(defaultStatus).stream().map(channel ->channelMapper.channelModeltoDto(channel)).collect(Collectors.toList());
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE,AppConstant.RESULT_DESC);
			resData.setChannels(channelLst);
			response.setData(resData);
		} catch (Exception e) {
			log.info("Exception in calling getAllChannels :{}",e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<ChannelRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody) {
		GenericResponse<List<ChannelRequestResponse>> response = new GenericResponse<>();
		List<ChannelRequestResponse> responseMap = new ArrayList<>();
		Map<String, Workflowable> resData = new HashMap<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var channelReqLst = channelStagedRepo.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if (Objects.nonNull(channelReqLst) && !channelReqLst.isEmpty()) {
				List<ChannelStagedRequest> channelReqLstDto = channelReqLst.stream()
						.map(channelStaged -> channelMapper.channelStagedEntityToDto(channelStaged)).collect(Collectors.toList());
				resData = channelReqLstDto.stream().collect(Collectors.toMap(ChannelStagedRequest::getRequestId, item -> item));
				List<Message> message = workflowService.requestDetails(resData, reqBody.get("requestIds"));

				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(ChannelRequestResponse::channelRequestResponse)
							.collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getPendingRequestLst of ChannelServiceImpl :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
