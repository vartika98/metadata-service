package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.smartInstallment.SmartInstDto;
import com.qnb.metadata.dto.smartInstallment.SmartInstRequestResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.mapper.SmartInstStagedMapper;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.SmartInstStagedRepository;
import com.qnb.metadata.service.SmartInstallmentService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SmartInstallmentServiceImpl implements SmartInstallmentService{

	@Autowired
	WorkflowService workflowService;
	
	@Autowired
	SmartInstStagedRepository smartInstStagedRepo;
	
	@Autowired
	SmartInstStagedMapper smartInstStagedMapper;
	
	ResultUtilVO resultVo = new ResultUtilVO();
	
	@Override
	public GenericResponse<List<SmartInstRequestResponse>> getPendingRequest(Map<String, List<String>> reqBody) {
		GenericResponse<List<SmartInstRequestResponse>> response = new GenericResponse<>();
		List<SmartInstRequestResponse> responseMap = new ArrayList<>();
		Map<String,Workflowable> resData = new HashMap<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var smartInstReqLst = smartInstStagedRepo.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if(Objects.nonNull(smartInstReqLst) && !smartInstReqLst.isEmpty()) {
				List<SmartInstDto> smartInstReqLstDto = smartInstReqLst.stream()
						.map(ipoStaged ->smartInstStagedMapper.smartInstStagedEntityToDto(ipoStaged)).collect(Collectors.toList());
				resData = smartInstReqLstDto.stream().collect(Collectors.toMap(SmartInstDto::getRequestId, item -> item));
				List<Message> message = workflowService.requestDetails(resData,reqBody.get("requestIds"));
				
				if(Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(SmartInstRequestResponse::smartInstRequestResponse).collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		}catch (Exception e) {
			log.info("Exception in calling getPendingRequest inside SmartInstallmentServiceImpl:{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
