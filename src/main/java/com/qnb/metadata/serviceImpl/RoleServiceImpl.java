package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.role.GroupDef;
import com.qnb.metadata.dto.role.GroupRequestResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.GroupRequestStagingEntity;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.GroupRequestStagingRepository;
import com.qnb.metadata.service.RoleService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RoleServiceImpl implements RoleService {
	
	ResultUtilVO resultVo = new ResultUtilVO();
	
	@Autowired
	GroupRequestStagingRepository groupRequestStagingRepo;
	
	@Autowired
	WorkflowService workflowService;

	@Override
	public GenericResponse<List<GroupRequestResponse>> getRoleRequest(Map<String, List<String>> reqBody) {
		GenericResponse<List<GroupRequestResponse>> response = new GenericResponse<>();
		List<GroupRequestResponse> responseMap = new ArrayList<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var reqRoleLst = groupRequestStagingRepo.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if(Objects.nonNull(reqRoleLst) && !reqRoleLst.isEmpty()) {
				Map<String,Workflowable> resData = new HashMap<>();
				for(var roleReq : reqRoleLst) {
					resData.put(roleReq.getRequestId(),formGroupDef(roleReq));
				}
				
				
				//Map message calling  
				List<Message> message = workflowService.requestDetails(resData,reqBody.get("requestIds"));
				if(Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(GroupRequestResponse::groupRequestResponse).collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getRoleRequest :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	
	private GroupDef formGroupDef(GroupRequestStagingEntity groupEntity) {
        var roles = (groupEntity.getRoles() != null) ? Set.of(groupEntity.getRoles().split(",")) : new HashSet<String>();
        return GroupDef.builder()
                            .groupId(groupEntity.getExternalId())
                            .groupCode(groupEntity.getGroupCode())
                            .groupDescription(groupEntity.getGroupDescription())
                            .roles(roles)
                            .action(groupEntity.getAction())
                            .unit(groupEntity.getUnit())
                            .makerComments(groupEntity.getRequestComments())
                            .build();
		}

}
