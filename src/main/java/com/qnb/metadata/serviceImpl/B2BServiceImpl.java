package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.b2b.B2BMaster;
import com.qnb.metadata.dto.b2b.B2BResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.B2BStagingEntity;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.B2BStagingRepository;
import com.qnb.metadata.service.B2BService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class B2BServiceImpl implements B2BService{
	
	@Autowired
	private WorkflowService workflowService;
	private ResultUtilVO resultVo = new ResultUtilVO();
	@Autowired
	private B2BStagingRepository b2bStagingRepository;

	@Override
	public GenericResponse<List<B2BResponse>> getB2BRequestByIds(List<String> requestList) {
		GenericResponse<List<B2BResponse>> response = new GenericResponse<List<B2BResponse>>();
		List<B2BResponse> responseList = new ArrayList<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var requiredList = b2bStagingRepository.findByRequestIdIn(requestList);
			Map<String, Workflowable> requestObjects = new HashMap<>();
			if(requiredList != null && !requiredList.isEmpty()) {
				for(var entity : requiredList) {
					requestObjects.put(entity.getRequestId(), fromEntity(entity));
				}
			}
			List<Message> message = workflowService.requestDetails(requestObjects,requestList);
			if (Objects.nonNull(message) && !message.isEmpty()) {
				responseList = message.stream()
						.map(B2BResponse::b2bResponse).collect(Collectors.toList());
				response.setData(responseList);
			}
		} catch (Exception e) {
			log.info("Exception in calling B2B details :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	
	private B2BMaster fromEntity(B2BStagingEntity entity) {
		return B2BMaster.builder()
				.userId(entity.getUserId())
				.globalId(entity.getGlobalId())
				.requestComments(entity.getRequestComments())
				.unitId(entity.getUnitId())
				.debitAccountNo(entity.getDebitAccountNo())
				.requestedBy(entity.getRequestedBy())
				.requestedDate(entity.getRequestedDate())
				.refNo(entity.getRefNo())
				.status(entity.getStatus())
				.rejectedBy(entity.getRejectedBy())
				.rejectedDate(entity.getRejectedDate())
				.rejectCount(entity.getRejectCount())
				.benfAccountNo(entity.getBenfAccountNo())
				.benfName(entity.getBenfName())
				.benfBankName(entity.getBenfBankName())
				.debitAmount(entity.getDebitAmount())
				.creditAmount(entity.getCreditAmount())
				.checkerComments(entity.getCheckerComments())
				.workflowId(entity.getWorkflowId())
				.requestId(entity.getRequestId())
				.build();
	}
}
