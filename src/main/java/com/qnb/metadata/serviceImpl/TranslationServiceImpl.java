package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.LabelDescription;
import com.qnb.metadata.dto.LabelDetails;
import com.qnb.metadata.dto.LabelListRes;
import com.qnb.metadata.dto.LabelResponseDto;
import com.qnb.metadata.dto.ManageLabelReqDto;
import com.qnb.metadata.dto.ManageMenuReqDto;
import com.qnb.metadata.dto.MenuListRes;
import com.qnb.metadata.dto.MenuResponseDto;
import com.qnb.metadata.dto.channel.ChannelRequestResponse;
import com.qnb.metadata.dto.channel.ChannelStagedRequest;
import com.qnb.metadata.dto.entitlement.TransactionEntitlementDto;
import com.qnb.metadata.dto.entitlement.TxnEntitlementRequestGetResponse;
import com.qnb.metadata.dto.label.Label;
import com.qnb.metadata.dto.label.LabelDetail;
import com.qnb.metadata.dto.label.LabelRequestResponse;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.Channel;
import com.qnb.metadata.entity.LabelDecriptionStagingEntity;
import com.qnb.metadata.entity.LabelStagingEntity;
import com.qnb.metadata.entity.LabelSubStagingEntity;
import com.qnb.metadata.entity.Translation;
import com.qnb.metadata.entity.TxnEntitlement;
import com.qnb.metadata.entity.TxnEntitlementRequestStagingEntity;
import com.qnb.metadata.entity.TxnEntitlementSubRequestStagingEntity;
import com.qnb.metadata.entity.Unit;
import com.qnb.metadata.entity.UnitLanguage;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.ChannelRespository;
import com.qnb.metadata.repo.LabelStagingRepository;
import com.qnb.metadata.repo.MenuRepository;
import com.qnb.metadata.repo.TranslationRepository;
import com.qnb.metadata.repo.TxnEntitlementRepository;
import com.qnb.metadata.repo.TxnEntitlementRequestStagingRepository;
import com.qnb.metadata.repo.UnitLanguageRespository;
import com.qnb.metadata.repo.UnitRespository;
import com.qnb.metadata.service.TranslationService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TranslationServiceImpl implements TranslationService {

	ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	MenuRepository menuRepository;

	@Autowired
	TranslationRepository translationRepo;

	@Autowired
	UnitLanguageRespository unitLanguageRepo;

	@Autowired
	ChannelRespository channelRepo;

	@Autowired
	UnitRespository unitRespository;

	@Autowired
	DataSource dataSource;

	@Autowired
	TxnEntitlementRepository txnEntitlementRepository;

	@Autowired
	TxnEntitlementRequestStagingRepository txnEntitlementStageRepo;

	@Autowired
	WorkflowService workflowService;

	@Autowired
	LabelStagingRepository labelStagingRepo;

	@Autowired
	LabelManagementServiceHelper labelManagementServiceHelper;

	@SuppressWarnings("unchecked")
	@Override
	public GenericResponse<MenuListRes> moduleList(Map<String, String> reqBody) {
		GenericResponse<MenuListRes> response = new GenericResponse<>();
		MenuListRes resData = new MenuListRes();
		String errorCode = "";
		List<Map<String, String>> menuList = new ArrayList<>();
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("ocs_t_menu_list")
					.withSchemaName("system");
			try {
				SqlParameterSource in = new MapSqlParameterSource().addValue("P_UNIT_ID", reqBody.get("unitId"))
						.addValue("P_CHANNEL_ID", reqBody.get("channelId")).addValue("p_menu_list", "out")
						.addValue("out_errorcode", "out").addValue("out_errormsg", "out");
				log.info("ocs_t_menu_list procedure inputs are :::{}", String.valueOf(in));
				Map<String, Object> out = jdbcCall.execute(in);
				log.info("ocs_t_menu_list procedure call response: {}", String.valueOf(out));
				if (Objects.nonNull(out) && Objects.nonNull(out.get("P_MENU_LIST"))) {
					menuList = (List<Map<String, String>>) out.get("P_MENU_LIST");
				}
			} catch (Exception e) {
				log.info("Exception in ocs_t_menu_list procedure :{}", e);
			}
			if (Objects.nonNull(menuList) && !menuList.isEmpty()) {
				List<MenuResponseDto> menuRes = new ArrayList<>();
				for (Map<String, String> menu : menuList) {
					MenuResponseDto menuDto = new MenuResponseDto();
					menuDto.setDescription(menu.get("DESCRIPTION"));
					menuDto.setMenuId(menu.get("MENUID"));
					menuDto.setStatus(menu.get("STATUS"));
					menuRes.add(menuDto);
				}
				resData.setMenus(menuRes);
				response.setData(resData);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			}
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public GenericResponse<MenuListRes> screenList(Map<String, String> reqBody) {
		GenericResponse<MenuListRes> response = new GenericResponse<>();
		List<Map<String, String>> screenList = new ArrayList<>();
		MenuListRes resData = new MenuListRes();
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("ocs_t_screen_list")
					.withSchemaName("system");
			try {
				SqlParameterSource in = new MapSqlParameterSource().addValue("P_UNIT_ID", reqBody.get("unitId"))
						.addValue("P_CHANNEL_ID", reqBody.get("channelId")).addValue("P_MENU_ID", reqBody.get("menuId"))
						.addValue("p_screen_list", "out").addValue("out_errorcode", "out")
						.addValue("out_errormsg", "out");
				log.info("ocs_t_menu_list procedure inputs are :::{}", String.valueOf(in));
				Map<String, Object> out = jdbcCall.execute(in);
				log.info("ocs_t_screen_list procedure call response: {}", String.valueOf(out));
				if (Objects.nonNull(out) && Objects.nonNull(out.get("P_SCREEN_LIST"))) {
					screenList = (List<Map<String, String>>) out.get("P_SCREEN_LIST");
				}
			} catch (Exception e) {
				log.info("Exception in ocs_t_screen_list procedure :{}", e);
			}
			if (Objects.nonNull(screenList) && !screenList.isEmpty()) {
				List<MenuResponseDto> screenRes = new ArrayList<>();
				for (Map<String, String> screen : screenList) {
					MenuResponseDto screenDto = new MenuResponseDto();
					screenDto.setDescription(screen.get("DESCRIPTION"));
					screenDto.setMenuId(screen.get("MENUID"));
					screenDto.setScreenId(screen.get("SCREENID"));
					screenDto.setStatus(screen.get("STATUS"));
					screenRes.add(screenDto);
				}
				resData.setMenus(screenRes);
				response.setData(resData);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			}
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<LabelListRes> labelList(Map<String, String> reqBody) {
		GenericResponse<LabelListRes> response = new GenericResponse<>();
		LabelListRes resData = new LabelListRes();
		List<LabelResponseDto> responseLst = new ArrayList<>();
		try {
			List<Translation> labelList = translationRepo.getLabelList(reqBody.get("unitId"), reqBody.get("channelId"),
					reqBody.get("screenId"));
			if (Objects.nonNull(labelList) && !labelList.isEmpty()) {
				List<UnitLanguage> langList = unitLanguageRepo
						.findByUnit_UnitIdAndChannel_ChannelId(reqBody.get("unitId"), reqBody.get("channelId"));
				if (Objects.nonNull(langList) && !langList.isEmpty()) {
					Map<String, List<Translation>> labelGrp = labelList.stream()
							.collect(Collectors.groupingBy(Translation::getKey));
					labelGrp.entrySet().forEach(lb -> {
						List<LabelDescription> labelValueList = lb.getValue().stream()
								.filter(langCode -> langList.contains(langCode.getLangCode()))
								.map(LabelDescription::toLabelDescription).collect(Collectors.toList());
						labelList.forEach(lang -> {
							boolean langExists = labelValueList.stream().anyMatch(
									lbValue -> lbValue != null && lbValue.getLangCode().equals(lang.toString()));
							if (!langExists) {
								var lbLandDesc = LabelDescription.builder().langCode(lang.getLangCode())
										.langValue(lang.getValue()).build();
								labelValueList.add(lbLandDesc);
							}
						});
						var labelDetail = LabelResponseDto.builder().unitId(reqBody.get("unitId"))
								.channelId(reqBody.get("channelId")).screenId(reqBody.get("screenId"))
								.labelKey(lb.getKey()).status(lb.getValue().get(0).getStatus())
								.labelValue(labelValueList).description(lb.getValue().get(0).getDescription()).build();
						responseLst.add(labelDetail);
					});
				}
				resData.setI18(responseLst);
				response.setData(resData);
				resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			}
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e.getMessage());
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageLabels(ManageLabelReqDto reqBody) {
		GenericResponse<Map<String, String>> response = new GenericResponse<>();
		List<Translation> labelsToadd = new ArrayList<>();
		try {
			Channel ch = channelRepo.findByChannelId(reqBody.getChannelId());
			Unit unit = unitRespository.findByUnitId(reqBody.getUnitId());
			List<Translation> existingLabelLst = translationRepo.getLabelList(reqBody.getUnitId(),
					reqBody.getChannelId(), reqBody.getScreenId());
			reqBody.getLabelDetails().stream().forEach(label -> label.getLabelValue().stream().forEach(lbl -> {
				List<Translation> existingLabel = existingLabelLst.stream()
						.filter(lblFil -> lblFil.getKey().equals(label.getLabelKey())
								&& lblFil.getLangCode().equals(lbl.getLangCode()))
						.collect(Collectors.toList());
				if (existingLabel.isEmpty()) {
					Translation txn = new Translation();
					txn.setChannel(ch);
					txn.setUnit(unit);
					txn.setKey(label.getLabelKey());
					txn.setValue(lbl.getLangValue());
					txn.setDescription(label.getRemark());
					txn.setPage(reqBody.getScreenId());
					txn.setLangCode(lbl.getLangCode());
					txn.setGroup("Default");
					txn.setStatus(label.getStatus() != null ? label.getStatus() : "ACT");
					txn.setCreatedBy("SYSTEM");
					labelsToadd.add(txn);
				} else {
					existingLabel.forEach(extLabel -> {
						extLabel.setStatus(label.getAction().equals("MODIFY") ? "ACT" : "DEL");
						if (label.getAction().equals("MODIFY")) {
							extLabel.setGroup("Default");
							extLabel.setKey(label.getLabelKey());
							extLabel.setValue(lbl.getLangValue());
							extLabel.setLangCode(lbl.getLangCode());
							extLabel.setDescription(label.getRemark());
						}
						labelsToadd.add(extLabel);
					});
				}
			}));
			// to save in i18N table:
			log.info("labels to be added:{}", labelsToadd);
			if (!labelsToadd.isEmpty()) {
				translationRepo.saveAll(labelsToadd);
			}
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in moduleList :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<Map<String, String>> manageMenus(ManageMenuReqDto reqBody) {
		GenericResponse<Map<String, String>> response = new GenericResponse<>();
		final List<TxnEntitlement> txnListForUpdate = new ArrayList<>();
		try {
			List<TxnEntitlement> existedTxnLst = txnEntitlementRepository
					.findByChannelIdAndUnitId(reqBody.getChannelId(), reqBody.getUnitId());
			reqBody.getTxnDetails().forEach(txn -> {
				List<TxnEntitlement> existingTxn = existedTxnLst.stream()
						.filter(TxnFil -> TxnFil.getMenuId().equals(txn.getMenuId())).collect(Collectors.toList());
				if (!existingTxn.isEmpty()) {
					existingTxn.forEach(extTxn -> {
						extTxn.setStatus(txn.getStatus());
						txnListForUpdate.add(extTxn);
					});
				}
			});
			log.info("menus to be updated:{}", txnListForUpdate);
			if (!txnListForUpdate.isEmpty()) {
				txnEntitlementRepository.saveAll(txnListForUpdate);
			}
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in manageMenus :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	@Override
	public GenericResponse<List<TxnEntitlementRequestGetResponse>> getEntitlementRequest(
			Map<String, List<String>> reqBody) {
		GenericResponse<List<TxnEntitlementRequestGetResponse>> response = new GenericResponse<>();
		List<TxnEntitlementRequestGetResponse> responseMap = new ArrayList<>();
		try {
			var reqTxnLst = txnEntitlementStageRepo
					.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if (Objects.nonNull(reqTxnLst) && !reqTxnLst.isEmpty()) {
				Map<String, Workflowable> resData = new HashMap<>();
				for (var txnReq : reqTxnLst) {
					final var txnEntitlementDomainSet = getTxnEntitlementDomainObjects(txnReq.getSubStagingEntity());
					resData.put(txnReq.getRequestId(),fromEntity(txnReq,txnEntitlementDomainSet));
				}
				// Map message calling
				List<Message> message = workflowService.requestDetails(resData, reqBody.get("requestIds"));
				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream()
							.map(TxnEntitlementRequestGetResponse::txnEntitlementRequestGetResponse)
							.collect(Collectors.toList());
					response.setData(responseMap);
					resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getRoleRequest :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	private Set<com.qnb.metadata.dto.entitlement.TxnEntitlement> getTxnEntitlementDomainObjects(
			Set<TxnEntitlementSubRequestStagingEntity> subStagingEntity) {
		if (subStagingEntity != null) {
			return subStagingEntity.stream().map(this::fromEntity).collect(Collectors.toCollection(LinkedHashSet::new));
		}
		return new LinkedHashSet<>();
	}

	private com.qnb.metadata.dto.entitlement.TxnEntitlement fromEntity(TxnEntitlementSubRequestStagingEntity entity) {
		return com.qnb.metadata.dto.entitlement.TxnEntitlement.builder().menuId(entity.getMenuId())
				.parentId(entity.getParentId()).status(entity.getStatus()).build();
	}

	private TransactionEntitlementDto fromEntity(TxnEntitlementRequestStagingEntity entity,
			Set<com.qnb.metadata.dto.entitlement.TxnEntitlement> entitlements) {
		return TransactionEntitlementDto.builder().txnEntitlement(entitlements).excuteAction(entity.getExecuteAction())
				.scheduledTimeText(entity.getScheduledTime()).scheduledTimeZone(entity.getScheduledTimeZone())
				.makerComments(entity.getRequestComments()).channelId(entity.getChannelId()).unitId(entity.getUnitId())
				.build();
	}

	@Override
	public GenericResponse<List<LabelRequestResponse>> getLabelRequest(Map<String, List<String>> reqBody) {
		GenericResponse<List<LabelRequestResponse>> response = new GenericResponse<>();
		List<LabelRequestResponse> responseMap = new ArrayList<>();
		Map<String, Workflowable> resData = new HashMap<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var labelReqLst = labelStagingRepo.findByRequestIdInOrderByCreatedTimeAsc(reqBody.get("requestIds"));
			if (Objects.nonNull(labelReqLst) && !labelReqLst.isEmpty()) {
				for (var entity : labelReqLst) {
					final var labelDomainSet = getLabelDomainObjects(entity.getSubStagingEntity());
					resData.put(entity.getRequestId(), toLabelDomain(entity, labelDomainSet));
				}
				List<Message> message = workflowService.requestDetails(resData, reqBody.get("requestIds"));

				if (Objects.nonNull(message) && !message.isEmpty()) {
					responseMap = message.stream().map(LabelRequestResponse::toLabelRequestResponse)
							.collect(Collectors.toList());
					response.setData(responseMap);
				}
			}
		} catch (Exception e) {
			log.info("Exception in calling getPendingRequestLst of TranslationServiceImpl :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

	private Label toLabelDomain(LabelStagingEntity labelStaging, List<LabelDetail> labelDetails) {
		Label label = Label.builder().labelDetails(labelDetails).channelId(labelStaging.getChannelId())
				.screenId(labelStaging.getScreenId()).makerComment(labelStaging.getReqComments())
				.unitId(labelStaging.getUnitId()).menuId(labelStaging.getMenuId()).build();

		List<Translation> labelList = translationRepo.findByUnit_UnitIdAndChannel_ChannelIdAndPage(label.getUnitId(),
				label.getChannelId(), label.getScreenId());

		List<UnitLanguage> langList = unitLanguageRepo.findByUnit_UnitIdAndChannel_ChannelId(label.getUnitId(),
				label.getChannelId());
		labelManagementServiceHelper.identifyPerformedActionOnLabel(label, langList, labelList);
		return label;
	}

	private List<LabelDetail> getLabelDomainObjects(Set<LabelSubStagingEntity> labelStaging) {
		if (labelStaging != null) {
			return labelStaging.stream().map(this::fromEntity).collect(Collectors.toList());
		}
		return new ArrayList<>();

	}

	private LabelDetail fromEntity(LabelSubStagingEntity labelStaging) {
		List<LabelDescription> labelValues = labelStaging.getDescStagingEntity().stream().map(this::getlabelDesc)
				.collect(Collectors.toList());
		return LabelDetail.builder().description(labelStaging.getDescription()).labelValues(labelValues)
				.status(labelStaging.getStatus()).transGroup(labelStaging.getTransGroup())
				.transKey(labelStaging.getTransKey())
				.txnId(labelStaging.getExternalId() != null ? Long.parseLong(labelStaging.getExternalId()) : null)
				.action(labelStaging.getAction()).build();
	}

	private LabelDescription getlabelDesc(LabelDecriptionStagingEntity labelDesc) {
		return LabelDescription.builder().langCode(labelDesc.getLangCode()).langValue(labelDesc.getTransValue())
				.build();
	}

}
