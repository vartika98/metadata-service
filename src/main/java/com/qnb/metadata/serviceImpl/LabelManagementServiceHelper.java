package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.qnb.metadata.dto.LabelDescription;
import com.qnb.metadata.dto.label.Label;
import com.qnb.metadata.entity.Translation;
import com.qnb.metadata.entity.UnitLanguage;
import com.qnb.metadata.enums.ActionType;
import com.qnb.metadata.enums.ExceptionMessages;
import com.qnb.metadata.exception.ValidationException;
@Service
public class LabelManagementServiceHelper {
	public void identifyPerformedActionOnLabel(Label labelReq, List<UnitLanguage> unitLang,
			List<Translation> existingLabelList) {
		labelReq.getLabelDetails().forEach(req -> {

			List<Translation> matchingExsistinglblEntity = existingLabelList.stream()
					.filter(ext -> ext.getKey().equals(req.getTransKey())).collect(Collectors.toList());

			if (ActionType.DELETE.toString().equalsIgnoreCase(req.getAction())
					&& matchingExsistinglblEntity.isEmpty()) {

				req.setAction(ActionType.CXL_ADD.toString());

			} else if (!ActionType.DELETE.toString().equalsIgnoreCase(req.getAction())
					&& matchingExsistinglblEntity.isEmpty()) {

				req.setAction(ActionType.ADD.toString());

			} else if (!ActionType.DELETE.toString().equalsIgnoreCase(req.getAction())
					&& !matchingExsistinglblEntity.isEmpty()) {

				List<LabelDescription> labelValuesList = matchingExsistinglblEntity.stream()
						.filter(langCode -> unitLang.contains(langCode.getLangCode()))
						.map(LabelDescription::toLabelDescription).collect(Collectors.toList());
				List<LabelDescription> modified = new ArrayList<>();

				req.getLabelValues().stream()
						.forEach(incoming -> modified.addAll(labelValuesList.stream()
								.filter(exist -> exist != null && exist.getLangCode().equals(incoming.getLangCode())
										&& !exist.getLangValue().equals(incoming.getLangValue()))
								.collect(Collectors.toList())));

				if (!modified.isEmpty() || !req.getStatus().equals(matchingExsistinglblEntity.get(0).getStatus())
						|| !req.getDescription().equals(matchingExsistinglblEntity.get(0).getDescription())) {
					req.setAction(ActionType.MODIFY.toString());
				} else {
					req.setAction(ActionType.NO_ACTION.toString());
				}
			} else if (matchingExsistinglblEntity.isEmpty()) {
				req.setAction(ActionType.ADD.toString());
			}
		});

		labelReq.getLabelDetails().removeIf(x -> ActionType.CXL_ADD.toString().equalsIgnoreCase(x.getAction()));

	}

	public void ckeckLabelAlreadyExist(Label labelReq, List<Translation> labelList) {
		Set<String> duplicateLabelKeys = new HashSet<>();
		labelReq.getLabelDetails().forEach(req -> {
			List<Translation> labelExist = labelList.stream()
					.filter(lbl -> lbl.getKey().equals(req.getTransKey())
							&& ActionType.ADD.toString().equalsIgnoreCase(req.getAction()))
					.collect(Collectors.toList());
			if (!labelExist.isEmpty()) {
				duplicateLabelKeys.add(req.getTransKey());
			}
		});
		if (!duplicateLabelKeys.isEmpty()) {
			throw new ValidationException(ExceptionMessages.LABEL_ALREADY_EXISTS_EXCEPTION.getErrorCode(),
					String.format("Label already exist with the keys: %s", String.join(", ", duplicateLabelKeys)),
					HttpStatus.CONFLICT);

		}
	}
}
