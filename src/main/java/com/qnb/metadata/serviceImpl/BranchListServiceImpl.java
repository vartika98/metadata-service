package com.qnb.metadata.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qnb.metadata.constant.AppConstant;
import com.qnb.metadata.dto.branchList.BranchListMaster;
import com.qnb.metadata.dto.branchList.BranchListResponse;
import com.qnb.metadata.dto.branchList.BranchParameterRequest;
import com.qnb.metadata.dto.workflow.Message;
import com.qnb.metadata.entity.BranchListStagingEntity;
import com.qnb.metadata.model.GenericResponse;
import com.qnb.metadata.model.ResultUtilVO;
import com.qnb.metadata.repo.BranchListStagingRepository;
import com.qnb.metadata.service.BranchListService;
import com.qnb.metadata.service.WorkflowService;
import com.qnb.metadata.service.workflow.Workflowable;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BranchListServiceImpl implements BranchListService{
	
	@Autowired
	private WorkflowService workflowService;
	private ResultUtilVO resultVo = new ResultUtilVO();
	@Autowired
	private BranchListStagingRepository branchListStagingRepository; 

	@Override
	public GenericResponse<List<BranchListResponse>> getBranchListRequestByIds(List<String> requestIdList) {
		GenericResponse<List<BranchListResponse>> response = new GenericResponse<List<BranchListResponse>>();
		List<BranchListResponse> responseList = new ArrayList<>();
		try {
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
			var requiredList = branchListStagingRepository.findByWorkFlowReqIdIn(requestIdList);
			Map<String, Workflowable> requestObjects = new HashMap<>();
			if(requiredList != null && !requiredList.isEmpty()) {
				for(var entity : requiredList) {
					if(requestObjects.containsKey(entity.getWorkFlowReqId())) {
						BranchListMaster oldEntity = (BranchListMaster) requestObjects.get(entity.getWorkFlowReqId());
						List<BranchParameterRequest> paramReq = oldEntity.getParamDetails();
						paramReq.addAll(convert(entity));
						oldEntity.setParamDetails(paramReq);
					}else {
						requestObjects.put(entity.getWorkFlowReqId(), fromEntity(entity));
					}
				}
			}
			List<Message> message = workflowService.requestDetails(requestObjects,requestIdList);
			if (Objects.nonNull(message) && !message.isEmpty()) {
				responseList = message.stream()
						.map(BranchListResponse::branchListResponse).collect(Collectors.toList());
				response.setData(responseList);
			}
		} catch (Exception e) {
			log.info("Exception in calling B2B details :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}
	
	private BranchListMaster fromEntity(BranchListStagingEntity entity) {
			
			return BranchListMaster.builder()
					.unitId(entity.getUnitId())
					.remarks(entity.getRequestComments())
					.paramDetails(convert(entity))
					.build();
		}
		
	private static List<BranchParameterRequest> convert(BranchListStagingEntity entity) {
			List<BranchParameterRequest> errorParameterReq = new ArrayList<>();
			BranchParameterRequest req=new BranchParameterRequest();
			if (entity != null) {
				req.setAction(entity.getActionType());
				req.setCountryCode(entity.getCountryCode());
				req.setBranchCode(entity.getBranchCode());
				req.setBranchDesc(entity.getBranchDesc());
				req.setBranchDescAr(entity.getBranchDescAr());
				req.setBranchDescFr(entity.getBranchDescFr());
				req.setCity(entity.getCity());
				req.setAddress(entity.getAddress());
				req.setCollectionBranchFlag(entity.getCollectionBranchFlag());
				req.setStatus(entity.getStatus());
				req.setCreatedBy(entity.getCreatedBy());
				req.setDateCreated(entity.getDateCreated());
				req.setModifiedBy(entity.getLastModifiedBy());
				req.setDateModified(entity.getDateModified());
				req.setBranchId(entity.getBranchId());
				errorParameterReq.add(req);
			}
			return errorParameterReq;
		}
}
